//
//  AppDelegate.h
//  XZSQ
//
//  Created by Paul on 2019/6/8.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoanProductsListViewController.h" //贷款

@interface AppDelegate : UIResponder <UIApplicationDelegate>

//主窗口
@property (strong, nonatomic) UIWindow *window;

//底部导航栏控制器
@property (nonatomic, strong) UITabBarController *tabBarController;

- (void) resetRoot_HomeViewController;
- (void) resetRoot_HomeViewControllerIndex:(NSInteger)index;
- (void) resetRoot_ForceLoginViewController;
- (void) setupWebControllerWithURL:(NSString *)url;
- (void) directToLoanRankControllerWithSortType:(SortType)sortType;
- (void) directToIdent;
@end

