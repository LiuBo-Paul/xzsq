//
//  AppDelegate.m
//  XZSQ
//
//  Created by Paul on 2019/6/8.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import "AppDelegate.h"
#import "ForcedLoginViewController.h" //强制登录
#import "DD_HomeViewController.h" //首页
#import "MyViewController.h" //个人中心

//启动图页面
#import "LaunchViewController.h"

//备用h5页面
#import "FuturesViewController.h"

#import "AL_HomeViewController.h"
#import "AL_CardsListViewController.h"
#import "AL_IdentityViewController.h"
#import "AL_MyViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // 应用程序启动后覆盖自定义的点。
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    // 设置启动页，用此控制器承接启屏页与首页
    LaunchViewController *viewController = NibController(LaunchViewController);
    self.window.rootViewController = viewController;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
        [SVProgressHUD setMaximumDismissTimeInterval:1];
        [SVProgressHUD setBackgroundLayerColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
        [SVProgressHUD setFadeInAnimationDuration:0.3];
        [SVProgressHUD setFadeOutAnimationDuration:0.3];
        
        //保存UserAgent, 用于所有请求头
        [[General_Tool GT_sharedInstance] GT_saveUserAgent];
        
    });
    
    return YES;
}

- (void)resetRoot_HomeViewController {
    [self resetRoot_HomeViewControllerIndex:0];
}

- (void)resetRoot_HomeViewControllerIndex:(NSInteger)index {
    // 首页
    UINavigationController *navHome = [self createTabItemWithController:[DD_HomeViewController new] title:@"首页" imageName:@"tab_home" selectedImageName:@"tab_home_selected"];
    UINavigationController *navBlack = [self createTabItemWithController:[LoanProductsListViewController new] title:@"贷款" imageName:@"tab_loan" selectedImageName:@"tab_loan_selected"];
    UINavigationController *navMy = [self createTabItemWithController:[MyViewController new] title:@"我的" imageName:@"tab_me" selectedImageName:@"tab_me_selected"];

    UINavigationController *navALHome = [self createTabItemWithController:[AL_HomeViewController new] title:@"首页" imageName:@"tab_home" selectedImageName:@"tab_home_selected"];
    AL_IdentityViewController *identifier = [[AL_IdentityViewController alloc] initWithNibName:@"AL_IdentityViewController" bundle:[NSBundle mainBundle]];
    identifier.showType = TableViewShowTypeCredit;
    UINavigationController *navALCard = [self createTabItemWithController:identifier title:@"" imageName:@"tab_loan" selectedImageName:@"tab_loan_selected"];
    UINavigationController *navALMy = [self createTabItemWithController:[AL_MyViewController new] title:@"我的" imageName:@"tab_me" selectedImageName:@"tab_me_selected"];
    self.tabBarController = [[UITabBarController alloc] init];
//    self.tabBarController.viewControllers = @[navHome, navBlack, navMy];
    self.tabBarController.viewControllers = @[navALHome, navALMy];
    self.tabBarController.selectedIndex = index;
    
    self.window.rootViewController = self.tabBarController;
}

-(void)directToIdent {
    [self resetRoot_HomeViewControllerIndex:1];
}

-(void)directToLoanRankControllerWithSortType:(SortType)sortType {
    NSInteger loanControllerIndex = 1;
    LoanProductsListViewController *controller = ((UINavigationController *)self.tabBarController.viewControllers[loanControllerIndex]).viewControllers.firstObject;
    controller.sortType = sortType;
    self.tabBarController.selectedIndex = loanControllerIndex;
}

- (void)resetRoot_ForceLoginViewController {
    ForcedLoginViewController *forcedLoginViewController = NibController(ForcedLoginViewController);
    self.window.rootViewController = forcedLoginViewController;
}

- (void)setupWebControllerWithURL:(NSString *)url
{
    FuturesViewController *rootVC = [[FuturesViewController alloc] init];
    rootVC.webUrl = url;
    [self.window setRootViewController:rootVC];
}

-(UINavigationController *)createTabItemWithController:(UIViewController *)viewController title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName
{
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    UIImage *image = [UIImage imageNamed:imageName];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIImage *imageSelected = [UIImage imageNamed:selectedImageName];
    imageSelected = [imageSelected imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:title image:image selectedImage:imageSelected];
    
    [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:MainColor, NSForegroundColorAttributeName, [UIFont systemFontOfSize:11], NSFontAttributeName, nil] forState:UIControlStateNormal];
    [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:MainSelectedColor, NSForegroundColorAttributeName, [UIFont systemFontOfSize:11], NSFontAttributeName, nil] forState:UIControlStateSelected];
    
    nav.tabBarItem = item;
    
    return nav;
}

@end
