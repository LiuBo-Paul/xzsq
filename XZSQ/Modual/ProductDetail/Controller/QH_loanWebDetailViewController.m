//
//  QH_loanWebDetailViewController.m
//  XZSQ
//
//  Created by Paul on 2019/8/29.
//  Copyright © 2018年 QingHu. All rights reserved.
//

#import "QH_loanWebDetailViewController.h"
#import <WebKit/WebKit.h>
#import "ExpandView.h"

@interface QH_loanWebDetailViewController ()<UIWebViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, copy) NSString *startTime;
@property (nonatomic, assign) NSTimeInterval startTimeInterval;
@property (nonatomic, copy) NSString *endTime;

@property (nonatomic, copy) NSString *urlStr;
@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic, copy) NSString *pid;

@end

@implementation QH_loanWebDetailViewController

- (instancetype)initWithUrlStr:(NSString *)urlStr titleStr:(NSString *)titleStr pid:(NSString *)pid
{
    self = [super init];
    if (self)
    {
        _urlStr = urlStr;
        _titleStr = titleStr;
        _pid = pid;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[ViewTool sharedInstance] removeBlankViewFromView:self.webView];
    self.startTimeInterval = [self getCurrentTime];
    self.startTime = [self getformatCurrentTimeWithInterval:self.startTimeInterval];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.webView removeFromSuperview];
    self.webView = nil;
    [self cleanWebViewCacheData];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{
        if(self.pid)
        {
            if(![DefaultNativeId isEqual:@""])
            {
                NSString *pid = self.pid?self.pid:@"";
                self.endTime = [self getformatCurrentTimeWithInterval:[self getCurrentTime]];
                NSString *timeDiffrence = [NSString stringWithFormat:@"%.3f", [self pleaseInsertStarTime:self.startTime andInsertEndTime:self.endTime]];
                [[RecordTool sharedRecordTool] accountTimeRecordWithPid:pid duration:timeDiffrence startTime:self.startTime endTime:self.endTime];
            }
        }
    });
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self openPanGestureToBack];
    if(self.pid.length > 0) {
        [[RecordTool sharedRecordTool] clickProductRecordWithPid:self.pid];
    }
    [SVProgressHUD show];
    [self addBackItems];
    self.title = self.titleStr;
    [self cleanWebViewCacheData];
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64)];
    self.webView.scalesPageToFit = YES;
    self.webView.delegate = self;
    if(self.urlStr.length > 0 && [self.urlStr rangeOfString:@"http"].length > 0) {
        [self.webView loadRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:self.urlStr?self.urlStr:@""] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0]];
        [[General_Tool GT_sharedInstance] GT_removeBlankViewFromView:self.view];
        [self.view addSubview:self.webView];
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress)];
        longPress.delegate = self;
        longPress.minimumPressDuration = 0.1;
        [self.webView addGestureRecognizer:longPress];
    } else {
        [SVProgressHUD showErrorWithStatus:@"页面走丢了，请稍后访问！"];
    }
}

-(void)addBackItems {
    UIBarButtonItem *itemBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"loan_ic_left"] style:(UIBarButtonItemStyleDone) target:self action:@selector(backToPrePage:)];
    itemBack.tintColor = [UIColor whiteColor];
    UIBarButtonItem *itemClose = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"loan_ic_delete"] style:(UIBarButtonItemStyleDone) target:self action:@selector(closePage:)];
    itemClose.tintColor = [UIColor whiteColor];
    NSArray<UIBarButtonItem *> *items = @[itemBack, itemClose];
    [self.navigationItem setLeftBarButtonItems:items];
}

-(void)backToPrePage:(UIBarButtonItem *)item {
    [SVProgressHUD dismiss];
    if(self.webView.canGoBack) {
        [self.webView goBack];
    } else {
        [self closePage:item];
    }
}

-(void)closePage:(UIBarButtonItem *)item {
    [SVProgressHUD dismiss];
    WEAKSELF
    if(self.navigationController.viewControllers.count >= 1) {
        [[ExpandView shareInstance] popWithNavController:self.navigationController type:(ExpandViewAnimationTypeDefault) showEnd:^{
            [weakSelf cleanWebViewCacheData];
        }];
    } else {
        [[ExpandView shareInstance] dismissToController:self type:(ExpandViewAnimationTypeDefault) showEnd:^{
            [weakSelf cleanWebViewCacheData];
        }];
    }
}

-(void)longPress
{
    //nothing to do.
}

-(void)didReceiveDataSuccessWithData:(id)data requestKey:(NSString *)requestKey {
    
}

-(void)didReceiveDataErrorWithError:(NSString *)errorMessage requestKey:(NSString *)requestKey {
    
}

-(void)back:(UIBarButtonItem *)item {
    [SVProgressHUD dismiss];
    WEAKSELF
    [[ExpandView shareInstance] popWithNavController:self.navigationController type:(ExpandViewAnimationTypeDefault) showEnd:^{
        [weakSelf cleanWebViewCacheData];
    }];
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
    [SVProgressHUD show];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [SVProgressHUD dismiss];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
//    if(error.code == NSURLErrorCancelled) {
//        return;
//    }
    
//    WEAKSELF
//    [[ViewTool sharedInstance] addBlankViewToView:self.webView title:@"加载失败" subTitle:@"请稍后重试...\n" image:[UIImage imageNamed:@"load_fail_bg"] refreshBlock:^{
//        [[ViewTool sharedInstance] removeBlankViewFromView:self.webView];
//        [weakSelf.webView loadRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:self.urlStr?self.urlStr:@""] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0]];
//    }];
    [SVProgressHUD dismiss];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = request.URL;
    if (![url.scheme isEqual:@"http"] && ![url.scheme isEqual:@"https"]) {
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
            return NO;
        }
    }
    return YES;
}

#pragma mark ---- 清理webview上的缓存
- (void)cleanWebViewCacheData {
    //清除cookies
    if ([[[UIDevice currentDevice] systemVersion] intValue] > 8)
    {
        NSArray * types =@[WKWebsiteDataTypeDiskCache,
                           WKWebsiteDataTypeMemoryCache,
                           WKWebsiteDataTypeOfflineWebApplicationCache,
                           WKWebsiteDataTypeCookies,
                           WKWebsiteDataTypeSessionStorage,
                           WKWebsiteDataTypeLocalStorage,
                           WKWebsiteDataTypeWebSQLDatabases,
                           WKWebsiteDataTypeIndexedDBDatabases]; // 9.0之后才有的
        NSSet *websiteDataTypes = [NSSet setWithArray:types];
        NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
        [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
        }];
    }
    else
    {
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask, YES) objectAtIndex:0];
        NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
        NSLog(@"%@", cookiesFolderPath);
        NSError *errors;
        [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
    }
    
    NSString *libraryDir = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,
                                                               NSUserDomainMask, YES)[0];
    NSString *bundleId  =  [[[NSBundle mainBundle] infoDictionary]
                            objectForKey:@"CFBundleIdentifier"];
    NSString *webkitFolderInLib = [NSString stringWithFormat:@"%@/WebKit",libraryDir];
    NSString *webKitFolderInCaches = [NSString
                                      stringWithFormat:@"%@/Caches/%@/WebKit",libraryDir,bundleId];
    NSString *webKitFolderInCachesfs = [NSString
                                        stringWithFormat:@"%@/Caches/%@/fsCachedData",libraryDir,bundleId];
    
    NSError *error;
    /* iOS8.0 WebView Cache的存放路径 */
    [[NSFileManager defaultManager] removeItemAtPath:webKitFolderInCaches error:&error];
    [[NSFileManager defaultManager] removeItemAtPath:webkitFolderInLib error:nil];
    
    /* iOS7.0 WebView Cache的存放路径 */
    [[NSFileManager defaultManager] removeItemAtPath:webKitFolderInCachesfs error:&error];
}

-(void)dealloc {
    [self.webView removeFromSuperview];
    self.webView = nil;
    [self cleanWebViewCacheData];
}

-(NSString *)getformatCurrentTimeWithInterval:(NSTimeInterval)timeInterval {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd hh:mm:ss.SSS"];
    return [format stringFromDate:[NSDate dateWithTimeIntervalSince1970:timeInterval]];
}

-(NSTimeInterval)getCurrentTime {
    return [[NSDate date] timeIntervalSince1970];
}

- (NSTimeInterval)pleaseInsertStarTime:(NSString *)starTime andInsertEndTime:(NSString *)endTime {
    NSDateFormatter* formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd hh:mm:ss.SSS"];//根据自己的需求定义格式
    NSDate* startDate = [formater dateFromString:starTime];
    NSDate* endDate = [formater dateFromString:endTime];
    NSTimeInterval time = [endDate timeIntervalSinceDate:startDate];
    return time;
}

@end
