//
//  QH_loanWebDetailViewController.h
//  XZSQ
//
//  Created by Paul on 2019/8/29.
//  Copyright © 2018年 QingHu. All rights reserved.
//

#import "BaseViewController.h"

@interface QH_loanWebDetailViewController : BaseViewController

- (instancetype)initWithUrlStr:(NSString *)urlStr titleStr:(NSString *)titleStr pid:(NSString *)pid;

@end
