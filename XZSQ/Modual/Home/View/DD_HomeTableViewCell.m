//
//  DD_HomeTableViewCell.m
//  XZSQ
//
//  Created by Paul on 2019/11/8.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import "DD_HomeTableViewCell.h"

@implementation DD_HomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initHomeTableCellWithModel:(Models *)model {
    if(model) {
        [self.dd_homeImageView sd_setImageWithURL:[NSURL URLWithString:model.Extend2]];
        self.dd_homeNameLabel.text = model.Name.length > 0?model.Name:@"";
        self.dd_home_remarkLabel.text = model.Extend3.length > 0?model.Extend3:@"";
        self.dd_homeMoneyLabel.text = [NSString stringWithFormat:@"%@-%@", model.MinLimit, model.Limit];
        self.dd_homeTimeLabel.text = model.FastestTime.length > 0 ? [NSString stringWithFormat:@"最快%@分钟下款", model.FastestTime] : @"";
        self.dd_homeRateLabel.text = model.Rate.length > 0?[NSString stringWithFormat:@"日利率%@", model.Rate]:@"";
        self.dd_homeDateLabel.text = model.DurationStart.length > 0 ? [NSString stringWithFormat:@"贷款期限%@-%@天", model.DurationStart, model.DurationEnd] : @"";

        self.dd_home_applyLabel.clipsToBounds = YES;
        self.dd_home_applyLabel.layer.cornerRadius = self.dd_home_applyLabel.height/2.0;
        self.dd_home_applyLabel.layer.borderWidth = 1;
        if([[NSString stringWithFormat:@"%@", model.ApplyStatus] isEqual:@"0"]) {
            self.dd_home_applyLabel.layer.borderColor = ItemColorFromRGB(0xED4054).CGColor;
            self.userInteractionEnabled = YES;
        } else {
            self.dd_home_applyLabel.layer.borderColor = ItemColorFromRGB(0x999999).CGColor;
            self.dd_home_applyLabel.textColor = ItemColorFromRGB(0x999999);
            self.dd_home_applyLabel.text = @"已申请";
            self.userInteractionEnabled = NO;
            self.alpha = 0.5;
        }
        [self layoutIfNeeded];
        [self addLabelsToViewWithTags:model.Extend4Convert];
    }
}

-(void)addLabelsToViewWithTags:(NSMutableArray *)tags {
    if(self.dd_home_labelsView.subviews.count != 0) {
        [self.dd_home_labelsView removeAllSubviews];
    }
    CGFloat labelsLeft = 0;
    CGFloat labelsWidth = 0;
    for (int i = 0; i < tags.count; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(labelsLeft, 0, labelsWidth, self.dd_home_labelsView.height)];
        label.clipsToBounds = YES;
        label.layer.borderWidth = 1.0;
        label.layer.borderColor = [MainSelectedColor colorWithAlphaComponent:0.5].CGColor;
        label.textColor = [MainSelectedColor colorWithAlphaComponent:0.5];
        label.layer.cornerRadius = label.height/2.0;
        label.textAlignment = NSTextAlignmentCenter;
        label.font = kSysFontSize(8);
        label.text = tags[i];
        [self.dd_home_labelsView addSubview:label];
        labelsWidth = [[StringTool sharedInstance] getWidthWithString:tags[i] fontSize:10];
        if((labelsLeft + labelsWidth) > self.dd_home_labelsView.width) {
            labelsWidth = self.dd_home_labelsView.width - labelsLeft;
            label.frame = ({
                CGRect frame = label.frame;
                frame.size.width = labelsWidth;
                frame;
            });
            break;
        } else {
            label.frame = ({
                CGRect frame = label.frame;
                frame.size.width = labelsWidth;
                frame;
            });
        }
        labelsLeft = labelsLeft + [[StringTool sharedInstance] getWidthWithString:tags[i] fontSize:10] + 4;
    }
}

@end
