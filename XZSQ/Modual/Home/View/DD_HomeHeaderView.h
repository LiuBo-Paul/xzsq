//
//  DD_HomeHeaderView.h
//  XZSQ
//
//  Created by Paul on 2019/11/8.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DD_HomeHeaderView : UIView

@property (strong, nonatomic) IBOutlet UIView *DDR_productsBackView;

@property (strong, nonatomic) IBOutlet UIImageView *DDRS_iconImageView;
@property (strong, nonatomic) IBOutlet UILabel *DDRS_titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *DDRS_btn;

@property (strong, nonatomic) IBOutlet UILabel *card_titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *card_moneyLabel;
@property (strong, nonatomic) IBOutlet UILabel *card_remarkLabel;
@property (strong, nonatomic) IBOutlet UILabel *card_timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *card_timeUnitLabel;
@property (strong, nonatomic) IBOutlet UIButton *card_sureBtn;
@property (strong, nonatomic) IBOutlet UIButton *card_coverBtn;

@property (strong, nonatomic) IBOutlet UILabel *secTwoHeaderTitleLabel;
@property (strong, nonatomic) IBOutlet UIButton *secTwoMoreBtn;


@property (strong, nonatomic) IBOutlet UIButton *sortAllBtn;
@property (strong, nonatomic) IBOutlet UIButton *sortSmallBtn;
@property (strong, nonatomic) IBOutlet UIButton *sortBigBtn;


-(instancetype)initWithRFrame:(CGRect)frame;
-(instancetype)initWithRSFrame:(CGRect)frame;
-(instancetype)initWithCardFrame:(CGRect)frame;
-(instancetype)initWithSecTwoHeaderFrame:(CGRect)frame;
-(instancetype)initWithSortHeaderFrame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
