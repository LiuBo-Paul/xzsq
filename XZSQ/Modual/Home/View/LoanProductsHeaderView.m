//
//  LoanProductsHeaderView.m
//  XZSQ
//
//  Created by Paul on 2019/1/2.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "LoanProductsHeaderView.h"

@implementation LoanProductsHeaderView

- (instancetype)initWithLoanProductsHeaderViewFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle] loadNibNamed:@"LoanProductsHeaderView" owner:self options:nil][0];
        self.frame = frame;
    }
    return self;
}

@end
