//
//  LoanProductsHeaderView.h
//  XZSQ
//
//  Created by Paul on 2019/1/2.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoanProductsHeaderView : UIView

@property (strong, nonatomic) IBOutlet UIView *highBackView;
@property (strong, nonatomic) IBOutlet UIButton *highBtn;
@property (strong, nonatomic) IBOutlet UILabel *highLabel;

@property (strong, nonatomic) IBOutlet UIView *fastBackView;
@property (strong, nonatomic) IBOutlet UIButton *fastBtn;
@property (strong, nonatomic) IBOutlet UILabel *fastLabel;

@property (strong, nonatomic) IBOutlet UIView *lowBackView;
@property (strong, nonatomic) IBOutlet UIButton *lowBtn;
@property (strong, nonatomic) IBOutlet UILabel *lowLabel;

@property (strong, nonatomic) IBOutlet UIView *lineView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lineViewLeading;

- (instancetype)initWithLoanProductsHeaderViewFrame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
