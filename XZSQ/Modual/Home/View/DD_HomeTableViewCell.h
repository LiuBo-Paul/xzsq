//
//  DD_HomeTableViewCell.h
//  XZSQ
//
//  Created by Paul on 2019/11/8.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DD_HomeTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *dd_homeImageView;
@property (strong, nonatomic) IBOutlet UILabel *dd_homeNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *dd_homeMoneyLabel;
@property (strong, nonatomic) IBOutlet UILabel *dd_homeRateLabel;
@property (strong, nonatomic) IBOutlet UILabel *dd_home_remarkLabel;
@property (strong, nonatomic) IBOutlet UIView *dd_home_labelsView;
@property (strong, nonatomic) IBOutlet UILabel *dd_home_applyLabel;
@property (strong, nonatomic) IBOutlet UILabel *dd_homeTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *dd_homeDateLabel;
@property (strong, nonatomic) IBOutlet UIImageView *dd_homeCornerHotImageView;

-(void)initHomeTableCellWithModel:(Models *)model;

@end

NS_ASSUME_NONNULL_END
