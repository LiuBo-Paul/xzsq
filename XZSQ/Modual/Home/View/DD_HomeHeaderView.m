//
//  DD_HomeHeaderView.m
//  XZSQ
//
//  Created by Paul on 2019/11/8.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import "DD_HomeHeaderView.h"

@implementation DD_HomeHeaderView

-(instancetype)initWithRFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        self = MainBundle(@"DD_HomeHeaderView", self, 0);
        self.frame = frame;
    }
    return self;
}

-(instancetype)initWithRSFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        self = MainBundle(@"DD_HomeHeaderView", self, 1);
        self.frame = frame;
    }
    return self;
}

-(instancetype)initWithCardFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        self = MainBundle(@"DD_HomeHeaderView", self, 2);
        self.frame = frame;
    }
    return self;
}

-(instancetype)initWithSecTwoHeaderFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self = MainBundle(@"DD_HomeHeaderView", self, 3);
        self.frame = frame;
    }
    return self;
}

-(instancetype)initWithSortHeaderFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self = MainBundle(@"DD_HomeHeaderView", self, 4);
        self.frame = frame;
    }
    return self;
}

@end
