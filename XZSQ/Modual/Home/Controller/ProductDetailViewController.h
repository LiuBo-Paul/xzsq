//
//  ProductDetailViewController.h
//  XZSQ
//
//  Created by Paul on 2019/7/19.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductDetailViewController : BaseViewController

@property (nonatomic, copy) NSString *pid;

@end

NS_ASSUME_NONNULL_END
