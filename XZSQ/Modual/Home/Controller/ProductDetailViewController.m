//
//  ProductDetailViewController.m
//  XZSQ
//
//  Created by Paul on 2019/7/19.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "OpenShowView.h"
#import "OpenShowMainView.h"
#import "ExpandView.h"
#import "QH_loanWebDetailViewController.h"

@interface ProductDetailViewController ()<OpenShowViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *pro_backScrollView;

@property (strong, nonatomic) IBOutlet UIImageView *pro_logo;
@property (strong, nonatomic) IBOutlet UILabel *pro_name;
@property (strong, nonatomic) IBOutlet UIView *pro_topLabelsBackView;
@property (strong, nonatomic) IBOutlet UILabel *pro_money;
@property (strong, nonatomic) IBOutlet UILabel *pro_rate;
@property (strong, nonatomic) IBOutlet UILabel *pro_time;

@property (strong, nonatomic) IBOutlet UIView *pro_applyBackView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *pro_applyBackViewHeightCon;
@property (strong, nonatomic) IBOutlet UILabel *pro_age;
@property (strong, nonatomic) IBOutlet UILabel *pro_identi;
@property (strong, nonatomic) IBOutlet UILabel *pro_edu;
@property (strong, nonatomic) IBOutlet UIView *pro_quaLabelsBackView;

@property (strong, nonatomic) IBOutlet UIView *pro_msgBackView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *pro_msgBackViewHeightCon;
@property (strong, nonatomic) IBOutlet UILabel *pro_date;
@property (strong, nonatomic) IBOutlet UILabel *pro_cost;
@property (strong, nonatomic) IBOutlet UILabel *pro_repay;
@property (strong, nonatomic) IBOutlet UILabel *pro_prepay;

@property (strong, nonatomic) IBOutlet UIView *pro_elseBackView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *pro_elseBackViewHeightCon;
@property (strong, nonatomic) IBOutlet UILabel *pro_adv;
@property (strong, nonatomic) IBOutlet UILabel *pro_proc;
@property (strong, nonatomic) IBOutlet UIButton *pro_apply;


@property (nonatomic, strong) ProductDetailModel *model;


@end

static BOOL isEnableClick = YES; //是否可点击，用于对比按钮重复点击

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"产品详情";
    [self addBackItem];
    [self.pro_apply addTarget:self action:@selector(pro_applyAction:) forControlEvents:(UIControlEventTouchUpInside)];
}

//初始化产品视图，需要先获取产品详情
-(void)initProViewWithProductModel:(ProductDetailModel *)model {
    WEAKSELF
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.pro_logo sd_setImageWithURL:[NSURL URLWithString:model.InnerData.Extend2]];
        weakSelf.pro_name.text = MakeSureNotNil(model.InnerData.Name);
        NSInteger moneyLimit = [model.InnerData.MinLimit integerValue];
        if(moneyLimit) {
            [weakSelf addLabelsToViewWithTags:[@[[NSString stringWithFormat:@"件均%ld元", (long)([model.InnerData.MinLimit integerValue]+1000)]] mutableCopy] inView:weakSelf.pro_topLabelsBackView];
        } else {
            [weakSelf addLabelsToViewWithTags:[@"件均3000元" mutableCopy] inView:weakSelf.pro_topLabelsBackView];
        }
        weakSelf.pro_money.text = [NSString stringWithFormat:@"%@-%@元", MakeSureNotNil(model.InnerData.MinLimit), MakeSureNotNil(model.InnerData.Limit)];
        weakSelf.pro_rate.text = [NSString stringWithFormat:@"%@%@", MakeSureNotNil(model.InnerData.Rate), @"%/日"];
        weakSelf.pro_time.text = [NSString stringWithFormat:@"%@分钟", MakeSureNotNil(model.InnerData.FastestTime)];
        weakSelf.pro_age.text = @"20-50周岁";
        weakSelf.pro_identi.text = @"除学生外";
        weakSelf.pro_edu.text = @"无";
        [weakSelf addLabelsToViewWithTags:[@[@" 手机号实名 ", @" 芝麻分350以上 "] mutableCopy] inView:weakSelf.pro_quaLabelsBackView];
        weakSelf.pro_date.text = [NSString stringWithFormat:@"%@-%@天", MakeSureNotNil(model.InnerData.DurationStart), MakeSureNotNil(model.InnerData.DurationEnd)];
        weakSelf.pro_cost.text = @"无";
        weakSelf.pro_repay.text = @"依据甲方实际还款方式";
        weakSelf.pro_prepay.text = @"可提前还款（提前还款费息不变）";
        weakSelf.pro_adv.text = @"下款快，速来申请。";
        weakSelf.pro_proc.text = @"实名认证->芝麻分认证->运营商授权";
    });
}

-(void)addLabelsToViewWithTags:(NSMutableArray *)tags inView:(UIView *)view {
    if(view.subviews.count != 0) {
        [view removeAllSubviews];
    }
    CGFloat labelsLeft = 0;
    CGFloat labelsWidth = 0;
    for (int i = 0; i < tags.count; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(labelsLeft, 0, labelsWidth, view.height)];
        label.textColor = ItemColorRGB(106, 151, 226);
        label.backgroundColor = ItemColorRGB(208, 229, 255);
        label.layer.cornerRadius = label.height/2.0;
        label.textAlignment = NSTextAlignmentCenter;
        label.font = kSysFontSize(8);
        label.text = tags[i];
        [view addSubview:label];
        labelsWidth = [[StringTool sharedInstance] getWidthWithString:tags[i] fontSize:10];
        if((labelsLeft + labelsWidth) > view.width) {
            labelsWidth = view.width - labelsLeft;
            label.frame = ({
                CGRect frame = label.frame;
                frame.size.width = labelsWidth;
                frame;
            });
            break;
        } else {
            label.frame = ({
                CGRect frame = label.frame;
                frame.size.width = labelsWidth;
                frame;
            });
        }
        labelsLeft = labelsLeft + [[StringTool sharedInstance] getWidthWithString:tags[i] fontSize:10] + 4;
    }
}


// 修改子页面高度，根据子页面内最后一个控件元素展开。
static CGFloat InViewBottomSpace = 8; //子页面内空白间距
-(void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.pro_applyBackViewHeightCon.constant = ((UIView *)self.pro_applyBackView.subviews.lastObject).bottom + InViewBottomSpace;
    [self.pro_applyBackView layoutIfNeeded];
    
    self.pro_msgBackViewHeightCon.constant = ((UIView *)self.pro_msgBackView.subviews.lastObject).bottom + InViewBottomSpace;
    [self.pro_msgBackView layoutIfNeeded];
    
    self.pro_elseBackViewHeightCon.constant = ((UIView *)self.pro_elseBackView.subviews.lastObject).bottom + InViewBottomSpace;
    [self.pro_elseBackView layoutIfNeeded];
    
    [self.pro_backScrollView layoutIfNeeded];
}

// 申请按钮点击事件
-(void)pro_applyAction:(UIButton *)sender {
    if(self.model) {
        [self directToDetailViewControllerWithUrl:self.model.InnerData.Extend1 name:self.model.InnerData.Name pid:self.model.InnerData.Pid isToSafari:[[NSString stringWithFormat:@"%@", self.model.InnerData.OpenType] isEqual:@"1"]];
    }
}

// 获取详情
-(void)requestDetailDataWithPid:(NSString *)pid type:(NSString *)type needUnionLogin:(BOOL)needUnionLogin {
    if(!isEnableClick) {
        return;
    }
    isEnableClick = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        isEnableClick = YES;
    });
    WEAKSELF
    if(needUnionLogin) {
        [[RequestMannager sharedInstance] requestProductDetailWithPid:pid
                                                                 type:type
                                                      completionBlock:^(id responseObject) {
                                                          [SVProgressHUD dismiss];
                                                          ProductDetailModel *model = [[ProductDetailModel alloc] initWithDictionary:responseObject error:nil];
                                                          weakSelf.model = model;
                                                          if([NSString stringWithFormat:@"%@", model.InnerData.UnionLoginAgreeH5].length > 0 &&
                                                             [[NSString stringWithFormat:@"%@", model.InnerData.UnionLoginAgreeH5] rangeOfString:@"http"].length > 0) {
                                                              // 弹出协议框
                                                              CGFloat viewLeftSpace = 40, viewTopSpace = 120;
                                                              OpenShowMainView *openShowMainView = [[OpenShowMainView alloc] initWithOpenShowFrame:CGRectMake(viewLeftSpace, viewTopSpace, ScreenWidth - viewLeftSpace*2, ScreenHeight - viewTopSpace*2)];
                                                              openShowMainView.clipsToBounds = YES;
                                                              openShowMainView.layer.cornerRadius = 10;
                                                              [openShowMainView.mainWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.InnerData.UnionLoginAgreeH5]]];
                                                              [openShowMainView.agreeBtn addTarget:weakSelf action:@selector(agreeBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
                                                              OpenShowView *openShowView = [OpenShowView shareInstance];
                                                              openShowView.delegate = self;
                                                              openShowView.isAnima = YES;
                                                              [openShowView showDeledate:self showView:openShowMainView];
                                                          } else {
                                                              [weakSelf initProViewWithProductModel:weakSelf.model];
                                                          }
                                                      } failBlock:^(NSError *error) {
                                                          [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                                                      }];
    } else {
        [[RequestMannager sharedInstance] requestProductDetailWithPid:pid
                                                                 type:type
                                                      completionBlock:^(id responseObject) {
                                                          [SVProgressHUD dismiss];
                                                          ProductDetailModel *model = [[ProductDetailModel alloc] initWithDictionary:responseObject error:nil];
                                                          weakSelf.model = model;
                                                          [weakSelf initProViewWithProductModel:weakSelf.model];
                                                      } failBlock:^(NSError *error) {
                                                          [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                                                      }];
    }
}

-(void)agreeBtnAction:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        sender.userInteractionEnabled = YES;
    });
    if(self.pid.length > 0) {
        [[OpenShowView shareInstance] hide];
        [self requestDetailDataWithPid:self.pid type:@"1" needUnionLogin:NO];
    } else {
        NSLog(@"pid 未设置!");
    }
}

// 弹窗消失后的回调方法
-(void)didHideOpenShowView {
    
}

// 跳转到产品详情页
-(void)directToDetailViewControllerWithUrl:(NSString *)url name:(NSString *)name pid:(NSString *)pid isToSafari:(BOOL)isToSafari {
    if(pid.length > 0 && url.length > 0 && [url rangeOfString:@"http"].length > 0) {
        if(isToSafari) {
            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:^(BOOL success) {
                    if(pid.length > 0) {
                        [[RecordTool sharedRecordTool] clickProductRecordWithPid:pid];
                    }
                }];
            }
        } else {
            QH_loanWebDetailViewController *detail = [[QH_loanWebDetailViewController alloc] initWithUrlStr:url titleStr:name pid:pid];
            [[ExpandView shareInstance] pushToController:detail navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:self.view type:ExpandViewAnimationTypeDefault showEnd:^{
                
            }];
        }
    }
}

-(void)setPid:(NSString *)pid {
    if(pid.length > 0) {
        _pid = pid;
        [self requestDetailDataWithPid:pid type:@"0" needUnionLogin:YES];
    }
}

@end
