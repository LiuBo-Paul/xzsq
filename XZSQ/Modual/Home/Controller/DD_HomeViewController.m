//
//  DD_HomeViewController.m
//  XZSQ
//
//  Created by Paul on 2019/11/8.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import "DD_HomeViewController.h"
#import "DD_HomeHeaderView.h"
#import "GeneralHeader.h"
#import "GeneralFooter.h"
#import "QH_loanWebDetailViewController.h"
#import "HomePageInfoModel.h"
#import "ESTimer.h"
#import "FSCacheManager.h"
#import "DD_HomeTableViewCell.h"
#import "FSCacheManager.h"
#import "ExpandView.h"
#import "PScrollNewsView.h"
#import "LoanProductsHeaderView.h"
#import "SDCycleScrollView.h"
#import "OpenShowView.h"
#import "OpenShowMainView.h"
#import "ProductDetailViewController.h"

@interface DD_HomeViewController ()<
    UIGestureRecognizerDelegate,
    UITableViewDelegate,
    PScrollNewsViewDelegate,
    SDCycleScrollViewDelegate,
    OpenShowViewDelegate,
    UITableViewDataSource,
    UIScrollViewDelegate
>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) HomePageInfoModel *homePageInfoModel;
@property (nonatomic, strong) ProductDetailModel *selecteDetailModel;

@property (nonatomic, strong) UIView *headerBackView;
@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;
@property (nonatomic, strong) NSMutableArray *cycleScrollArray;
@property (nonatomic, strong) UIView *sortBackView;
@property (nonatomic, strong) PScrollNewsView *newsView;
@property (nonatomic, strong) NSMutableArray *newsArray;

@property (nonatomic, strong) DD_HomeHeaderView *headerSectionTitleView;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) BOOL hasNextPage;

@property (nonatomic, strong) DD_HomeHeaderView *tableHeader;
@property (nonatomic, strong) UIView *prepareExpandView;

@end

static CGFloat sortHeaderHeight = 84;
static NSString *openShowPid = @"";
static BOOL isEnableClick = YES;

@implementation DD_HomeViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[UITabBar appearance] setTranslucent:NO];
    [[General_Tool GT_sharedInstance] GT_requestGlobalInfo];
}

//-(void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO animated:NO];
//}

-(void)viewDidLoad {
    [super viewDidLoad];
    self.currentPage = 1;
    self.navigationItem.title = AppName;
    [self.view addSubview:self.tableView];
    self.tableView.tableHeaderView = [self createHeaderView];
    
    [self requestHomePageInfo];
    [self refreshProducts];
    WEAKSELF
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf showAdView];
    });
}

// 显示弹窗广告，每次f打开APP都要显示
static NSInteger adShowViewWidth = 310;     //弹窗固定高度，取自一倍背景图
static NSInteger adShowViewHeight = 436;    //弹窗固定高度，取自一倍背景图
-(void)showAdView {
    OpenShowMainView *adContentView = [[OpenShowMainView alloc] initWithAdFrame:CGRectMake((ScreenWidth - adShowViewWidth)/2.0, (ScreenHeight - adShowViewHeight)/2.0, adShowViewWidth, adShowViewHeight)];
    [adContentView.adSureBtn addTarget:self action:@selector(adSureBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    adContentView.tag = 1029;
    [[OpenShowView shareInstance] showDeledate:self showView:adContentView];
}

// 广告按钮点击事件 进入详情页，目前只有广告点击a才进入详情页
-(void)adSureBtnAction:(UIButton *)sender {
    [[OpenShowView shareInstance] hide];
    ProductDetailViewController *detail = NibController(ProductDetailViewController);
    detail.hidesBottomBarWhenPushed = YES;
    detail.pid = self.homePageInfoModel.InnerData.PopupAdvert.Pid;
    [self.navigationController pushViewController:detail animated:YES];
}

// 列表懒加载初始化
-(UITableView *)tableView {
    if(!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - ToolbarHeight - NavBarHeight - StateBarHeight) style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.separatorColor = [UIColor clearColor];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tag = 23450;
        WEAKSELF
        _tableView.mj_header = [GeneralHeader headerWithRefreshingBlock:^{
            [weakSelf requestHomePageInfo];
            [weakSelf refreshProducts];
        }];
        _tableView.mj_footer = [GeneralFooter footerWithRefreshingBlock:^{
            [weakSelf loadMoreProducts];
        }];
    }
    return _tableView;
}

-(UIView *)createHeaderView {
    self.headerBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 0)];
    self.headerBackView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.2];
    [self createCycleScrollViewWithArray:self.cycleScrollArray];
    [self.headerBackView addSubview:self.cycleScrollView];

//    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, StateBarHeight, ScreenWidth, 30)];
//    titleLabel.text = AppName;
//    titleLabel.textColor = [UIColor whiteColor];
//    titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleLabel.font = kSysFontSize(15);
//    [self.headerBackView addSubview:titleLabel];
    
    [self createSortView];
    [self createNewsView];
    
    WEAKSELF
    self.headerBackView.frame = ({
        CGRect frame = weakSelf.headerBackView.frame;
        frame.size.height = weakSelf.newsView.bottom;
        frame;
    });
    
    return self.headerBackView;
}

-(UIView *)refreshHeaderView {
    [self refreshCycleScrollViewWithArray:self.cycleScrollArray];
    [self refreshSortView];
    [self refreshNewsView];
    WEAKSELF
    self.headerBackView.frame = ({
        CGRect frame = weakSelf.headerBackView.frame;
        frame.size.height = weakSelf.newsView.bottom + 10;
        frame;
    });
    return self.headerBackView;
}

-(void)createSortView {
    self.sortBackView = [[UIView alloc] initWithFrame:CGRectMake(0, self.cycleScrollView.bottom, ScreenWidth, sortHeaderHeight)];
    self.sortBackView.userInteractionEnabled = YES;
    self.sortBackView.backgroundColor = [UIColor whiteColor];
    DD_HomeHeaderView *sortSubView = [[DD_HomeHeaderView alloc] initWithSortHeaderFrame:CGRectMake(0, 0, self.sortBackView.width, self.sortBackView.height)];
    [sortSubView.sortAllBtn addTarget:self action:@selector(sortAllBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [sortSubView.sortSmallBtn addTarget:self action:@selector(sortSmallBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [sortSubView.sortBigBtn addTarget:self action:@selector(sortBigBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.sortBackView addSubview:sortSubView];
    [self.headerBackView addSubview:self.sortBackView];
}

-(void)sortAllBtnAction:(UIButton *)sender {
    [APPDELEGATE directToLoanRankControllerWithSortType:SortTypeAll];
}

-(void)sortSmallBtnAction:(UIButton *)sender {
    [APPDELEGATE directToLoanRankControllerWithSortType:SortTypeSmall];
}

-(void)sortBigBtnAction:(UIButton *)sender {
    [APPDELEGATE directToLoanRankControllerWithSortType:SortTypeBig];
}

-(void)refreshSortView {
    self.sortBackView.frame = CGRectMake(0, self.cycleScrollView.bottom, ScreenWidth, sortHeaderHeight);
    [self.sortBackView layoutSubviews];
}

-(UIView *)createCycleScrollViewWithArray:(NSArray *)array
{
    //轮播图
    CGFloat cycleImageHeight = 190;
    UIImage *cycleImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[self.cycleScrollArray firstObject]]]];
    if(!cycleImage) {
        cycleImage = [UIImage imageNamed:@"banner"];
    }
    cycleImageHeight = cycleImage.size.height*ScreenWidth/cycleImage.size.width;
    if(!self.cycleScrollView) {
        self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0,ScreenWidth, cycleImageHeight) delegate:self placeholderImage:[UIImage imageNamed:@"banner"]];
        self.cycleScrollView.backgroundColor = [UIColor whiteColor];
    }
    self.cycleScrollView.imageURLStringsGroup = array;
    [self.cycleScrollView reloadInputViews];
    return self.cycleScrollView;
}

-(UIView *)refreshCycleScrollViewWithArray:(NSArray *)array
{
    CGFloat cycleImageHeight = 190;
    UIImage *cycleImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[self.cycleScrollArray firstObject]]]];
    if(!cycleImage) {
        cycleImage = [UIImage imageNamed:@"banner"];
    }
    cycleImageHeight = cycleImage.size.height*ScreenWidth/cycleImage.size.width;
    self.cycleScrollView.frame = CGRectMake(0, 0,ScreenWidth, cycleImageHeight);
    self.cycleScrollView.imageURLStringsGroup = array;
    [self.cycleScrollView reloadInputViews];
    return self.cycleScrollView;
}

-(void)createNewsView {
    //跑马灯
    self.newsView = [[PScrollNewsView alloc] initWithRichFrame:CGRectMake(0, self.sortBackView.bottom + 1, ScreenWidth, 40) backgroundColor:[UIColor whiteColor] viewsArray:@[]];
    self.newsView.delegate = self;
    [self.headerBackView addSubview:self.newsView];
}

-(void)refreshNewsView {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableArray *mArr = [NSMutableArray new];
        for (int i = 0; i < self.homePageInfoModel.InnerData.Bulletin.count; i++) {
            Bulletin *model = self.homePageInfoModel.InnerData.Bulletin[i];
            
            UIView *newBackView = [[UIView alloc] initWithFrame:CGRectMake(8, 0, self.newsView.width, self.newsView.height)];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(8, self.newsView.height/4.0, self.newsView.height/2.0, self.newsView.height/2.0)];
            [imageView sd_setImageWithURL:[NSURL URLWithString:model.PLogo] placeholderImage:[UIImage imageNamed:@"ic_notice"]];
            [newBackView addSubview:imageView];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(imageView.right + imageView.left, 0, self.newsView.width - (imageView.right + imageView.left) - imageView.left, self.newsView.height)];
            label.attributedText = [self highLightStr:model.SummaryHighLineWordsSplit inStr:model.Summary];
            [label setFont:[UIFont systemFontOfSize:12]];
            [label adjustsFontSizeToFitWidth];
            
            [newBackView addSubview:label];
            
            [mArr addObject:newBackView];
        }
        self.newsArray = [NSMutableArray arrayWithArray:[mArr copy]];
        [self.newsView reloadScrollRighTextViewsArray:self.newsArray];
        WEAKSELF
        self.newsView.frame = ({
            CGRect frame = weakSelf.newsView.frame;
            frame.origin.y = weakSelf.sortBackView.bottom + 1;
            frame;
        });
    });
}

-(void)viewWillLayoutSubviews {
    WEAKSELF
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.tableView.frame = ({
            CGRectMake(0, 0, ScreenWidth, ScreenHeight - ToolbarHeight - NavBarHeight - StateBarHeight);
        });
    });
}

//获取首页基础数据
-(void)requestHomePageInfo {
    WEAKSELF
    // [SVProgressHUD show];
    [[RequestMannager sharedInstance] requestHomePageInfoParam:@{} completionBlock:^(id responseObject) {
        [SVProgressHUD dismiss];
        [[General_Tool GT_sharedInstance] GT_removeBlankViewFromView:self.view];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        
        weakSelf.homePageInfoModel = [[HomePageInfoModel alloc] initWithDictionary:responseObject error:nil];
        if(weakSelf.homePageInfoModel) {
            NSMutableArray *bannermArr = [[NSMutableArray alloc] init];
            for (int i = 0; i < weakSelf.homePageInfoModel.InnerData.Banners.count; i++)
            {
                Banners *banners = weakSelf.homePageInfoModel.InnerData.Banners[i];
                [bannermArr addObject:banners.Image];
            }
            [[ViewTool sharedInstance] removeBlankViewFromView:weakSelf.tableView.tableHeaderView];
            weakSelf.cycleScrollArray = [NSMutableArray arrayWithArray:[bannermArr copy]];
            [weakSelf refreshHeaderView];
            [weakSelf.tableView reloadData];
        } else {
            STRONGSELF
            [[ViewTool sharedInstance] addBlankViewToView:weakSelf.tableView.tableHeaderView title:@"加载失败" subTitle:@"请稍后重试..." image:[UIImage imageNamed:@"load_fail_bg"] refreshBlock:^{
                [strongSelf requestHomePageInfo];
            }];
        }
    } failBlock:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        STRONGSELF
        [[ViewTool sharedInstance] addBlankViewToView:weakSelf.tableView.tableHeaderView title:@"加载失败" subTitle:@"请稍后重试..." image:[UIImage imageNamed:@"load_fail_bg"] refreshBlock:^{
            [strongSelf requestHomePageInfo];
        }];
    }];
}

-(void)refreshProducts {
    self.currentPage = 1;
    [self requestProducts];
}

-(void)loadMoreProducts {
    self.currentPage ++;
    [self requestProducts];
}

//获取产品列表
-(void)requestProducts {
    WEAKSELF
    [[RequestMannager sharedInstance] requestProductsWithParam:@{@"pageIndex":[NSString stringWithFormat:@"%ld", (long)self.currentPage], @"pageNo":@"10", @"sortBy":@"1", @"channel":AppStoreKey}
                                               completionBlock:^(id responseObject) {
                                                   [SVProgressHUD dismiss];
                                                   ProductsModel *model = [[ProductsModel alloc] initWithDictionary:responseObject error:nil];
                                                   if(model) {
                                                       weakSelf.hasNextPage = model.InnerData.HasNextPage;
                                                       [weakSelf.tableView.mj_header endRefreshing];
                                                       [weakSelf.tableView.mj_footer endRefreshing];
                                                       if(weakSelf.currentPage != 1) {
                                                           if(!model.InnerData.HasNextPage) {
                                                               [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                                                           } else {
                                                               if(model.InnerData.Models.count > 0) {
                                                                   [weakSelf.dataArray addObjectsFromArray:[model.InnerData.Models copy]];
                                                                   [weakSelf.tableView reloadData];
                                                               }
                                                           }
                                                       } else {
                                                           if(model.InnerData.Models.count > 0) {
                                                               weakSelf.dataArray = [NSMutableArray arrayWithArray:[model.InnerData.Models copy]];
                                                               [weakSelf.tableView reloadData];
                                                           }
                                                       }
                                                   } else {
                                                       
                                                   }
                                               } failBlock:^(NSError *error) {
                                                   [weakSelf.tableView.mj_header endRefreshing];
                                                   [weakSelf.tableView.mj_footer endRefreshing];
                                                   [SVProgressHUD dismiss];
                                               }];
}

-(NSString *)getListTypeWithIndex:(NSInteger)index {
    NSString *type = @"0";
    switch (index) {
        case 0: {
            type = @"3";
        } break;
        case 1: {
            type = @"1";
        } break;
        case 2:{
            type = @"2";
        } break;
        default:
            break;
    }
    return type;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0: {
            return 1;
        } break;
        case 1: {
            return self.dataArray.count;
        } break;
        default:
            return 0;
            break;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listSubView"];
        if(!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"listSubView"];
        }
        [cell.contentView removeAllSubviews];
        [cell.contentView addSubview:self.headerSectionTitleView];
        return cell;
    } else {
        DD_HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DD_HomeTableViewCell"];
        if(!cell)
        {
            cell = MainBundle(@"DD_HomeTableViewCell", self, 0);
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if(indexPath.row >= 3) {
            cell.dd_homeCornerHotImageView.hidden = YES;
        }
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            return self.headerSectionTitleView.height;
        } break;
        case 1: {
            return 126;
        } break;
        default:
            return 0;
            break;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if(indexPath.section == 1) {
        [self beginLoadCell:(DD_HomeTableViewCell *)cell index:indexPath.row];
    }
}

-(void)beginLoadCell:(DD_HomeTableViewCell *)cell index:(NSInteger)index {
    if(index < self.dataArray.count) {
        Models *model = self.dataArray[index];
        [cell initHomeTableCellWithModel:model];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 1) {
        self.prepareExpandView = [tableView cellForRowAtIndexPath:indexPath];
        if(indexPath.row < self.dataArray.count) {
            Models *model = self.dataArray[indexPath.row];
            if(model && model.Pid.length > 0) {
                [self requestDetailDataWithPid:model.Pid type:@"0" needUnionLogin:[[NSString stringWithFormat:@"%@", model.NeedUnionLogin] isEqual:@"1"]];
            }
        }
    }
}

-(void)clickNewsItem:(id)bulletin index:(NSInteger)index {
    if(index >= 0 && index < self.dataArray.count) {
        Bulletin *model = self.homePageInfoModel.InnerData.Bulletin[index];
        if(model && model.Pid.length > 0) {
            self.prepareExpandView = self.newsView;
            [self requestDetailDataWithPid:model.Pid type:@"0" needUnionLogin:YES];
        }
    }
}

-(NSAttributedString *)highLightStr:(NSString *)subString inStr:(NSString *)originStr {
    NSArray *subStringArr = [subString componentsSeparatedByString:@"|"];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:originStr];;
    for (int i = 0; i < subStringArr.count; i++) {
        [attrStr addAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} range:[originStr rangeOfString:subStringArr[i]]];
    }
    return [attrStr copy];
}

// 获取详情
-(void)requestDetailDataWithPid:(NSString *)pid type:(NSString *)type needUnionLogin:(BOOL)needUnionLogin {
    if(!isEnableClick) {
        return;
    }
    isEnableClick = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        isEnableClick = YES;
    });
    WEAKSELF
//    [SVProgressHUD show];
    if(needUnionLogin) {
        [[RequestMannager sharedInstance] requestProductDetailWithPid:pid
                                                                 type:type
                                                      completionBlock:^(id responseObject) {
                                                          [SVProgressHUD dismiss];
                                                          ProductDetailModel *model = [[ProductDetailModel alloc] initWithDictionary:responseObject error:nil];
                                                          if([NSString stringWithFormat:@"%@", model.InnerData.UnionLoginAgreeH5].length > 0 &&
                                                             [[NSString stringWithFormat:@"%@", model.InnerData.UnionLoginAgreeH5] rangeOfString:@"http"].length > 0) {
                                                              // 弹出协议框
                                                              CGFloat viewLeftSpace = 40, viewTopSpace = 120;
                                                              OpenShowMainView *openShowMainView = [[OpenShowMainView alloc] initWithOpenShowFrame:CGRectMake(viewLeftSpace, viewTopSpace, ScreenWidth - viewLeftSpace*2, ScreenHeight - viewTopSpace*2)];
                                                              openShowMainView.clipsToBounds = YES;
                                                              openShowMainView.layer.cornerRadius = 10;
                                                              [openShowMainView.mainWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.InnerData.UnionLoginAgreeH5]]];
                                                              [openShowMainView.agreeBtn addTarget:weakSelf action:@selector(agreeBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
                                                              OpenShowView *openShowView = [OpenShowView shareInstance];
                                                              openShowView.delegate = self;
                                                              openShowView.isAnima = YES;
                                                              openShowPid = pid;
                                                              [openShowView showDeledate:self showView:openShowMainView];
                                                          } else {
                                                              [weakSelf directToDetailViewControllerWithUrl:model.InnerData.Extend1 name:model.InnerData.Name pid:model.InnerData.Pid];
                                                          }
                                                      } failBlock:^(NSError *error) {
                                                          [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                                                      }];
    } else {
        [[RequestMannager sharedInstance] requestProductDetailWithPid:pid
                                                                 type:type
                                                      completionBlock:^(id responseObject) {
                                                          [SVProgressHUD dismiss];
                                                          ProductDetailModel *model = [[ProductDetailModel alloc] initWithDictionary:responseObject error:nil];
                                                          [weakSelf directToDetailViewControllerWithUrl:model.InnerData.Extend1 name:model.InnerData.Name pid:model.InnerData.Pid];
                                                      } failBlock:^(NSError *error) {
                                                          [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                                                      }];
    }
}

-(void)agreeBtnAction:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        sender.userInteractionEnabled = YES;
    });
    if(openShowPid.length > 0) {
        [[OpenShowView shareInstance] hide];
        NSInteger index = 0;
        for (int i = 0; i < self.dataArray.count; i++) {
            Models *model = self.dataArray[i];
            if([[NSString stringWithFormat:@"%@", model.Pid] isEqual:openShowPid]) {
                index = i;
                break;
            }
        }
        DD_HomeTableViewCell * cell = (DD_HomeTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:1]];
        cell.alpha = 0.5;
        cell.dd_home_applyLabel.layer.borderColor = ItemColorFromRGB(0x999999).CGColor;
        cell.dd_home_applyLabel.textColor = ItemColorFromRGB(0x999999);
        cell.dd_home_applyLabel.text = @"已申请";
        cell.userInteractionEnabled = NO;
        [self requestDetailDataWithPid:openShowPid type:@"1" needUnionLogin:NO];
    } else {
        NSLog(@"pid 未设置!");
    }
}

// 弹窗消失后的回调方法
-(void)didHideOpenShowView {
    
}

// 跳转到产品详情页
-(void)directToDetailViewControllerWithUrl:(NSString *)url name:(NSString *)name pid:(NSString *)pid {
    if(pid.length > 0 && url.length > 0 && [url rangeOfString:@"http"].length > 0) {
//        QH_loanWebDetailViewController *detail = [[QH_loanWebDetailViewController alloc] initWithUrlStr:url titleStr:name pid:pid];
//        [[ExpandView shareInstance] pushToController:detail navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:self.prepareExpandView type:ExpandViewAnimationTypeDefault showEnd:^{
//
//        }];
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:^(BOOL success) {
                if(pid.length > 0) {
                    [[RecordTool sharedRecordTool] clickProductRecordWithPid:pid];
                }
            }];
        }
    }
}

-(NSMutableArray *)dataArray {
    if(!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

-(PScrollNewsView *)newsView {
    if(!_newsView) {
        //跑马灯
        _newsView = [[PScrollNewsView alloc] initWithRichFrame:CGRectMake(0, 0, ScreenWidth, 40) backgroundColor:[UIColor whiteColor] viewsArray:@[]];
        _newsView.delegate = self;
    }
    return _newsView;
}

-(DD_HomeHeaderView *)headerSectionTitleView {
    if(!_headerSectionTitleView) {
        _headerSectionTitleView = [[DD_HomeHeaderView alloc] initWithSecTwoHeaderFrame:CGRectMake(0, 0, ScreenWidth, 38.5)];
        [_headerSectionTitleView.secTwoMoreBtn addTarget:self action:@selector(secTwoMoreBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _headerSectionTitleView;
}

-(void)secTwoMoreBtnAction:(UIButton *)sender {
    [APPDELEGATE directToLoanRankControllerWithSortType:(SortTypeAll)];
}

/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    Banners *banners = self.homePageInfoModel.InnerData.Banners[index];
    NSString *pid = @"";
    if(banners.Url.length > 0) {
        //url不为空
        if([banners.Url rangeOfString:@"productdetail:"].length > 0) {
            //跳转商品详情页
            pid = [[banners.Url componentsSeparatedByString:@":"] lastObject];
            self.prepareExpandView = cycleScrollView;
            [self requestDetailDataWithPid:pid type:@"0" needUnionLogin:YES];
        } else {
            if([banners.Url rangeOfString:@"http"].length > 0) {
                [self directToDetailViewControllerWithUrl:banners.Url name:banners.Title pid:@""];
            } else {
                // url 不合法
            }
        }
    } else {
        // url为空
    }
}

@end
