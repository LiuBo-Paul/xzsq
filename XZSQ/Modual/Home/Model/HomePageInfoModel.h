//
//  HomePageInfoModel.h
//  DaddyLoan
//
//  Created by Sara on 18/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol Banners
@end
@interface Banners : JSONModel

@property (nonatomic, copy) NSString *Id; //编号
@property (nonatomic, copy) NSString *SortIndex; //排序(升序)
@property (nonatomic, copy) NSString *Title; //描述
@property (nonatomic, copy) NSString *Image; //图片地址
@property (nonatomic, copy) NSString *Url; //跳转地址

@end

@protocol Bulletin
@end
@interface Bulletin : JSONModel

@property (nonatomic, copy) NSString *Title; //描述
@property (nonatomic, copy) NSString *Summary; //摘要信息
@property (nonatomic, copy) NSString *SummaryHighLineWords; //摘要信息需要高亮的字段 （全字段匹配）
@property (nonatomic, copy) NSString *SummaryHighLineWordsSplit; //摘要信息需要高亮的字段集合： 通过竖线分割
@property (nonatomic, copy) NSString *CreateTime; //创建时间
@property (nonatomic, copy) NSString *Pid; //商品编号
@property (nonatomic, copy) NSString *PLogo; //商品Logo
@property (nonatomic, copy) NSString *Url; //跳转地址

@end

@protocol PopupAdvert
@end
@interface PopupAdvert : JSONModel

@property (nonatomic, copy) NSString *Pid; //商品编号

@end


@interface HomePageInfoModelInnerData : BaseJSONModel

@property (nonatomic, strong) NSMutableArray<Banners> *Banners; //Banner 列表
@property (nonatomic, strong) NSMutableArray<Bulletin> *Bulletin; //通告栏 列表
@property (nonatomic, strong) PopupAdvert *PopupAdvert; //首页广告弹窗

@end

@interface HomePageInfoModel : BaseJSONModel

@property (nonatomic, strong) HomePageInfoModelInnerData *InnerData;

@end
