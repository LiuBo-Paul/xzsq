//
//  GlobalConfigInfoModel.h
//  DaddyLoan
//
//  Created by 青弧 on 12/03/2018.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import "BaseJSONModel.h"

@interface GlobalConfigInfoModelInnerData : BaseJSONModel

@property (nonatomic, copy) NSString *ContactPhone; //联系我们 电话
@property (nonatomic, copy) NSString *ContactWXNO; //联系我们微信号
@property (nonatomic, copy) NSString *ContactQR; //联系我们 二维码
@property (nonatomic, copy) NSString *ContactQRDescribe; //联系我们 二维码 描述
@property (nonatomic, copy) NSString *AboutUsDescribe; //关于我们 描述
@property (nonatomic, copy) NSString *BlacklistH5; //征信-黑名单查询H5地址
@property (nonatomic, copy) NSString *TMobileH5; //征信-运营商查询H5地址
@property (nonatomic, copy) NSString *EContactH5; //征信-紧急联系人查询H5地址
@property (nonatomic, copy) NSString *ContactWXGZH; // 微信公众号
@property (nonatomic, copy) NSString *NewsH5; //神吐槽链接

@end

@interface GlobalConfigInfoModel : BaseJSONModel

@property (nonatomic, strong) GlobalConfigInfoModelInnerData *InnerData; //返回数据

@end


/**
 
 */
