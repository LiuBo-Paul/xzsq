//
//  HomePageInfoModel.m
//  DaddyLoan
//
//  Created by Sara on 18/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import "HomePageInfoModel.h"

@implementation Banners

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

@end

@implementation Bulletin

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

@end

@implementation PopupAdvert

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end

@implementation HomePageInfoModelInnerData

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end

@implementation HomePageInfoModel

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
  return YES;
}

@end
