//
//  ProductsModel.h
//  DaddyLoan
//
//  Created by Sara on 17/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol Models
@end

@interface Models : JSONModel

@property (nonatomic, copy) NSString *Pid; //产品编号
@property (nonatomic, copy) NSString *Name; //产品名称
@property (nonatomic, copy) NSString *Rate; //利率
@property (nonatomic, copy) NSString *MinLimit; //最低额度
@property (nonatomic, copy) NSString *Limit; //最高额度
@property (nonatomic, copy) NSString *FastestTime; //放款最快时间
@property (nonatomic, copy) NSString *LatestTime; //放款最慢时间
@property (nonatomic, copy) NSString *DurationStart; //放款期限最低：天
@property (nonatomic, copy) NSString *DurationEnd; //放款期限最高：天

@property (nonatomic, copy) NSString *Extend2; //扩展：logo
@property (nonatomic, copy) NSString *Extend3; //扩展：产品介绍
@property (nonatomic, copy) NSString *Extend4; //扩展：申请条件。竖线分割
@property (nonatomic, strong) NSMutableArray *Extend4Convert; //申请条件分割成的数组集合
@property (nonatomic, copy) NSString *Extend5; //暂无使用
@property (nonatomic, copy) NSString *Extend6; //暂无使用
@property (nonatomic, copy) NSString *Extend7; //暂无使用
@property (nonatomic, copy) NSString *Extend8; //暂无使用
@property (nonatomic, copy) NSString *Extend9; //暂无使用
@property (nonatomic, copy) NSString *NeedUnionLogin; //是否需要联登
@property (nonatomic, copy) NSString *ApplyStatus; //产品的可申请状态：0默认，可申请；1已申请

@end

@interface ProductsModelInnerData : JSONModel

@property (nonatomic, strong) NSMutableArray<Models> *Models; //产品集合
@property (nonatomic, assign) BOOL HasNextPage; //是否有下一页

@end

@interface ProductsModel : JSONModel

@property (nonatomic, strong) ProductsModelInnerData *InnerData; //返回数据

@end
