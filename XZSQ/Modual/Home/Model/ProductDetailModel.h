//
//  ProductDetailModel.h
//  DaddyLoan
//
//  Created by Sara on 18/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ProductDetailModelInnerData : JSONModel

@property (nonatomic, copy) NSString *Pid; //产品编号
@property (nonatomic, copy) NSString *Name; //产品名称
@property (nonatomic, copy) NSString *Rate; //利率
@property (nonatomic, copy) NSString *MinLimit; //最低额度
@property (nonatomic, copy) NSString *Limit; //最高额度
@property (nonatomic, copy) NSString *FastestTime; //放款最快时间
@property (nonatomic, copy) NSString *LatestTime; //放款最慢时间
@property (nonatomic, copy) NSString *DurationStart; //放款期限最低：天
@property (nonatomic, copy) NSString *DurationEnd; //放款期限最高：天
@property (nonatomic, copy) NSString *OpenType; //打开方式 。0(默认)：内嵌浏览器。1：原生浏览器

@property (nonatomic, copy) NSString *Extend1; //扩展：跳转连接
@property (nonatomic, copy) NSString *Extend2; //扩展：logo
@property (nonatomic, copy) NSString *Extend3; //扩展：产品介绍
@property (nonatomic, copy) NSString *Extend4; //扩展：申请条件。竖线分割
@property (nonatomic, strong) NSMutableArray *Extend4Convert; //申请条件分割成的数组集合
@property (nonatomic, copy) NSString *Extend5; //暂无使用
@property (nonatomic, copy) NSString *Extend6; //暂无使用
@property (nonatomic, copy) NSString *Extend7; //暂无使用
@property (nonatomic, copy) NSString *Extend8; //暂无使用
@property (nonatomic, copy) NSString *Extend9; //暂无使用
@property (nonatomic, copy) NSString *UnionLoginAgreeH5; //联登协议H5地址。如UnionLoginAgreeH5为空跳转到详情页，否则前端弹出联登协议框，并加载h5。在用户点击确认按钮后，再调用产品详情接口进行跳转
@property (nonatomic, copy) NSString *ApplyStatus; //产品的可申请状态：0默认，可申请；1已申请

@end

@interface ProductDetailModel : JSONModel

@property (nonatomic, strong) ProductDetailModelInnerData *InnerData; //返回数据

@end

