//
//  LoginvcodeModel.h
//  PunchCardPlanet
//
//  Created by Paul on 2019/10/17.
//  Copyright © 2018年 LiuBo. All rights reserved.
//

#import "BaseJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginvcodeModel : BaseJSONModel

@property (nonatomic, copy) NSString *ResultCode;

@end

NS_ASSUME_NONNULL_END
