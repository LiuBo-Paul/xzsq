//
//  UserInfoModel.h
//  PunchCardPlanet
//
//  Created by Paul on 2019/10/18.
//  Copyright © 2018年 LiuBo. All rights reserved.
//

#import "BaseJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserInfoModelInnerData : BaseJSONModel

@property (nonatomic, copy) NSString *Avatar;
@property (nonatomic, copy) NSString *Mobile;
@property (nonatomic, copy) NSString *NickName;
@property (nonatomic, copy) NSString *IFRealNameStatus;
@property (nonatomic, copy) NSString *MTIdNo;
@property (nonatomic, copy) NSString *MTRealName;
@property (nonatomic, copy) NSString *IFAddressBookStatus;

@end

@interface UserInfoModel : BaseJSONModel

@property (nonatomic, strong) UserInfoModelInnerData *InnerData;

+(void)clearInfo;
+(UserInfoModel *)getUserInfo;
+(void)setUserInfo:(UserInfoModel *)userInfoModel;

@end

NS_ASSUME_NONNULL_END
