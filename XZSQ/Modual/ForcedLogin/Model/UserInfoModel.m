//
//  UserInfoModel.m
//  PunchCardPlanet
//
//  Created by Paul on 2019/10/18.
//  Copyright © 2018年 LiuBo. All rights reserved.
//

#import "UserInfoModel.h"

@implementation UserInfoModelInnerData

+(BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}

@end

@implementation UserInfoModel

+(BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}

+(void)clearInfo
{
    [USERDEFAULTS removeObjectForKey:@"Avatar"];
    [USERDEFAULTS removeObjectForKey:@"Mobile"];
    [USERDEFAULTS removeObjectForKey:@"NickName"];
    [USERDEFAULTS removeObjectForKey:@"IFRealNameStatus"];
    [USERDEFAULTS removeObjectForKey:@"MTRealName"];
    [USERDEFAULTS removeObjectForKey:@"MTIdNo"];
    [USERDEFAULTS removeObjectForKey:@"IFAddressBookStatus"];
}

+(UserInfoModel *)getUserInfo
{
    UserInfoModel *model = [[UserInfoModel alloc] init];
    
    UserInfoModelInnerData *innerModel = [[UserInfoModelInnerData alloc] init];
    
    innerModel.Avatar = [USERDEFAULTS objectForKey:@"Avatar"];
    innerModel.Mobile = [USERDEFAULTS objectForKey:@"Mobile"];
    innerModel.NickName = [USERDEFAULTS objectForKey:@"NickName"];
    innerModel.IFRealNameStatus = [USERDEFAULTS objectForKey:@"IFRealNameStatus"];
    innerModel.MTRealName = [USERDEFAULTS objectForKey:@"MTRealName"];
    innerModel.MTIdNo = [USERDEFAULTS objectForKey:@"MTIdNo"];
    innerModel.IFAddressBookStatus = [USERDEFAULTS objectForKey:@"IFAddressBookStatus"];
    
    model.InnerData = innerModel;
    
    return model;
}

+(void)setUserInfo:(UserInfoModel *)userInfoModel {
    if(userInfoModel && userInfoModel.InnerData) {
        NSMutableString *avatar = [[NSMutableString alloc] initWithString:userInfoModel.InnerData.Avatar];
        [USERDEFAULTS setObject:avatar forKey:@"Avatar"];
        [USERDEFAULTS setObject:userInfoModel.InnerData.Mobile forKey:@"Mobile"];
        [USERDEFAULTS setObject:userInfoModel.InnerData.NickName forKey:@"NickName"];
        [USERDEFAULTS setObject:userInfoModel.InnerData.IFRealNameStatus forKey:@"IFRealNameStatus"];
        [USERDEFAULTS setObject:userInfoModel.InnerData.MTRealName forKey:@"MTRealName"];
        [USERDEFAULTS setObject:userInfoModel.InnerData.MTIdNo forKey:@"MTIdNo"];
        [USERDEFAULTS setObject:userInfoModel.InnerData.IFAddressBookStatus forKey:@"IFAddressBookStatus"];
    }
}

@end
