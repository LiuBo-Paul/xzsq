//
//  VerifyimgcodeModel.h
//  PunchCardPlanet
//
//  Created by Paul on 2019/10/17.
//  Copyright © 2018年 LiuBo. All rights reserved.
//

#import "BaseJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface VerifyimgcodeModelInnerData : BaseJSONModel

@property (nonatomic, copy) NSString *Code;

@end

@interface VerifyimgcodeModel : BaseJSONModel

@property (nonatomic, strong) VerifyimgcodeModelInnerData *InnerData;

@end

NS_ASSUME_NONNULL_END
