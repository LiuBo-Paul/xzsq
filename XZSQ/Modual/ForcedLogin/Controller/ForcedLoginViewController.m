//
//  ForcedLoginViewController.m
//  Loan
//
//  Created by Paul on 2019/9/30.
//  Copyright © 2018年 Qinghu. All rights reserved.
//

#import "ForcedLoginViewController.h"
#import "VerifyimgcodeModel.h"
#import "UserInfoModel.h"
#import "ESTimer.h"
#import "FSCacheManager.h"

@interface ForcedLoginViewController ()<UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;
@property (strong, nonatomic) IBOutlet UITextField *vCodeTextField;
@property (strong, nonatomic) IBOutlet UILabel *requestCodeLabel;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;

@property (strong, nonatomic) ESTimer *timer;
@property (nonatomic, assign) BOOL isVertifyOutOfDate; //验证码是否超时, YES-超时, NO-未超时

@end

@implementation ForcedLoginViewController

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.timer stopTimerWithTimerType:(ESTimerTypeGCD) stopTimerBlock:^{
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.loginBtn.userInteractionEnabled = NO;
    [self.loginBtn setBackgroundColor:ItemColorFromRGB(0x999999)];
    [self.phoneTextField becomeFirstResponder];
    self.phoneTextField.delegate = self;
    [self.phoneTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    self.vCodeTextField.delegate = self;
    [self.vCodeTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    self.view.backgroundColor = ItemColorFromRGB(0xf2f2f2);
    self.isVertifyOutOfDate = YES;
    UITapGestureRecognizer *tapCode = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCodeAction:)];
    tapCode.delegate = self;
    [self.requestCodeLabel addGestureRecognizer:tapCode];
    self.requestCodeLabel.userInteractionEnabled = NO;
    self.requestCodeLabel.textColor = MainColor;
}

-(void)tapCodeAction:(UITapGestureRecognizer *)tap {
    if(self.phoneTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入手机号"];
        return;
    }
    if(![[General_Tool GT_sharedInstance] GT_isPhoneNum:self.phoneTextField.text]) {
        self.loginBtn.userInteractionEnabled = NO;
        [self.loginBtn setBackgroundColor:ItemColorFromRGB(0x999999)];
        [SVProgressHUD showErrorWithStatus:@"请输入合法手机号！"];
        [self.phoneTextField becomeFirstResponder];
        return;
    }
    [USERDEFAULTS setObject:self.phoneTextField.text forKey:@"LoginMobile"];
    // 拿手机号请求短信验证码
    [SVProgressHUD showWithStatus:@"验证码发送中..."];
    WEAKSELF
    [[RequestMannager sharedInstance] postSendLoginByVCodeWithMobile:self.phoneTextField.text completionBlock:^(id responseObject) {
        BaseJSONModel *model = [[BaseJSONModel alloc] initWithDictionary:responseObject error:nil];
        NSString *state = [NSString stringWithFormat:@"%@", model.Status];
        if([state isEqual:@"1"]) {
            [SVProgressHUD showSuccessWithStatus:model.Message];
            [weakSelf.vCodeTextField becomeFirstResponder];
            STRONGSELF
            [weakSelf.timer startTimerWithTimerType:(ESTimerTypeGCD) startTimerBlock:^(CGFloat seconds) {
                if(seconds >= 60) {
                    strongSelf.requestCodeLabel.userInteractionEnabled = YES;
                    strongSelf.requestCodeLabel.textColor = MainSelectedColor;
                    strongSelf.requestCodeLabel.text = @"重新获取";
                } else {
                    strongSelf.requestCodeLabel.userInteractionEnabled = NO;
                    strongSelf.requestCodeLabel.textColor = MainColor;
                    strongSelf.requestCodeLabel.text = [NSString stringWithFormat:@"%.0lf", 60-seconds];
                }
            }];
        } else {
            [weakSelf.timer stopTimerWithTimerType:(ESTimerTypeGCD) stopTimerBlock:^{
                
            }];
            weakSelf.requestCodeLabel.text = @"重新获取";
            weakSelf.requestCodeLabel.textColor = MainSelectedColor;
            weakSelf.requestCodeLabel.userInteractionEnabled = YES;
            [SVProgressHUD showErrorWithStatus:model.Message];
        }
    } failBlock:^(NSError *error) {
        [weakSelf.timer stopTimerWithTimerType:(ESTimerTypeGCD) stopTimerBlock:^{
            
        }];
        weakSelf.requestCodeLabel.text = @"重新获取";
        weakSelf.requestCodeLabel.textColor = MainSelectedColor;
        weakSelf.requestCodeLabel.userInteractionEnabled = YES;
        [SVProgressHUD showErrorWithStatus:@"获取失败，请稍后重试！"];
    }];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder)to:nil from:nil forEvent:nil];
}

-(void)textFieldDidChange:(UITextField *)textField
{
    if(textField.tag == 6500) {
        if(textField.text.length >= 12) {
            textField.text = [textField.text substringToIndex:11];
            return;
        }
        if(textField.text.length == 11) {
            // 手机号输入完成
            // 校验手机号合法性
            if(![[General_Tool GT_sharedInstance] GT_isPhoneNum:self.phoneTextField.text]) {
                // 手机号不合法
                self.requestCodeLabel.userInteractionEnabled = NO;
                self.requestCodeLabel.textColor = MainColor;
                [SVProgressHUD showErrorWithStatus:@"请输入合法手机号！"];
                [self.phoneTextField becomeFirstResponder];
                return;
            } else {
                // 手机号合法，请求图形验证码并显示验证码输入框
                [USERDEFAULTS setObject:self.phoneTextField.text forKey:@"LoginMobile"];
                self.requestCodeLabel.userInteractionEnabled = YES;
                self.requestCodeLabel.textColor = MainSelectedColor;
            }
        } else {
            // 手机号不合法，将不能进行下一步
            self.requestCodeLabel.userInteractionEnabled = NO;
            self.requestCodeLabel.textColor = MainColor;
        }
        
    } else if(textField.tag == 6600) {
        if(textField.text.length >= 6) {
            textField.text = [textField.text substringToIndex:6];
        }
        if(textField.text.length > 0) {
            //开始输入图形验证码，开启下一步按钮
            self.loginBtn.userInteractionEnabled = YES;
            [self.loginBtn setBackgroundColor:MainSelectedColor];
        } else {
            self.loginBtn.userInteractionEnabled = NO;
            [self.loginBtn setBackgroundColor:ItemColorFromRGB(0x999999)];
        }
    }
}

- (IBAction)loginBtnAction:(UIButton *)sender
{
    if(self.phoneTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入手机号"];
        return;
    }
    if(![[General_Tool GT_sharedInstance] GT_isPhoneNum:self.phoneTextField.text]) {
        self.loginBtn.userInteractionEnabled = NO;
        [self.loginBtn setBackgroundColor:ItemColorFromRGB(0x999999)];
        [SVProgressHUD showErrorWithStatus:@"请输入合法手机号！"];
        [self.phoneTextField becomeFirstResponder];
        return;
    }
    if(self.vCodeTextField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入短信验证码"];
        return;
    }
    [SVProgressHUD showWithStatus:@"登录中..."];
    [[RequestMannager sharedInstance] postLoginByVCodeWithMobile:self.phoneTextField.text
                                                           vcode:self.vCodeTextField.text
                                                 completionBlock:^(id responseObject) {
                                                     BaseJSONModel *model = [[BaseJSONModel alloc] initWithDictionary:responseObject error:nil];
                                                     NSString *state = [NSString stringWithFormat:@"%@", model.Status];
                                                     if([state isEqual:@"1"]) {
                                                         if(responseObject && responseObject[@"InnerData"][@"NativeId"]) {
                                                             SETUSERDEFAULTS(responseObject[@"InnerData"][@"NativeId"], NativeKey);
                                                         }
                                                         [SVProgressHUD showSuccessWithStatus:@"登录成功"];
                                                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                             [APPDELEGATE resetRoot_HomeViewController];
                                                         });
                                                         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                                                             [[RequestMannager sharedInstance] requestAccountInfoWithCompletionBlock:^(id responseObject) {
                                                                 UserInfoModel *model = [[UserInfoModel alloc] initWithDictionary:responseObject error:nil];
                                                                 [UserInfoModel setUserInfo:model];
                                                                     [[FSCacheManager sharedManager] saveData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[UserInfoModel getUserInfo].InnerData.Avatar]] name:@"AvatarImage" cacheType:(FSCacheTypeImage) successBlock:^(NSString * _Nullable message, NSData * _Nullable data, NSString * _Nullable path) {
                                                                         
                                                                     } failBlock:^(NSString * _Nullable message) {
                                                                         
                                                                     }];
                                                             } failBlock:^(NSError *error) {
                                                                 
                                                             }];
                                                         });

                                                     } else {
                                                         [SVProgressHUD showErrorWithStatus:model.Message];
                                                     }
                                                 } failBlock:^(NSError *error) {
                                                     [SVProgressHUD showErrorWithStatus:@"请求失败，请稍后重试"];
                                                 }];
}

-(ESTimer *)timer {
    if(!_timer) {
        _timer = [ESTimer new];
    }
    return _timer;
}

@end
