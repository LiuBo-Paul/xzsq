//
//  AL_MyTableViewCell.h
//  XZSQ
//
//  Created by LiuBo on 2019/7/23.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AL_MyTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet UIView *borderView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *detailLabel;
@property (strong, nonatomic) IBOutlet UIButton *idenBtn;

@end

NS_ASSUME_NONNULL_END
