//
//  AL_IdentifierTableViewCell.h
//  XZSQ
//
//  Created by LiuBo on 2019/7/22.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    AL_IdentifierTableViewCellInput,
    AL_IdentifierTableViewCellShow,
    AL_IdentifierTableViewCellSelecte,
} AL_IdentifierTableViewCellType;

@interface AL_IdentifierTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *i_nameLabel;
@property (strong, nonatomic) IBOutlet UITextField *i_contentTextField;

@property (strong, nonatomic) IBOutlet UILabel *s_nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *s_subNameLabel;

@property (strong, nonatomic) IBOutlet UILabel *sel_nameLabel;
@property (strong, nonatomic) IBOutlet UIButton *sel_actionBtn;

@end

NS_ASSUME_NONNULL_END
