//
//  AL_MyViewController.m
//  XZSQ
//
//  Created by LiuBo on 2019/7/23.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "AL_MyViewController.h"
#import "AL_MyTableViewCell.h"
#import "AL_IdentityViewController.h"

@interface AL_MyViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *dataArray;

@end

@implementation AL_MyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.dataArray = @[@{@"image":@"al_ic_zm", @"name":@"芝麻授权", @"detail":@"授权后最高可借款额度2000元", @"isIdentified":[[NSUserDefaults standardUserDefaults] objectForKey:@"芝麻授权"]?[[NSUserDefaults standardUserDefaults] objectForKey:@"芝麻授权"]:@"0"},
                       @{@"image":@"al_ic_gr", @"name":@"个人信息", @"detail":@"保证个人信息有效", @"isIdentified":[[NSUserDefaults standardUserDefaults] objectForKey:@"个人信息"]?[[NSUserDefaults standardUserDefaults] objectForKey:@"个人信息"]:@"0"},
                       @{@"image":@"al_ic_sk", @"name":@"收款银行卡", @"detail":@"保证银行卡信息有效", @"isIdentified":[[NSUserDefaults standardUserDefaults] objectForKey:@"收款银行卡"]?[[NSUserDefaults standardUserDefaults] objectForKey:@"收款银行卡"]:@"0"},
                       @{@"image":@"al_ic_sj", @"name":@"手机运营商", @"detail":@"认证后有助于加快审核速度", @"isIdentified":[[NSUserDefaults standardUserDefaults] objectForKey:@"手机运营商"]?[[NSUserDefaults standardUserDefaults] objectForKey:@"手机运营商"]:@"0"}];
    [self.tableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AL_MyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AL_MyTableViewCell"];
    if(!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"AL_MyTableViewCell" owner:self options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *dic = self.dataArray[indexPath.row];
    cell.borderView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.nameLabel.text = dic[@"name"];
    cell.iconImageView.image = [UIImage imageNamed:dic[@"image"]];
    cell.detailLabel.text = dic[@"detail"];
    cell.idenBtn.userInteractionEnabled = NO;
    [cell.idenBtn setTitle:[NSString stringWithFormat:@"%@", [dic[@"isIdentified"] isEqual:@"0"]?@"未认证":@"已认证"] forState:(UIControlStateNormal)];
    cell.idenBtn.layer.borderColor = [UIColor colorWithRed:72/255.0 green:141/255.0 blue:227/255.0 alpha:1.0].CGColor;
    cell.idenBtn.layer.borderWidth = 1;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if([[NSString stringWithFormat:@"%@", self.dataArray[indexPath.row][@"isIdentified"]] isEqual:@"1"]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"该项已认证，无需重新认证！" preferredStyle:(UIAlertControllerStyleAlert)];
        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        if(indexPath.row == 0) {
            [self presentToViewControllerWithIndex:indexPath.row];
        } else {
            if([[NSString stringWithFormat:@"%@", self.dataArray[indexPath.row-1][@"isIdentified"]] isEqual:@"1"]) {
                [self presentToViewControllerWithIndex:indexPath.row];
            } else {
                NSString *name = @"";
                for (int i = 0; i < self.dataArray.count; i++) {
                    if([[NSString stringWithFormat:@"%@", self.dataArray[i][@"isIdentified"]] isEqual:@"0"]) {
                        name = self.dataArray[i][@"name"];
                        break;
                    }
                }
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:[NSString stringWithFormat:@"请先认证%@", name] preferredStyle:(UIAlertControllerStyleAlert)];
                [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    
                }]];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
    }
}

-(void)presentToViewControllerWithIndex:(NSInteger)index {
    TableViewShowType type;
    switch (index) {
        case 0: {
            // 芝麻信用
            type = TableViewShowTypeCredit;
        } break;
        case 1: {
            // 个人信息
            type = TableViewShowTypeSelf;
        } break;
        case 2: {
            // 收款银行卡
            type = TableViewShowTypeBank;
        } break;
        case 3: {
            // 手机运营商
            type = TableViewShowTypePhone;
        } break;
        default:
            type = TableViewShowTypeDefault;
            break;
    }
    AL_IdentityViewController *ide = [[AL_IdentityViewController alloc] initWithNibName:@"AL_IdentityViewController" bundle:[NSBundle mainBundle]];
    ide.showType = type;
    [self presentViewController:ide animated:YES completion:nil];
}

-(NSArray *)dataArray {
    if(!_dataArray) {
        _dataArray = [NSArray new];
    }
    return _dataArray;
}

@end
