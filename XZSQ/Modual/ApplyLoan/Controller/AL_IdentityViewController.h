//
//  AL_IdentityViewController.h
//  XZSQ
//
//  Created by LiuBo on 2019/7/22.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    TableViewShowTypeDefault,
    TableViewShowTypeCredit,
    TableViewShowTypeSelf,
    TableViewShowTypeBank,
    TableViewShowTypePhone,
} TableViewShowType;

@interface AL_IdentityViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *navTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *applyBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *processLineViewTrailCon;

@property (nonatomic, assign) TableViewShowType showType;

@end

NS_ASSUME_NONNULL_END
