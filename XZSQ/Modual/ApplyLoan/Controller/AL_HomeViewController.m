//
//  AL_HomeViewController.m
//  XZSQ
//
//  Created by LiuBo on 2019/7/22.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "AL_HomeViewController.h"

#import "UDIDSafeAuthEngine.h"
#import "UDIDSafeDataDefine.h"

static NSString *UDAUTHKEY = @"93f22dd3-84d5-44d6-a30f-9974329f6a4b"; // 商户 authkey
static NSString *UDSECKEY = @"f5108d8b-86b8-427f-ad2d-c22f424bcfb2"; // 商户 secretkey

typedef enum : NSUInteger {
    PickViewTagMoney = 200,
    PickViewTagDay = 300,
} PickViewTag;

@interface AL_HomeViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UDIDSafeAuthDelegate>
{
    NSString *_authorized_partner_order_id;
    UIImage *_choseImage;
}

@property (nonatomic, strong) NSString * partnerOrderId;
@property (nonatomic, strong) NSString * signTime;

@end

static CGFloat PickerSepLineWidth = 150; // 分%ld线(long)宽度
static CGFloat PickerRowheight = 30; // 行高
static CGFloat PickerMoneyRowCount = 30; // 申请金额行数
static NSInteger TotalMoney = 30000; // 钱总数
static NSInteger MoneyMultiple = 1000; // 钱倍数
static NSInteger MinDay = 15; // 申请最小天数
static NSInteger MaxDay = 30; // 申请最大天数

static BOOL canLoan = NO;

@implementation AL_HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    self.totalMoneyLabel.text = [NSString stringWithFormat:@"%ld", (long)TotalMoney];
    //test
//    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"芝麻授权"];
//    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"个人信息"];
//    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"收款银行卡"];
//    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"手机运营商"];
//    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"isApplyMoney"];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    // 人脸识别
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970] *1000;
    NSString *outUserId = [NSString stringWithFormat:@"I%.0f%ld", time, (long)[self createRandomNumber:1000 to:9999]];
    _authorized_partner_order_id = outUserId;
    UDIDSafeAuthEngine * engine = [[UDIDSafeAuthEngine alloc]init];
    engine.delegate = self;
    engine.authKey = UDAUTHKEY;
    engine.outOrderId = outUserId;
    engine.notificationUrl = @"http://baidu.com";
    engine.showInfo = YES;
    engine.isExposureDetection = YES;
    [engine startIdSafeFaceAuthInViewController:self];
}

-(NSInteger)createRandomNumber:(NSInteger)fromNum to:(NSInteger)toNum {
    return (arc4random() % toNum) + fromNum;
}

#pragma mark - UDIDSafeAuthDelegate
- (void)idSafeAuthFinishedWithResult:(UDIDSafeAuthResult)result UserInfo:(id)userInfo{
    NSLog(@"OC authFinish");
    NSLog(@"userInfo: %@", userInfo);
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"芝麻授权"] isEqual:@"1"] &&
       [[[NSUserDefaults standardUserDefaults] objectForKey:@"个人信息"] isEqual:@"1"] &&
       [[[NSUserDefaults standardUserDefaults] objectForKey:@"收款银行卡"] isEqual:@"1"] &&
       [[[NSUserDefaults standardUserDefaults] objectForKey:@"手机运营商"] isEqual:@"1"]) {
        canLoan = YES;
    } else {
        canLoan = NO;
    }
    if(![[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"isApplyMoney"]] isEqual:@"1"]) {
        [self.applyBtn addTarget:self action:@selector(applyBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    } else {
        [self.applyBtn setTitle:@"已申请" forState:(UIControlStateNormal)];
        self.applyBtn.userInteractionEnabled = NO;
        [self.applyBtn setBackgroundColor:[UIColor lightGrayColor]];
        self.moneyPicker.userInteractionEnabled = NO;
        self.dayPicker.userInteractionEnabled = NO;
    }

}

-(void)applyBtnAction:(UIButton *)sender {
    if(canLoan) {
        [SVProgressHUD showWithStatus:@"申请提交中... ..."];
        WEAKSELF
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"申请成功！客服人员随后会与您取得联系，请耐心等待！" preferredStyle:(UIAlertControllerStyleAlert)];
            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"isApplyMoney"];
                weakSelf.applyBtn.userInteractionEnabled = NO;
                [weakSelf.applyBtn setTitle:@"已申请" forState:(UIControlStateNormal)];
                weakSelf.applyBtn.userInteractionEnabled = NO;
                [weakSelf.applyBtn setBackgroundColor:[UIColor lightGrayColor]];
                weakSelf.moneyPicker.userInteractionEnabled = NO;
                weakSelf.dayPicker.userInteractionEnabled = NO;

            }]];
            [weakSelf presentViewController:alertController animated:YES completion:nil];
        });
    } else {
        [SVProgressHUD showErrorWithStatus:@"请先完善资料"];
        [APPDELEGATE directToIdent];
    }
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(pickerView.tag == PickViewTagMoney) {
        // 申请金额
        return PickerMoneyRowCount;
    } else {
        // 申请天数
        return MaxDay - MinDay + 1;
    }
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(-5, 0, pickerView.width+10, pickerView.height)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:72/255.0 green:141/255.0 blue:227/255.0 alpha:1.0];
    label.font = [UIFont systemFontOfSize:13];
    if(pickerView.tag == PickViewTagMoney) {
        // 申请金额
        label.text = [NSString stringWithFormat:@"%@元", [self getMultipleMoneyWithRow:row]];
    } else {
        // 申请天数
        label.text = [NSString stringWithFormat:@"%d", row + MinDay];
    }
    [self changeBankPickerSeparaterLineWithPickerView:pickerView];
    return label;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return pickerView.width;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return PickerRowheight;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
}

// 获取钱倍数
-(NSString *)getMultipleMoneyWithRow:(NSInteger)row {
    return [NSString stringWithFormat:@"%d", (row+1)*MoneyMultiple];
}

// 修改pickerview分割线样式
- (void)changeBankPickerSeparaterLineWithPickerView:(UIPickerView *)pickerView{
    for (UIView *separatorLine in pickerView.subviews) {
        if (separatorLine.frame.size.height < 1) {
            separatorLine.backgroundColor = [UIColor colorWithRed:72/255.0 green:141/255.0 blue:227/255.0 alpha:1.0];
            separatorLine.width = PickerSepLineWidth;
            separatorLine.left = (pickerView.width - PickerSepLineWidth)/2.0;
        }
    }
}

@end
