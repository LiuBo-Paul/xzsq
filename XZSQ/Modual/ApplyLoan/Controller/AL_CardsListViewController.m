//
//  AL_CardsListViewController.m
//  XZSQ
//
//  Created by LiuBo on 2019/7/22.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "AL_CardsListViewController.h"

@interface AL_CardsListViewController ()

@end

@implementation AL_CardsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
}

@end
