//
//  AL_HomeViewController.h
//  XZSQ
//
//  Created by LiuBo on 2019/7/22.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AL_HomeViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *totalMoneyLabel;
@property (strong, nonatomic) IBOutlet UIPickerView *moneyPicker;
@property (strong, nonatomic) IBOutlet UIPickerView *dayPicker;
@property (strong, nonatomic) IBOutlet UIButton *applyBtn;

@end

NS_ASSUME_NONNULL_END
