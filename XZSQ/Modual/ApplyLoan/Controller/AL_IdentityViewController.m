//
//  AL_IdentityViewController.m
//  XZSQ
//
//  Created by LiuBo on 2019/7/22.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "AL_IdentityViewController.h"
#import "AL_IdentifierTableViewCell.h"

@interface AL_IdentityViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *dataArray;

@end

static CGFloat CellHeight = 52;

@implementation AL_IdentityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)applyBtnAction:(UIButton *)sender {
    BOOL canSubmmit = YES;
    for(int i = 0; i < self.dataArray.count; i++) {
        if(((AL_IdentifierTableViewCellType)[self.dataArray[i][@"cellType"] integerValue]) == AL_IdentifierTableViewCellInput) {
            AL_IdentifierTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            if(cell.i_contentTextField.text.length == 0) {
                canSubmmit = NO;
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:[NSString stringWithFormat:@"%@", cell.i_contentTextField.placeholder] preferredStyle:(UIAlertControllerStyleAlert)];
                [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
                    [cell.i_contentTextField becomeFirstResponder];
                }]];
                [self presentViewController:alertController animated:YES completion:^{
                    
                }];
                break;
            }
        }
    }
    if(!canSubmmit) {
        return;
    }
    NSString *name = @"";
    switch (self.showType) {
        case TableViewShowTypeCredit:{
            name = @"芝麻授权";
        } break;
        case TableViewShowTypeSelf:{
            name = @"个人信息";
        } break;
        case TableViewShowTypeBank:{
            name = @"收款银行卡";
        } break;
        case TableViewShowTypePhone:{
            name = @"手机运营商";
        } break;
        default:
            break;
    }
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:name];
    [SVProgressHUD showWithStatus:@"提交中... ..."];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD showSuccessWithStatus:@"提交成功!"];
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.dataArray.count == 0) {
        return [UITableViewCell new];
    } else {
        AL_IdentifierTableViewCellType type = (AL_IdentifierTableViewCellType)[self.dataArray[indexPath.row][@"cellType"] integerValue];
        switch (type) {
            case AL_IdentifierTableViewCellShow: {
                AL_IdentifierTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AL_IdentifierTableViewCellShow"];
                if(!cell) {
                    cell = [[NSBundle mainBundle] loadNibNamed:@"AL_IdentifierTableViewCell" owner:self options:nil][1];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                cell.s_nameLabel.text = self.dataArray[indexPath.row][@"name"];
                cell.s_subNameLabel.text = self.dataArray[indexPath.row][@"subTitle"];
                return cell;
            } break;
            case AL_IdentifierTableViewCellInput: {
                AL_IdentifierTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AL_IdentifierTableViewCellInput"];
                if(!cell) {
                    cell = [[NSBundle mainBundle] loadNibNamed:@"AL_IdentifierTableViewCell" owner:self options:nil][0];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                cell.i_nameLabel.text = self.dataArray[indexPath.row][@"name"];
                cell.i_contentTextField.placeholder = [NSString stringWithFormat:@"请输入%@", self.dataArray[indexPath.row][@"name"]];
                return cell;
            } break;
            case AL_IdentifierTableViewCellSelecte: {
                AL_IdentifierTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AL_IdentifierTableViewCellSelecte"];
                if(!cell) {
                    cell = [[NSBundle mainBundle] loadNibNamed:@"AL_IdentifierTableViewCell" owner:self options:nil][2];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                cell.sel_nameLabel.text = self.dataArray[indexPath.row][@"name"];
                [cell.sel_actionBtn setTitle: [NSString stringWithFormat:@"%@", self.dataArray[indexPath.row][@"subTitle"]] forState:(UIControlStateNormal)];
                [cell.sel_actionBtn addTarget:self action:@selector(cellAction:) forControlEvents:(UIControlEventTouchUpInside)];
                cell.sel_actionBtn.tag = 900 + indexPath.row;
                return cell;
            } break;
        }
    }
}

-(void)cellAction:(UIButton *)sender {
    NSInteger index = sender.tag - 900;
    if(index < self.dataArray.count) {
        NSArray *arr = [NSArray arrayWithArray:self.dataArray[index][@"SelecteArr"]];
        if(arr.count > 0) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"请选择%@", self.dataArray[index][@"name"]] message:@"" preferredStyle:(UIAlertControllerStyleAlert)];
            for (int i = 0; i < arr.count; i++) {
                [alertController addAction:[UIAlertAction actionWithTitle:arr[i] style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    [sender setTitle:arr[i] forState:(UIControlStateNormal)];
                    NSMutableArray *mArr = [NSMutableArray arrayWithArray:self.dataArray];
                    NSMutableDictionary *mDic = [NSMutableDictionary dictionaryWithDictionary:mArr[index]];
                    [mDic setObject:arr[i] forKey:@"subTitle"];
                    mArr[index] = mDic;
                    self.dataArray = [mArr copy];
                }]];
            }
            [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [self presentViewController:alertController animated:YES completion:^{
                
            }];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CellHeight;
}

-(void)setShowType:(TableViewShowType)showType {
    _showType = showType;
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.processLineViewTrailCon.constant = (4-showType)*(ScreenWidth/4);
        switch (showType) {
            case TableViewShowTypeCredit: {
                weakSelf.dataArray = @[@{@"name":@"支付宝账号", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""},
                                       @{@"name":@"芝麻分", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""},
                                       @{@"name":@"真实姓名", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""}];
                weakSelf.navTitleLabel.text = @"个人信息";
                weakSelf.subTitleLabel.text = @"芝麻授权";
            } break;
            case TableViewShowTypeSelf: {
                weakSelf.dataArray = @[@{@"name":@"直系亲属", @"cellType":@(AL_IdentifierTableViewCellSelecte), @"subTitle":@"父亲", @"SelecteArr":@[@"父亲", @"母亲"]},
                                       @{@"name":@"姓名", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""},
                                       @{@"name":@"手机号", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""},
                                       @{@"name":@"同事朋友", @"cellType":@(AL_IdentifierTableViewCellSelecte), @"subTitle":@"同事", @"SelecteArr":@[@"同事", @"朋友"]},
                                       @{@"name":@"姓名", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""},
                                       @{@"name":@"手机号", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""}];
                weakSelf.navTitleLabel.text = @"个人信息";
                weakSelf.subTitleLabel.text = @"紧急联系人";
            } break;
            case TableViewShowTypeBank: {
                weakSelf.dataArray = @[@{@"name":@"银行及支行名称", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""},
                                       @{@"name":@"姓名", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""},
                                       @{@"name":@"银行卡号", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""},
                                       @{@"name":@"预留手机号", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""}];
                weakSelf.navTitleLabel.text = @"个人信息";
                weakSelf.subTitleLabel.text = @"收款银行卡";
            } break;
            case TableViewShowTypePhone: {
                weakSelf.dataArray = @[@{@"name":@"手机号", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""},
                                       @{@"name":@"姓名", @"cellType":@(AL_IdentifierTableViewCellInput), @"subTitle":@""}];
                weakSelf.navTitleLabel.text = @"个人信息";
                weakSelf.subTitleLabel.text = @"手机运营商";
            } break;
            default:
                weakSelf.dataArray = @[];
                break;
        }
        [weakSelf.tableView reloadData];
    });
}

-(NSArray *)dataArray {
    if(!_dataArray) {
        _dataArray = [NSArray new];
    }
    return _dataArray;
}

@end
