//
//  AL_MyViewController.h
//  XZSQ
//
//  Created by LiuBo on 2019/7/23.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AL_MyViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
