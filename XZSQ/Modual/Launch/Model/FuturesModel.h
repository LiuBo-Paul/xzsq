//
//  FuturesModel.h
//  GH_ToolBarFrame
//
//  Created by 青弧 on 2017/9/8.
//  Copyright © 2017年 秦国华. All rights reserved.
//

#import "JSONModel.h"

@protocol FuturesHomeModel @end;
@interface FuturesHomeModel : JSONModel

@property(copy,nonatomic)NSString <Optional> *Url;

@end

@interface FuturesModel : JSONModel

@property(assign,nonatomic)BOOL Result;
@property(assign,nonatomic)NSInteger Status;
@property(copy,nonatomic)NSString <Optional> *Message;
@property(copy,nonatomic)NSString <Optional> *ResultCode;
@property(copy,nonatomic)FuturesHomeModel *InnerData;

@end


