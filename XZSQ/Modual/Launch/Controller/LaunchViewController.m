//
//  LaunchViewController.m
//  XZSQ
//
//  Created by Paul on 2019/12/22.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import "LaunchViewController.h"
#import "FuturesModel.h"
#import "FuturesViewController.h"
#import <Contacts/Contacts.h>

@interface LaunchViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *launchImageView;
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong, nonatomic) IBOutlet UIButton *startBtn;

@end

@implementation LaunchViewController

- (IBAction)startBtnAction:(UIButton *)sender {
    [self checkNetworkState];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.launchImageView.image = [[General_Tool GT_sharedInstance] GT_getLaunchImage];
    self.versionLabel.text = [NSString stringWithFormat:@"v %@", AppVersion];
    if([[General_Tool GT_sharedInstance] GT_isFirstLoad]) {
        //第一次打开APP
        self.startBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        self.startBtn.hidden = NO;
        [APPDELEGATE resetRoot_ForceLoginViewController];
    } else {
        //非第一次打开APP
        self.startBtn.hidden = YES;
        [APPDELEGATE resetRoot_HomeViewController];
        WEAKSELF
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [weakSelf checkNetworkState];
        });
    }
}

-(void)checkNetworkState {
    WEAKSELF
    // 检测网络状态
    [[General_Tool GT_sharedInstance] GT_AFNReachabilitynetworkBlock:^(AFNetworkReachabilityStatus status) {
        STRONGSELF
        dispatch_async(dispatch_get_main_queue(), ^{
            [strongSelf checkUpdate];
        });
    } noNetworkBlock:^{
        [SVProgressHUD showErrorWithStatus:@"网络较差！"];
    }];
}

static BOOL networkIsGood = NO;
static BOOL canReSetRoot = YES;

// 检查更新
- (void)checkUpdate {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(!networkIsGood) {
            canReSetRoot = NO;
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"当前网络较差，请检查网络！" preferredStyle:(UIAlertControllerStyleAlert)];
            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                [APPDELEGATE resetRoot_ForceLoginViewController];
            }]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    });
    dispatch_async(dispatch_get_main_queue(), ^{
        [[RequestMannager sharedInstance] checkVersionUpdateCompletionBlock:^(id responseObject) {
            networkIsGood = YES;
            if(!canReSetRoot) {
                return;
            }
            BaseJSONModel *model = [[BaseJSONModel alloc] initWithDictionary:responseObject error:nil];
            if([model.Status isEqual:@"1"]) {
                NSDictionary *dic = responseObject[@"InnerData"];
                NSString *needUpdate = [NSString stringWithFormat:@"%@", dic[@"needUpdate"]];
                if(![needUpdate isEqual:@"1"]) {
                    //强制更新
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"更新提示" message:@"发现新版本，请安装更新？取消将退出APP" preferredStyle:(UIAlertControllerStyleAlert)];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                        NSString *downloadUrl = [NSString stringWithFormat:@"%@", dic[@"DownloadUrl"]];
                        if(downloadUrl.length > 0 && [downloadUrl rangeOfString:@"http"].length > 0) {
                            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:downloadUrl]]) {
                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:downloadUrl] options:@{} completionHandler:^(BOOL success) {
                                    
                                }];
                            } else {
                                [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"无法下载app！%@", downloadUrl]];
                                NSLog(@"无法下载app！%@", downloadUrl);
                            }
                        } else {
                            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"下载地址有误！%@", downloadUrl]];
                            NSLog(@"下载地址有误！%@", downloadUrl);
                        }
                    }]];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
                        exit(0);
                    }]];
                    [APPDELEGATE.window.rootViewController presentViewController:alertController animated:YES completion:nil];
                } else {
                    //非强制更新
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"更新提示" message:@"发现新版本，请安装更新？" preferredStyle:(UIAlertControllerStyleAlert)];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                        NSString *downloadUrl = [NSString stringWithFormat:@"%@", dic[@"DownloadUrl"]];
                        if(downloadUrl.length > 0 && [downloadUrl rangeOfString:@"http"].length > 0) {
                            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:downloadUrl]]) {
                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:downloadUrl] options:@{} completionHandler:^(BOOL success) {
                                    
                                }];
                            } else {
                                [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"无法下载app！%@", downloadUrl]];
                                NSLog(@"无法下载app！%@", downloadUrl);
                            }
                        } else {
                            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"下载地址有误！%@", downloadUrl]];
                            NSLog(@"下载地址有误！%@", downloadUrl);
                        }
                    }]];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
//                        [APPDELEGATE resetRoot_ForceLoginViewController];
                    }]];
                    [APPDELEGATE.window.rootViewController presentViewController:alertController animated:YES completion:nil];
                }
            } else {
                // 无需更新
//                [APPDELEGATE resetRoot_HomeViewController];
            }
        } failBlock:^(NSError *error) {
//            [APPDELEGATE resetRoot_ForceLoginViewController];
        }];
    });
}

@end
