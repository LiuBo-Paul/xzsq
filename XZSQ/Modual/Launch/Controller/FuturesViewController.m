//
//  FuturesViewController.m
//  GH_ToolBarFrame
//
//  Created by 青弧 on 2017/9/8.
//  Copyright © 2017年 秦国华. All rights reserved.
//

#import "FuturesViewController.h"
#import <WebKit/WebKit.h>
@interface FuturesViewController ()<WKUIDelegate,WKNavigationDelegate,UIAlertViewDelegate>

@property(strong,nonatomic)WKWebView *webView;

@property(strong,nonatomic)UIImageView *launchImage;

@property(copy,nonatomic)NSString *updateVersionUrl;

@property(strong,nonatomic)UILabel *infoLabel;

@end

@implementation FuturesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self createWebView];
}

- (void)createWebView
{
    self.webView = [[WKWebView alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height-20.f)];
    self.webView.UIDelegate = self;
    self.webView.navigationDelegate = self;
    self.webView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.webView];
    
    //先显示启动页，等加载数据完成后在现实webView
    [self showLaunchImage];

    //无数据的情况下显示
    self.infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 50.f)];
    
    self.infoLabel.text = @"由于网络原因界面丢失，请检查网络，重新打开app";
    self.infoLabel.textAlignment = NSTextAlignmentCenter;
    self.infoLabel.textColor = [[UIColor darkTextColor]colorWithAlphaComponent:0.8];
    self.infoLabel.font = kSysFontSize(14);
    self.infoLabel.hidden = YES;
    self.infoLabel.numberOfLines = 0;
    self.infoLabel.center = self.view.center;
    [self.view addSubview:self.infoLabel];
    
    
    [self setWebViewShowsVerticalAndHorizontal];
    [self cleanWebViewCacheData];
    //加载webview界面
    if (_webUrl == nil || _webUrl.length == 0) {
        self.infoLabel.hidden = NO;
    }else{
        [self loadWebViewWithUrl:_webUrl];
    }

}
#pragma mark ---- 清理webview上的缓存
- (void)cleanWebViewCacheData
{
    //清除cookies
    if ([[[UIDevice currentDevice] systemVersion]intValue ] > 8) {
        NSArray * types =@[WKWebsiteDataTypeMemoryCache,WKWebsiteDataTypeDiskCache]; // 9.0之后才有的
        NSSet *websiteDataTypes = [NSSet setWithArray:types];
        NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
        
        [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
        }];
    }else{
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask, YES) objectAtIndex:0];
        NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
        NSLog(@"%@", cookiesFolderPath);
        NSError *errors;
        [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
    }
}

#pragma mark ---- 显示加载图片
- (void)showLaunchImage
{
    self.launchImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.launchImage.contentMode = UIViewContentModeScaleToFill;
    self.launchImage.autoresizesSubviews = YES;
    self.launchImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.launchImage];
    
    UIImage *image = [[UIImage alloc]init];
    if (kDevice_Is_iPhone4)
    {
        image = [UIImage imageNamed:@"640_960"];
    }
    else if (kDevice_Is_iPhone5)
    {
        image = [UIImage imageNamed:@"640_1136"];
    }
    else if (kDevice_Is_iPhone6)
    {
        image = [UIImage imageNamed:@"750_1334"];
    }
    else if (kDevice_Is_iPhone6Plus)
    {
        image = [UIImage imageNamed:@"1242_2208"];
    }
    else if (kDevice_Is_iPhoneX)
    {
        image = [UIImage imageNamed:@"1125_2436"];
    }
    else
    {
        image = [UIImage imageNamed:@"1242_2208"];
    }
    self.launchImage.image = image;
    [self.launchImage bringSubviewToFront:self.view];
    
    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0/*延迟执行时间*/ * NSEC_PER_SEC));
    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
        if (self.launchImage.hidden == NO) {
//            [MBManager showBriefAlert:@"网络较差，请耐心等待"];
        }
        self.launchImage.hidden = YES;
    });
    
    
}
#pragma mark ---- 取消webView的横竖滚动线
- (void)setWebViewShowsVerticalAndHorizontal
{
    for (UIView *_aView in [self.webView subviews])
    {
        if ([_aView isKindOfClass:[UIScrollView class]])
        {
            [(UIScrollView *)_aView setShowsVerticalScrollIndicator:NO];
            //右侧的滚动条
            [(UIScrollView *)_aView setShowsHorizontalScrollIndicator:NO];
            //下侧的滚动条
            for (UIView *_inScrollview in _aView.subviews)
            {
                if ([_inScrollview isKindOfClass:[UIImageView class]])
                {
                    _inScrollview.hidden = YES;  //上下滚动出边界时的黑色的图片
                }
            }
        }
    }
}
#pragma mark --- 获取版本号
- (NSString *)getVersion
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    CFShow((__bridge CFTypeRef)(infoDictionary));
    // app版本
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    return app_Version;
}


#pragma mark ---- 加载webview内容
-( void)loadWebViewWithUrl:(NSString *)url
{
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData | NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    
    [self.webView loadRequest:request];
}

//字典转字符串
-(NSString *)dicToStringWithDict:(id)dict
{
    NSString *dataStr;
    if (dict == nil) {
        dataStr = @"";
        return dataStr;
    }
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
    
    if (!parseError) {
        dataStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return MakeSureNotNil(dataStr);
}
//字符串转字典
- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

#pragma mark -  UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //点击”升级“按钮，就从打开app store上应用的详情页面
    if (buttonIndex == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.updateVersionUrl]];
    }
}

#pragma mark -  WKNavigationDelegate
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [UIView animateWithDuration:0.1 animations:^{
        self.launchImage.hidden = YES;
        [self.launchImage removeFromSuperview];
    }];
}
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    self.infoLabel.hidden = NO;
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    WKNavigationActionPolicy policy =WKNavigationActionPolicyAllow;
    /* 简单判断host，真实App代码中，需要更精确判断itunes链接 */
    NSString *url = [NSString stringWithFormat:@"%@", navigationAction.request.URL];
    if([url rangeOfString:@"tel"].length > 0)
    {
        [[UIApplication sharedApplication] openURL:navigationAction.request.URL];
    }

    if([[navigationAction.request.URL host] isEqualToString:@"itunes.apple.com"] && [[UIApplication sharedApplication] openURL:navigationAction.request.URL]){
        policy =WKNavigationActionPolicyCancel;
    }
    
    decisionHandler(policy);
}



-(BOOL)webView:(WKWebView *)webView shouldPreviewElement:(WKPreviewElementInfo *)elementInfo
{
    [webView loadRequest:[NSURLRequest requestWithURL:elementInfo.linkURL]];
    NSLog(@"\n哈哈哈哈哈😄：%@\n",elementInfo.linkURL.absoluteString);
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
