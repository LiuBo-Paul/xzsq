//
//  LoanProductsListViewController.h
//  QHWallet
//
//  Created by Paul on 2019/1/2.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    SortTypeAll = 0,
    SortTypeSmall = 2,
    SortTypeBig = 3,
} SortType;

@interface LoanProductsListViewController : BaseViewController

@property (nonatomic, assign) SortType sortType;

@end

NS_ASSUME_NONNULL_END
