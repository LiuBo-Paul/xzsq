//
//  LoanProductsListViewController.m
//  QHWallet
//
//  Created by Paul on 2019/1/2.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "LoanProductsListViewController.h"
#import "ESTimer.h"
#import "DD_HomeTableViewCell.h"
#import "DD_HomeHeaderView.h"
#import "QH_loanWebDetailViewController.h"
#import "ExpandView.h"
#import "QH_loanRankSubView.h"
#import "OpenShowView.h"
#import "OpenShowMainView.h"

@interface LoanProductsListViewController ()<UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, OpenShowViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, strong) ProductDetailModel *selecteDetailModel;
@property (nonatomic, strong) UIView *prepareExpandView;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) QH_loanRankSubView *headerBtnsView;
@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, assign) BOOL isHideMsg;

@end

static CGFloat headerHeight = 78;
static CGFloat headerDisHeight = 48;
static NSString *openShowPid = @"";
static BOOL isEnableClick = YES;

@implementation LoanProductsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentPage = 1;
    self.hasNextPage = YES;
    self.isHideMsg = NO;
    ESTimer *timer = [[ESTimer alloc] init];
    [timer startTimerWithTimerType:(ESTimerTypeGCD) timeInterval:60 startTimerBlock:^(CGFloat seconds) {
        [[SDImageCache sharedImageCache] clearMemory];
        [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
    }];
    self.navigationItem.title = @"贷款";
    [self.navigationController setToolbarHidden:YES animated:YES];
    self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, headerHeight)];
    self.headerView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self refreshRankViewWithSuperView:self.headerView];
    [self.view addSubview:self.headerView];
    
    [self.view addSubview:self.mainScrollView];
    
}

-(void)viewWillLayoutSubviews {
    WEAKSELF
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.mainScrollView.frame = ({
            CGRect frame = weakSelf.mainScrollView.frame;
            frame.size.height = weakSelf.view.height - frame.origin.y;
            frame;
        });
        weakSelf.mainScrollView.contentSize = ({
            CGSize size = weakSelf.mainScrollView.contentSize;
            size.height = weakSelf.mainScrollView.height;
            size;
        });
        weakSelf.tableView.frame = ({
            CGRect frame = weakSelf.tableView.frame;
            frame.size.height = weakSelf.mainScrollView.frame.size.height;
            frame;
        });
    });
}

-(void)refreshRankViewWithSuperView:(UIView *)view
{
    if(!self.headerBtnsView) {
        CGFloat height = headerHeight;
        if(self.isHideMsg) {
            height = headerDisHeight;
        }
        self.headerBtnsView = [[QH_loanRankSubView alloc] initWithSortFrame:CGRectMake(0, 0, ScreenWidth, height)];
        self.headerBtnsView.backgroundColor = ItemColorFromRGB(0xf2f2f2);
        [self.headerBtnsView.allLoanBtn addTarget:self action:@selector(allLoanBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.headerBtnsView.smallLoanBtn addTarget:self action:@selector(smallLoanBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.headerBtnsView.bigLoanBtn addTarget:self action:@selector(bigLoanBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.headerBtnsView.closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        if(self.sortType == SortTypeAll) {
            [self moveToItem:0];
        }
        [view addSubview:self.headerBtnsView];
    } else {
        CGFloat height = headerHeight;
        if(self.isHideMsg) {
            height = headerDisHeight;
        }
        self.headerBtnsView.frame = ({
            CGRect frame = self.headerBtnsView.frame;
            frame.size.height = height;
            frame;
        });
    }
}


-(void)allLoanBtnAction:(UIButton *)sender {
    [self moveToItem:0];
}

-(void)smallLoanBtnAction:(UIButton *)sender {
    [self moveToItem:1];
}

-(void)bigLoanBtnAction:(UIButton *)sender {
    [self moveToItem:2];
}

-(void)closeBtnAction:(UIButton *)sender {
    self.isHideMsg = YES;
    sender.userInteractionEnabled = NO;
    WEAKSELF
    [UIView animateWithDuration:.5 animations:^{
        STRONGSELF
        dispatch_async(dispatch_get_main_queue(), ^{
            strongSelf.headerBtnsView.bottomView.frame = ({
                CGRect frame = strongSelf.headerBtnsView.bottomView.frame;
                frame.size.height = 0;
                frame;
            });
            strongSelf.headerView.frame = ({
                CGRect frame = strongSelf.headerView.frame;
                frame.size.height = headerDisHeight;
                frame;
            });
            strongSelf.mainScrollView.frame = ({
                CGRect frame = strongSelf.mainScrollView.frame;
                frame.origin.y = headerDisHeight;
                frame.size.height = strongSelf.mainScrollView.height + (headerHeight - headerDisHeight);
                frame;
            });
            strongSelf.mainScrollView.contentSize = CGSizeMake(ScreenWidth*3, strongSelf.mainScrollView.height);
            strongSelf.tableView.frame = ({
                CGRect frame = strongSelf.tableView.frame;
                frame.size.height = strongSelf.mainScrollView.height;
                frame;
            });
        });
    } completion:^(BOOL finished) {
        [weakSelf refreshRankViewWithSuperView:weakSelf.headerView];
        [weakSelf.headerBtnsView.bottomView removeAllSubviews];
        [weakSelf.headerBtnsView.bottomView removeFromSuperview];
    }];
    [self.view layoutSubviews];
}


-(void)refreshProductsWithSortBy:(SortType)sortType {
    self.currentPage = 1;
    [self requestProductsWithSortType:sortType];
}

-(void)loadMoreProductsWithSortBy:(SortType)sortType {
    self.currentPage ++;
    [self requestProductsWithSortType:sortType];
}

//获取产品列表
-(void)requestProductsWithSortType:(SortType)sortType
{
    // [SVProgressHUD show];
    WEAKSELF
    [[RequestMannager sharedInstance] requestProductsWithParam:@{@"pageIndex":[NSString stringWithFormat:@"%ld", (long)self.currentPage], @"pageNo":@"10", @"sortBy":[NSString stringWithFormat:@"%lu", (unsigned long)sortType], @"channel":AppStoreKey}
                                               completionBlock:^(id responseObject) {
                                                   [SVProgressHUD dismiss];
                                                   ProductsModel *model = [[ProductsModel alloc] initWithDictionary:responseObject error:nil];
                                                   weakSelf.hasNextPage = model.InnerData.HasNextPage;
                                                   [weakSelf.tableView.mj_header endRefreshing];
                                                   [weakSelf.tableView.mj_footer endRefreshing];
                                                   if(weakSelf.currentPage != 1) {
                                                       if(!model.InnerData.HasNextPage) {
                                                           [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                                                       } else {
                                                           if(model.InnerData.Models.count > 0) {
                                                               NSInteger count = weakSelf.dataArray.count;
                                                               NSMutableArray *indexPaths = [NSMutableArray new];
                                                               [weakSelf.dataArray addObjectsFromArray:[model.InnerData.Models copy]];
                                                               for (NSInteger i = count; i < weakSelf.dataArray.count; i++) {
                                                                   [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                                                               }
                                                               [weakSelf.tableView beginUpdates];
                                                               [weakSelf.tableView reloadRowsAtIndexPaths:[indexPaths copy] withRowAnimation:(UITableViewRowAnimationNone)];
                                                               [weakSelf.tableView endUpdates];
                                                           }
                                                       }
                                                   } else {
                                                       if(model.InnerData.Models.count > 0) {
                                                           weakSelf.dataArray = [NSMutableArray arrayWithArray:[model.InnerData.Models copy]];
                                                           [weakSelf.tableView reloadData];
                                                       }
                                                   }
                                               } failBlock:^(NSError *error) {
                                                   [weakSelf.tableView.mj_header endRefreshing];
                                                   [weakSelf.tableView.mj_footer endRefreshing];
                                                   [SVProgressHUD dismiss];
                                               }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DD_HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DD_HomeTableViewCell"];
    if(!cell)
    {
        cell = MainBundle(@"DD_HomeTableViewCell", self, 0);
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(indexPath.row >= 3) {
        cell.dd_homeCornerHotImageView.hidden = YES;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 126;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [self beginLoadCell:(DD_HomeTableViewCell *)cell index:indexPath.row];
}

-(void)beginLoadCell:(DD_HomeTableViewCell *)cell index:(NSInteger)index {
    if(index < self.dataArray.count) {
        Models *model = self.dataArray[index];
        [cell initHomeTableCellWithModel:model];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.prepareExpandView = [tableView cellForRowAtIndexPath:indexPath];
    Models *model = self.dataArray[indexPath.row];
    if(model && model.Pid.length > 0) {
        Models *model = self.dataArray[indexPath.row];
        if(model && model.Pid.length > 0) {
            [self requestDetailDataWithPid:model.Pid type:@"0" needUnionLogin:[[NSString stringWithFormat:@"%@", model.NeedUnionLogin] isEqual:@"1"]];
        } else {
            [self requestDetailDataWithPid:model.Pid type:@"0" needUnionLogin:[[NSString stringWithFormat:@"%@", model.NeedUnionLogin] isEqual:@"1"]];
        }
    }
}

// 获取详情
-(void)requestDetailDataWithPid:(NSString *)pid type:(NSString *)type needUnionLogin:(BOOL)needUnionLogin {
    if(!isEnableClick) {
        return;
    }
    isEnableClick = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        isEnableClick = YES;
    });
    WEAKSELF
//    [SVProgressHUD show];
    if(needUnionLogin) {
        [[RequestMannager sharedInstance] requestProductDetailWithPid:pid
                                                                 type:type
                                                      completionBlock:^(id responseObject) {
                                                          [SVProgressHUD dismiss];
                                                          ProductDetailModel *model = [[ProductDetailModel alloc] initWithDictionary:responseObject error:nil];
                                                          if([NSString stringWithFormat:@"%@", model.InnerData.UnionLoginAgreeH5].length > 0 &&
                                                             [[NSString stringWithFormat:@"%@", model.InnerData.UnionLoginAgreeH5] rangeOfString:@"http"].length > 0) {
                                                              // 弹出协议框
                                                              CGFloat viewLeftSpace = 40, viewTopSpace = 120;
                                                              OpenShowMainView *openShowMainView = [[OpenShowMainView alloc] initWithOpenShowFrame:CGRectMake(viewLeftSpace, viewTopSpace, ScreenWidth - viewLeftSpace*2, ScreenHeight - viewTopSpace*2)];
                                                              openShowMainView.clipsToBounds = YES;
                                                              openShowMainView.layer.cornerRadius = 10;
                                                              [openShowMainView.mainWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.InnerData.UnionLoginAgreeH5]]];
                                                              [openShowMainView.agreeBtn addTarget:weakSelf action:@selector(agreeBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
                                                              OpenShowView *openShowView = [OpenShowView shareInstance];
                                                              openShowView.delegate = self;
                                                              openShowView.isAnima = YES;
                                                              openShowPid = pid;
                                                              [openShowView showDeledate:self showView:openShowMainView];
                                                          } else {
                                                              [weakSelf directToDetailViewControllerWithUrl:model.InnerData.Extend1 name:model.InnerData.Name pid:model.InnerData.Pid];
                                                          }
                                                      } failBlock:^(NSError *error) {
                                                          [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                                                      }];
    } else {
        if(![[OpenShowView shareInstance] isHidden]) {
            [[OpenShowView shareInstance] hide];
        }
        [[RequestMannager sharedInstance] requestProductDetailWithPid:pid
                                                                 type:type
                                                      completionBlock:^(id responseObject) {
                                                          [SVProgressHUD dismiss];
                                                          ProductDetailModel *model = [[ProductDetailModel alloc] initWithDictionary:responseObject error:nil];
                                                          [weakSelf directToDetailViewControllerWithUrl:model.InnerData.Extend1 name:model.InnerData.Name pid:model.InnerData.Pid];
                                                      } failBlock:^(NSError *error) {
                                                          [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                                                      }];
    }
}

-(void)agreeBtnAction:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        sender.userInteractionEnabled = YES;
    });
    if(openShowPid.length > 0) {
        [self requestDetailDataWithPid:openShowPid type:@"1" needUnionLogin:NO];
    } else {
        NSLog(@"pid 未设置!");
    }
}

-(void)didHideOpenShowView {
    
}

// 跳转到产品详情页
-(void)directToDetailViewControllerWithUrl:(NSString *)url name:(NSString *)name pid:(NSString *)pid {
    if(pid.length > 0 && url.length > 0 && [url rangeOfString:@"http"].length > 0) {
//        QH_loanWebDetailViewController *detail = [[QH_loanWebDetailViewController alloc] initWithUrlStr:url titleStr:name pid:pid];
//        [[ExpandView shareInstance] pushToController:detail navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:self.prepareExpandView type:ExpandViewAnimationTypeDefault showEnd:^{
//
//        }];
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:^(BOOL success) {
                if(pid.length > 0) {
                    [[RecordTool sharedRecordTool] clickProductRecordWithPid:pid];
                }
            }];
        }
    }
}

-(NSString *)getListTypeWithIndex:(NSInteger)index {
    NSString *type = @"0";
    switch (index) {
        case 0: { type = @"3"; } break;
        case 1: { type = @"1"; } break;
        case 2: { type = @"2"; } break;
        default: break;
    }
    return type;
}

- (NSMutableArray *)dataArray {
    if(!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if([scrollView class] == [UIScrollView class]) {
        CGFloat position = scrollView.contentOffset.x/ScreenWidth;
        WEAKSELF
        self.headerBtnsView.lineView.frame = ({
            CGRect frame = weakSelf.headerBtnsView.lineView.frame;
            frame.origin.x = position*ScreenWidth/3.0;
            frame.size.width = ScreenWidth / 3.0;
            frame;
        });
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if([scrollView class] == [UIScrollView class]) {
        NSInteger index = scrollView.contentOffset.x/ScreenWidth;
        [self moveToItem:index];
    }
}

static NSInteger selectedIndex = 0;

-(void)moveToItem:(NSInteger)index {
    selectedIndex = index;
    [self refreshSortBtnWithIndex:index]; // 刷新按钮状态
    [self.mainScrollView scrollRectToVisible:CGRectMake(ScreenWidth*index, 0, ScreenWidth, _mainScrollView.height) animated:YES];  // 修改scrollView显示位置
    [self scrollLineViewToIndex:index]; // 修改滑动条位置
    [self refreTableViewWithIndex:index]; // 刷新列表
}

-(void)refreshSortBtnWithIndex:(NSInteger)index {
    [self.headerBtnsView.allLoanBtn setTitleColor:MainColor forState:(UIControlStateNormal)];
    [self.headerBtnsView.smallLoanBtn setTitleColor:MainColor forState:(UIControlStateNormal)];
    [self.headerBtnsView.bigLoanBtn setTitleColor:MainColor forState:(UIControlStateNormal)];
    switch (index) {
        case 0: {
            [self.headerBtnsView.allLoanBtn setTitleColor:MainSelectedColor forState:(UIControlStateNormal)];
        } break;
        case 1: {
            [self.headerBtnsView.smallLoanBtn setTitleColor:MainSelectedColor forState:(UIControlStateNormal)];
        } break;
        case 2: {
            [self.headerBtnsView.bigLoanBtn setTitleColor:MainSelectedColor forState:(UIControlStateNormal)];
        } break;
        default:
            break;
    }
}

-(void)scrollLineViewToIndex:(NSInteger) index {
    WEAKSELF
    [UIView animateWithDuration:0.1 animations:^{
        weakSelf.headerBtnsView.lineView.frame = ({
            CGRect frame = weakSelf.headerBtnsView.lineView.frame;
            frame.origin.x = index*ScreenWidth/3.0;
            frame.size.width = ScreenWidth / 3.0;
            frame;
        });
    }];
}

-(void)refreTableViewWithIndex:(NSInteger)index {
    self.tableView = [self.mainScrollView viewWithTag:1000+index];
    SortType sortType;
    switch (index) {
        case 0:
            sortType = SortTypeAll;
            break;
        case 1:
            sortType = SortTypeSmall;
            break;
        case 2:
            sortType = SortTypeBig;
            break;
        default:
            sortType = SortTypeAll;
            break;
    }
    WEAKSELF
    self.tableView.mj_header = [GeneralHeader generalHeaderRefreshingBlock:^{
        [weakSelf refreshProductsWithSortBy:sortType];
    }];
    
    self.tableView.mj_footer = [GeneralFooter generalFooterWithRefreshingBlock:^{
        [weakSelf loadMoreProductsWithSortBy:sortType];
    }];
    
    if([self isLoadedTableView]) {
        [self refreshProductsWithSortBy:sortType];
    }
}

-(BOOL)isLoadedTableView {
    if(self.tableView.visibleCells.count == 0) {
        return YES;
    } else {
        return NO;
    }
}

-(UIScrollView *)mainScrollView {
    if(!_mainScrollView) {
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, headerHeight, ScreenWidth, ScreenHeight - ToolbarHeight - NavBarHeight - StateBarHeight - headerHeight)];
        _mainScrollView.contentSize = CGSizeMake(ScreenWidth*3.0, _mainScrollView.height);
        _mainScrollView.pagingEnabled = YES;
        _mainScrollView.delegate = self;
        _mainScrollView.bounces = NO;
        
        for (int i = 0; i < 3; i++) {
            UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(ScreenWidth*i, 0, ScreenWidth, _mainScrollView.height) style:(UITableViewStylePlain)];
            tableView.tag = 1000+i;
            tableView.delegate = self;
            tableView.dataSource = self;
            tableView.separatorColor = [UIColor clearColor];
            [_mainScrollView addSubview:tableView];
            if(i == 0) {
                self.tableView = tableView;
            }
        }
        [self.view addSubview:_mainScrollView];
    }
    return _mainScrollView;
}

-(void)setSortType:(SortType)sortType {
    if(!_sortType) {
        _sortType = sortType;
    }
    WEAKSELF
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        STRONGSELF
        dispatch_async(dispatch_get_main_queue(), ^{
            switch (sortType) {
                case SortTypeAll:
                    [strongSelf moveToItem:0];
                    break;
                case SortTypeSmall:
                    [strongSelf moveToItem:1];
                    break;
                case SortTypeBig:
                    [strongSelf moveToItem:2];
                    break;
                default:
                    [strongSelf moveToItem:0];
                    break;
            }
        });
    });
}

@end
