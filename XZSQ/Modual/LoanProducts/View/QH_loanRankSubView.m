//
//  QH_loanRankSubView.m
//  QHWallet
//
//  Created by Paul on 2018/8/28.
//  Copyright © 2018年 QingHu. All rights reserved.
//

#import "QH_loanRankSubView.h"

@implementation QH_loanRankSubView

- (instancetype)initWithSortFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self = [[NSBundle mainBundle] loadNibNamed:@"QH_loanRankSubView" owner:self options:nil][0];
        self.frame = frame;
    }
    return self;
}

@end
