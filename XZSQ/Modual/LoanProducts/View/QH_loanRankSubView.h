//
//  QH_loanRankSubView.h
//  QHWallet
//
//  Created by Paul on 2018/8/28.
//  Copyright © 2018年 QingHu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QH_loanRankSubView : UIView

@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UIButton *allLoanBtn;
@property (strong, nonatomic) IBOutlet UIButton *smallLoanBtn;
@property (strong, nonatomic) IBOutlet UIButton *bigLoanBtn;
@property (strong, nonatomic) IBOutlet UIButton *closeBtn;
@property (strong, nonatomic) IBOutlet UIView *lineView;

- (instancetype)initWithSortFrame:(CGRect)frame;

@end
