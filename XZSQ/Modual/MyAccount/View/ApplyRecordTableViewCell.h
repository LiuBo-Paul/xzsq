//
//  ApplyRecordTableViewCell.h
//  DaddyLoan
//
//  Created by Sara on 23/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApplyRecordTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *applyRecordImageView;
@property (strong, nonatomic) IBOutlet UILabel *applyRecordTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *applyRecordTimeLabel;

@property (strong, nonatomic) IBOutlet UILabel *red_titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *red_timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *red_subtitleLabel;

@property (strong, nonatomic) IBOutlet UILabel *w_moneyLabel;
@property (strong, nonatomic) IBOutlet UILabel *w_timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *w_userNameLable;
@property (strong, nonatomic) IBOutlet UILabel *w_remarkLabel;

@end
