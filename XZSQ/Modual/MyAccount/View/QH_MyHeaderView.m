//
//  QH_MyView.m
//  XZSQ
//
//  Created by Paul on 2019/6/25.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "QH_MyHeaderView.h"

@implementation QH_MyHeaderView

- (instancetype)initWithMyHeaderFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle] loadNibNamed:@"QH_MyHeaderView" owner:self options:nil][0];
        self.frame = frame;
    }
    return self;
}

@end
