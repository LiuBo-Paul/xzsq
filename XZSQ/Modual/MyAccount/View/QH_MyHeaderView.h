//
//  QH_MyView.h
//  XZSQ
//
//  Created by Paul on 2019/6/25.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QH_MyHeaderView : UIView

@property (strong, nonatomic) IBOutlet UIView *subBackView;
@property (strong, nonatomic) IBOutlet UIButton *headIconBtn;
@property (strong, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *completeMaterailBtn;
@property (strong, nonatomic) IBOutlet UIButton *loanRecordBtn;

- (instancetype)initWithMyHeaderFrame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
