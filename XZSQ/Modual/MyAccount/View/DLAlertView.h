//
//  DLAlertView.h
//  DaddyLoan
//
//  Created by Sara on 23/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLAlertView : UIView

- (instancetype)initWithFrame:(CGRect)frame;
-(void)show;

@end
