//
//  DLAlertView.m
//  DaddyLoan
//
//  Created by Sara on 23/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import "DLAlertView.h"

@interface DLAlertView()<UIGestureRecognizerDelegate, UIWebViewDelegate>

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UIView *blackView;
@property (nonatomic, strong) UIView *backView;

@end

static BOOL isVisible = NO;
static CGFloat DLAlertViewAnimationDuration = 0.3;

@implementation DLAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.window = [UIApplication sharedApplication].keyWindow;
    }
    return self;
}

-(void)show
{
    if(!isVisible)
    {
        [self removeAllSubviews];
        isVisible = YES;
        GlobalConfigInfoModel *model = [[General_Tool GT_sharedInstance] GT_getGlobalConfigInfoModel];
        
        self.blackView = [[UIView alloc] initWithFrame:self.window.bounds];
        self.blackView.userInteractionEnabled = YES;
        self.blackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        
        UITapGestureRecognizer *tapblackView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBlackView:)];
        tapblackView.delegate = self;
        [self.blackView addGestureRecognizer:tapblackView];
        
        [self.window addSubview:self.blackView];
        
        self.backView = [[UIView alloc] initWithFrame:CGRectMake(10, (ScreenHeight-80)/2.0, ScreenWidth-20, 60*2)];
        self.backView.backgroundColor = [UIColor whiteColor];
        self.backView.clipsToBounds = YES;
        self.backView.userInteractionEnabled = YES;
        self.backView.layer.cornerRadius = 8;
        [self.window addSubview:self.backView];
        
        UIImageView *imageViewWeChat = [[UIImageView alloc] initWithFrame:CGRectMake(10, 20, 20, 20)];
        imageViewWeChat.image = [UIImage imageNamed:@"ic_wechat"];
        [self.backView addSubview:imageViewWeChat];
        
        UILabel *weChatLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageViewWeChat.right + 10, imageViewWeChat.top, self.backView.width - imageViewWeChat.right - 100, imageViewWeChat.height)];
        weChatLabel.text = [NSString stringWithFormat:@"%@", model.InnerData.ContactWXNO];
        [weChatLabel setTextColor:ItemColorFromRGB(0x333333)];
        weChatLabel.adjustsFontSizeToFitWidth = YES;
        [self.backView addSubview:weChatLabel];
        
        UIButton *copyBtn = [[UIButton alloc] initWithFrame:CGRectMake(weChatLabel.right, weChatLabel.top-5, 80, weChatLabel.height+10)];
        [copyBtn setBackgroundColor:MainSelectedColor];
        [copyBtn setTitle:@"复制" forState:UIControlStateNormal];
        [copyBtn.titleLabel setFont:kSysFontSize(13)];
        [copyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        copyBtn.clipsToBounds = YES;
        copyBtn.layer.cornerRadius = 5;
        [copyBtn addTarget:self action:@selector(copyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.backView addSubview:copyBtn];
        
        UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.backView.height/2.0, self.backView.width, 1.5)];
        lineLabel.backgroundColor = [[UIColor darkGrayColor] colorWithAlphaComponent:0.6];
        [self.backView addSubview:lineLabel];
        
        UIImageView *imageViewPhone = [[UIImageView alloc] initWithFrame:CGRectMake(10, lineLabel.bottom + 20, 20, 20)];
        imageViewPhone.image = [UIImage imageNamed:@"ic_phone"];
        [self.backView addSubview:imageViewPhone];
        
        UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageViewPhone.right + 10, imageViewPhone.top, self.backView.width - imageViewPhone.right - 100, imageViewPhone.height)];
        phoneLabel.text = [NSString stringWithFormat:@"%@", model.InnerData.ContactPhone];
        [phoneLabel setTextColor:ItemColorFromRGB(0x333333)];
        phoneLabel.adjustsFontSizeToFitWidth = YES;
        [self.backView addSubview:phoneLabel];
        
        UIButton *copy2Btn = [[UIButton alloc] initWithFrame:CGRectMake(phoneLabel.right, phoneLabel.top-5, 80, phoneLabel.height+10)];
        [copy2Btn setBackgroundColor:MainSelectedColor];
        [copy2Btn setTitle:@"复制" forState:UIControlStateNormal];
        [copy2Btn.titleLabel setFont:kSysFontSize(13)];
        [copy2Btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        copy2Btn.clipsToBounds = YES;
        copy2Btn.layer.cornerRadius = 5;
        [copy2Btn addTarget:self action:@selector(copy2BtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.backView addSubview:copy2Btn];
        
        
        __weak typeof(self) weakSelf = self;
        self.backView.frame = ({
            CGRect frame = weakSelf.backView.frame;
            frame.origin.x = ScreenWidth/2.0;
            frame.origin.y = ScreenHeight/2.0;
            frame.size.width = 0;
            frame.size.height = 0;
            frame;
        });
        [UIView animateWithDuration:DLAlertViewAnimationDuration animations:^{
            __strong typeof(weakSelf) strongSelf = weakSelf;
            weakSelf.backView.frame = ({
                CGRect frame = strongSelf.backView.frame;
                frame.origin.x = 10;
                frame.origin.y = (ScreenHeight - 120)/2.0;
                frame.size.width = ScreenWidth-20;
                frame.size.height = 120;
                frame;
            });
        }];
    }
    else
    {
        //nothing to do
    }
}

-(void)copyBtnClick:(UIButton *)sender
{
    GlobalConfigInfoModel *model = [[General_Tool GT_sharedInstance] GT_getGlobalConfigInfoModel];
    [self copyStr:model.InnerData.ContactWXNO];
}

-(void)copy2BtnClick:(UIButton *)sender
{
    GlobalConfigInfoModel *model = [[General_Tool GT_sharedInstance] GT_getGlobalConfigInfoModel];
    [self copyStr:model.InnerData.ContactPhone];
}

-(void)copyStr:(NSString *)str
{
    [self hide];
    UIPasteboard*pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = str;
    [SVProgressHUD showSuccessWithStatus:@"已复制到剪切板"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

-(void)callBtnClick:(UIButton *)sender {
    [self hide];
    [SVProgressHUD show];
    NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"tel:%@",@"021-32265088"];
    UIWebView *webView = [[UIWebView alloc] init];
    webView.delegate = self;
    [webView loadRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:str]]];
    [self addSubview:webView];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

-(void)tapBlackView:(UITapGestureRecognizer *)tap {
    [self hide];
}

-(void)hide
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:DLAlertViewAnimationDuration animations:^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        weakSelf.backView.frame = ({
            CGRect frame = strongSelf.backView.frame;
            frame.origin.x = ScreenWidth/2.0;
            frame.origin.y = ScreenHeight/2.0;
            frame.size.width = 0;
            frame.size.height = 0;
            frame;
        });
    } completion:^(BOOL finished) {
        isVisible = NO;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [UIView animateWithDuration:0.5 animations:^{
            strongSelf.blackView.alpha = 0;
            strongSelf.backView.alpha = 0;
        } completion:^(BOOL finished) {
            [strongSelf.blackView removeFromSuperview];
            [strongSelf.backView removeFromSuperview];
        }];
    }];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [SVProgressHUD dismiss];
}
@end

