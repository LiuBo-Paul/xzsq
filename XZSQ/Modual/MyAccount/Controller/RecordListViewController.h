//
//  RecordListViewController.h
//  XZSQ
//
//  Created by Paul on 2019/6/26.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    RecordTypeApply = 0,
    RecordTypeClick = 1
} RecordType;

@interface RecordListViewController : BaseViewController

@property (nonatomic, assign) RecordType recordType;

@end

NS_ASSUME_NONNULL_END
