//
//  FeedbackViewController.m
//  DaddyLoan
//
//  Created by Sara on 20/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import "FeedbackViewController.h"
#import "ExpandView.h"

@interface FeedbackViewController ()<UITextViewDelegate>

@property (nonatomic, strong) UILabel *countLabel;
@property (nonatomic, strong) UIButton *submitBtn;

@property (nonatomic, copy) NSString *feedbackContent;

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ItemColorFromRGB(0xf2f2f2);
    self.navigationItem.title = @"反馈";
    self.feedbackContent = @"";
    [self addBackItem];
    
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 20, ScreenWidth-20, 200)];
    textView.delegate = self;
    textView.textColor = [UIColor darkGrayColor];
    textView.clipsToBounds = YES;
    textView.layer.cornerRadius = 5;
    textView.font = kSysFontSize(16);
    textView.backgroundColor = [UIColor whiteColor];
    textView.text = @"请输入您的反馈意见(5-500字)";
    [self.view addSubview:textView];
    
    self.countLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, textView.bottom + 5, ScreenWidth-20, 20)];
    self.countLabel.text = @"0/500";
    self.countLabel.font = kSysFontSize(13);
    self.countLabel.textColor = [UIColor darkGrayColor];
    self.countLabel.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:self.countLabel];
    
    UILabel *remarkLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, self.countLabel.bottom + 10, ScreenWidth-20, 60)];
    remarkLabel.font = kSysFontSize(14);
    remarkLabel.numberOfLines = 0;
    remarkLabel.textColor = [UIColor darkGrayColor];
    remarkLabel.text = @"我们会认真查看您的反馈, 如紧急请注明, 工作人员会在一个工作日内联系您. ";
    [self.view addSubview:remarkLabel];
    
    self.submitBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, remarkLabel.bottom + 20, ScreenWidth - 20, 40)];
    [self.submitBtn setTitle:@"提交" forState:UIControlStateNormal];
    [self.submitBtn addTarget:self action:@selector(submitBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.submitBtn setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.5]];
    self.submitBtn.clipsToBounds = YES;
    self.submitBtn.layer.cornerRadius = self.submitBtn.height/2.0;
    [self.view addSubview:self.submitBtn];
}

-(void)back:(UIBarButtonItem *)item
{
    [[ExpandView shareInstance] popWithNavController:self.navigationController type:(ExpandViewAnimationTypeDefault) showEnd:^{
        
    }];
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    if([textView.text rangeOfString:@"请输入您的反馈意见"].length > 0)
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
}

-(void)textViewDidChange:(UITextView *)textView {
    self.countLabel.text = [NSString stringWithFormat:@"%ld/500", textView.text.length];
    self.feedbackContent = textView.text;
    if(textView.text.length > 10)
    {
        [self.submitBtn setBackgroundColor:MainSelectedColor];
    }
    if(textView.text.length >= 500)
    {
        textView.text = self.feedbackContent;
        [SVProgressHUD showErrorWithStatus:@"最多输入500字"];
        return;
    }
}

-(void)submitBtnClick:(UIButton *)sender {
    if((self.feedbackContent.length < 10) || (self.feedbackContent.length > 500))
    {
        [SVProgressHUD showErrorWithStatus:@"反馈字数应在10-500之间"];
        return;
    }
    [SVProgressHUD show];
    [[RequestMannager sharedInstance] feedBackWithContent:self.feedbackContent completionBlock:^(id responseObject) {
        [SVProgressHUD showSuccessWithStatus:@"反馈成功"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[ExpandView shareInstance] popWithNavController:self.navigationController type:(ExpandViewAnimationTypeDefault) showEnd:^{
                
            }];
        });
    } failBlock:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder)to:nil from:nil forEvent:nil];
}

@end

