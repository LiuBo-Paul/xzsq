//
//  MyViewController.m
//  XZSQ
//
//  Created by Paul on 2019/8/28.
//  Copyright © 2018年 QingHu. All rights reserved.
//

#import "MyViewController.h"
#import "EditMeViewController.h"
#import "DLAlertView.h"
#import "ExpandView.h"
#import "FeedbackViewController.h"
#import "QH_MyHeaderView.h"
#import "RecordListViewController.h"
#import "AboutUsViewController.h"
#import "CompleteMaterialViewController.h"
#import "UserInfoModel.h"
#import "CompleteListViewController.h"
#import "FSCacheManager.h"
#import "BaseWebViewController.h"

@interface MyViewController ()
<
    UITableViewDelegate,
    UITableViewDataSource,
    UIGestureRecognizerDelegate
>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) DLAlertView *dlAlertView;
@property (nonatomic, strong) QH_MyHeaderView *myHeaderView;

@end

static CGFloat myHeaderViewHeight = 275;

@implementation MyViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    if(![[General_Tool GT_sharedInstance] GT_isLogined]) {
        [APPDELEGATE resetRoot_ForceLoginViewController];
    } else {
        WEAKSELF
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[General_Tool GT_sharedInstance] GT_requestGlobalInfo];
            [[RequestMannager sharedInstance] requestAccountInfoWithCompletionBlock:^(id responseObject) {
                UserInfoModel *model = [[UserInfoModel alloc] initWithDictionary:responseObject error:nil];
                [UserInfoModel setUserInfo:model];
            } failBlock:^(NSError *error) {
                
            }];
        });
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.myHeaderView.headIconBtn.clipsToBounds = YES;
            weakSelf.myHeaderView.headIconBtn.layer.cornerRadius = weakSelf.myHeaderView.headIconBtn.height/2.0;
            
            STRONGSELF
            [[FSCacheManager sharedManager] searchDataWithName:@"AvatarImage" cacheType:(FSCacheTypeImage) successBlock:^(NSString * _Nullable message, NSData * _Nullable data, NSString * _Nullable path) {
                UIImage *image = [UIImage imageWithData:data];
                if(image) {
                    [strongSelf.myHeaderView.headIconBtn setImage:image forState:(UIControlStateNormal)];
                } else {
                    [strongSelf.myHeaderView.headIconBtn setImage:[UIImage imageNamed:@"ic_head"] forState:(UIControlStateNormal)];
                }
            } failBlock:^(NSString * _Nullable message) {
                
            }];
            NSString *nickName = [[NSUserDefaults standardUserDefaults] objectForKey:@"NickName"];
            weakSelf.myHeaderView.nickNameLabel.text = nickName?nickName:@"";
            [weakSelf.tableView reloadData];
        });
        [SVProgressHUD dismiss];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的";
    [self initData];
    [self initView];
}

-(void)initData {
    self.dataArray = [[NSMutableArray alloc] initWithArray:@[@[@{@"title":@"浏览记录", @"image":@"ic_browse"},
                                                               @{@"title":@"我的反馈", @"image":@"ic_feedback"}],
                                                             @[@{@"title":@"检查更新", @"image":@"ic_up"},
                                                               @{@"title":@"关于我们", @"image":@"ic_about"}]]];
}

-(void)initView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,  - StateBarHeight, ScreenWidth, ScreenHeight - ToolbarHeight + StateBarHeight)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorColor = ItemColorFromRGB(0xf2f2f2);
    [self.tableView setBackgroundColor:ItemColorFromRGB(0xf2f2f2)];
    [self.view addSubview:self.tableView];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, myHeaderViewHeight)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    self.myHeaderView = [[QH_MyHeaderView alloc] initWithMyHeaderFrame:CGRectMake(0, 0, ScreenWidth, myHeaderViewHeight)];
    [self.myHeaderView.headIconBtn addTarget:self action:@selector(headIconBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.myHeaderView.completeMaterailBtn addTarget:self action:@selector(completeMaterailBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.myHeaderView.loanRecordBtn addTarget:self action:@selector(loanRecordBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    
    [self addShadowToView:self.myHeaderView.headIconBtn withColor:MainColor];
    
    [self addShadowToView:self.myHeaderView.subBackView withColor:MainSelectedColor];
    
    [headerView addSubview:self.myHeaderView];
    self.tableView.tableHeaderView = headerView;

    CGFloat space = 20;
    CGFloat alHeight = 60;
    CGFloat alWidth = ScreenWidth - space*2.0;
    self.dlAlertView = [[DLAlertView alloc] initWithFrame:CGRectMake(space, (ScreenHeight - alHeight)/3.0, alWidth, alHeight)];
}

- (void)addShadowToView:(UIView *)theView withColor:(UIColor *)theColor {
    // 阴影颜色
    theView.layer.shadowColor = theColor.CGColor;
    // 阴影偏移
    theView.layer.shadowOffset = CGSizeMake(0,0);
    // 阴影透明度
    theView.layer.shadowOpacity = 0.5;
    // 阴影半径
    theView.layer.shadowRadius = 6;
    theView.clipsToBounds = NO;
    
}

-(void)headIconBtnAction:(UIButton *)sender {
    EditMeViewController *editMeViewController = [[EditMeViewController alloc] init];
    editMeViewController.title = @"账户管理";
    [[ExpandView shareInstance] pushToController:editMeViewController navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:sender type:(ExpandViewAnimationTypeDefault) showEnd:^{
        
    }];
}

-(void)completeMaterailBtnAction:(UIButton *)sender {
    CompleteListViewController *completeListViewController = NibController(CompleteListViewController);
    [[ExpandView shareInstance] pushToController:completeListViewController navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:sender type:(ExpandViewAnimationTypeDefault) showEnd:^{
        
    }];
}

-(void)loanRecordBtnAction:(UIButton *)sender {
    RecordListViewController *recordList = [[RecordListViewController alloc] init];
    recordList.recordType = RecordTypeApply;
    [[ExpandView shareInstance] pushToController:recordList navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:sender type:(ExpandViewAnimationTypeDefault) showEnd:^{
        
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ((NSArray *)self.dataArray[section]).count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *myCellIdentifier = @"GeneralTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myCellIdentifier];
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:myCellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section < self.dataArray.count && indexPath.row < ((NSArray *)self.dataArray[indexPath.section]).count) {
        cell.imageView.image = [UIImage imageNamed:self.dataArray[indexPath.section][indexPath.row][@"image"]];
        cell.textLabel.font = kSysFontSize(14);
        cell.textLabel.textColor = ItemColorFromRGB(0x333333);
        cell.textLabel.text = self.dataArray[indexPath.section][indexPath.row][@"title"];
        if([cell.textLabel.text isEqual:@"版本更新"]) {
            cell.detailTextLabel.font = kSysFontSize(12);
            cell.detailTextLabel.text = [NSString stringWithFormat:@"v %@", AppVersion];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.width, tableView.sectionHeaderHeight)];
    headerView.backgroundColor = ItemColorFromRGB(0xf2f2f2);
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        if (indexPath.row == 0) {
            // 浏览记录
            RecordListViewController *recordList = [[RecordListViewController alloc] init];
            recordList.recordType = RecordTypeClick;
            [[ExpandView shareInstance] pushToController:recordList navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:[tableView cellForRowAtIndexPath:indexPath] type:(ExpandViewAnimationTypeDefault) showEnd:^{
                
            }];
        } else if(indexPath.row == 1) {
            // 我的反馈
            FeedbackViewController *feedBack = [[FeedbackViewController alloc] init];
            [[ExpandView shareInstance] pushToController:feedBack navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:[tableView cellForRowAtIndexPath:indexPath] type:(ExpandViewAnimationTypeDefault) showEnd:^{
                
            }];
        }
    } else if(indexPath.section == 1) {
        if (indexPath.row == 0) {
            // 检查更新
            [self checkUpdate];
        } else if(indexPath.row == 1) {
            // 关于我们
            BaseWebViewController *web = [[BaseWebViewController alloc] initWithUrl:[[General_Tool GT_sharedInstance] GT_getGlobalConfigInfoModel].InnerData.AboutUsDescribe title:@"关于我们"];
            [[ExpandView shareInstance] pushToController:web navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:[tableView cellForRowAtIndexPath:indexPath] type:(ExpandViewAnimationTypeDefault) showEnd:^{
                
            }];
        }
    }
}

// 检查更新
- (void)checkUpdate
{
    [[RequestMannager sharedInstance] checkVersionUpdateCompletionBlock:^(id responseObject) {
        BaseJSONModel *model = [[BaseJSONModel alloc] initWithDictionary:responseObject error:nil];
        if([model.Status isEqual:@"1"])
        {
            NSDictionary *dic = responseObject[@"InnerData"];
            NSString *needUpdate = [NSString stringWithFormat:@"%@", dic[@"needUpdate"]];
            if(![needUpdate isEqual:@"1"]) {
                //强制更新
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"更新提示" message:@"发现新版本，请安装更新？取消将退出APP" preferredStyle:(UIAlertControllerStyleAlert)];
                [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    NSString *downloadUrl = [NSString stringWithFormat:@"%@", dic[@"DownloadUrl"]];
                    if(downloadUrl.length > 0 && [downloadUrl rangeOfString:@"http"].length > 0) {
                        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:downloadUrl]]) {
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:downloadUrl] options:@{} completionHandler:^(BOOL success) {
                                
                            }];
                        } else {
                            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"无法下载app！%@", downloadUrl]];
                            NSLog(@"无法下载app！%@", downloadUrl);
                        }
                    } else {
                        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"下载地址有误！%@", downloadUrl]];
                        NSLog(@"下载地址有误！%@", downloadUrl);
                    }
                }]];
                [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
                    exit(0);
                }]];
                [APPDELEGATE.window.rootViewController presentViewController:alertController animated:YES completion:nil];
            } else {
                //非强制更新
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"更新提示" message:@"发现新版本，请安装更新？" preferredStyle:(UIAlertControllerStyleAlert)];
                [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    NSString *downloadUrl = [NSString stringWithFormat:@"%@", dic[@"DownloadUrl"]];
                    if(downloadUrl.length > 0 && [downloadUrl rangeOfString:@"http"].length > 0) {
                        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:downloadUrl]]) {
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:downloadUrl] options:@{} completionHandler:^(BOOL success) {
                                
                            }];
                        } else {
                            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"无法下载app！%@", downloadUrl]];
                            NSLog(@"无法下载app！%@", downloadUrl);
                        }
                    } else {
                        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"下载地址有误！%@", downloadUrl]];
                        NSLog(@"下载地址有误！%@", downloadUrl);
                    }
                }]];
                [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
                    [SVProgressHUD show];
                    //取消更新，下一步检查登录状态
                    [SVProgressHUD showSuccessWithStatus:@"已取消更新！"];
                }]];
                [APPDELEGATE.window.rootViewController presentViewController:alertController animated:YES completion:nil];
            }
        }
        else
        {
            //无需更新，下一步检查登录状态
            [SVProgressHUD showSuccessWithStatus:@"当前已是最新版本，无需更新！"];
        }
    } failBlock:^(NSError *error) {
        //检查更新失败，下一步仍然检查登录状态，避免卡死
        [SVProgressHUD showErrorWithStatus:@"检查失败，请稍后重试！"];
    }];
}

@end
