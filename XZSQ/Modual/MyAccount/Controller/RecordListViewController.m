//
//  RecordListViewController.m
//  XZSQ
//
//  Created by Paul on 2019/6/26.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "RecordListViewController.h"
#import "ESTimer.h"
#import "DD_HomeTableViewCell.h"
#import "DD_HomeHeaderView.h"
#import "QH_loanWebDetailViewController.h"
#import "ExpandView.h"

@interface RecordListViewController ()<UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, strong) ProductDetailModel *selecteDetailModel;
@property (nonatomic, strong) UIView *prepareExpandView;

@end

static BOOL isEnableClick = YES;

@implementation RecordListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackItem];
    self.currentPage = 1;
    self.hasNextPage = YES;
    ESTimer *timer = [[ESTimer alloc] init];
    [timer startTimerWithTimerType:(ESTimerTypeGCD) timeInterval:60 startTimerBlock:^(CGFloat seconds) {
        [[SDImageCache sharedImageCache] clearMemory];
        [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
    }];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - StateBarHeight - NavBarHeight) style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    WEAKSELF
    self.tableView.mj_header = [GeneralHeader generalHeaderRefreshingBlock:^{
        [weakSelf refreshProducts];
    }];
    
    self.tableView.mj_footer = [GeneralFooter generalFooterWithRefreshingBlock:^{
        [weakSelf loadMoreProducts];
    }];
    
    // [SVProgressHUD show];
    
    [self refreshProducts];
    
}

-(void)refreshProducts {
    self.currentPage = 1;
    [self requestProducts];
}

-(void)loadMoreProducts {
    self.currentPage ++;
    [self requestProducts];
}

//获取产品列表
-(void)requestProducts {
    if(self.recordType == RecordTypeClick) {
        WEAKSELF
        [[RequestMannager sharedInstance] requestRecordBrowsesWithPageIndex:self.currentPage pageNo:10 completionBlock:^(id responseObject) {
            [[ViewTool sharedInstance] removeBlankViewFromView:weakSelf.tableView];
            ProductsModel *model = [[ProductsModel alloc] initWithDictionary:responseObject error:nil];
            weakSelf.hasNextPage = model.InnerData.HasNextPage;
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            if(weakSelf.currentPage != 1) {
                if(!model.InnerData.HasNextPage) {
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                } else {
                    if(model.InnerData.Models.count > 0) {
                        NSInteger count = weakSelf.dataArray.count;
                        NSMutableArray *indexPaths = [NSMutableArray new];
                        [weakSelf.dataArray addObjectsFromArray:[model.InnerData.Models copy]];
                        for (NSInteger i = count; i < weakSelf.dataArray.count; i++) {
                            [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                        }
                        [weakSelf.tableView beginUpdates];
                        [weakSelf.tableView reloadRowsAtIndexPaths:[indexPaths copy] withRowAnimation:(UITableViewRowAnimationNone)];
                        [weakSelf.tableView endUpdates];
                    } else {
                        [SVProgressHUD dismiss];
                        [[ViewTool sharedInstance] removeBlankViewFromView:self.tableView];
                        WEAKSELF
                        [[ViewTool sharedInstance] addBlankViewToView:self.tableView title:@"提示" subTitle:@"暂无借款记录！" image:[UIImage imageNamed:@"load_fail"] refreshBlock:^{
                            [weakSelf refreshProducts];
                        }];
                    }
                }
            } else {
                if(model.InnerData.Models.count > 0) {
                    weakSelf.dataArray = [NSMutableArray arrayWithArray:[model.InnerData.Models copy]];
                    [weakSelf.tableView reloadData];
                } else {
                    [SVProgressHUD dismiss];
                    [[ViewTool sharedInstance] removeBlankViewFromView:self.tableView];
                    WEAKSELF
                    [[ViewTool sharedInstance] addBlankViewToView:self.tableView title:@"提示" subTitle:@"暂无借款记录！" image:[UIImage imageNamed:@"load_fail"] refreshBlock:^{
                        [weakSelf refreshProducts];
                    }];
                }
            }
            [SVProgressHUD dismiss];
        } failBlock:^(NSError *error) {
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [SVProgressHUD dismiss];
        }];
    } else {
        [SVProgressHUD dismiss];
        [[ViewTool sharedInstance] removeBlankViewFromView:self.tableView];
        WEAKSELF
        [[ViewTool sharedInstance] addBlankViewToView:self.tableView title:@"提示" subTitle:@"暂无借款记录！" image:[UIImage imageNamed:@"load_fail"] refreshBlock:^{
            [weakSelf refreshProducts];
        }];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DD_HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DD_HomeTableViewCell"];
    if(!cell)
        {
        cell = MainBundle(@"DD_HomeTableViewCell", self, 0);
        }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.dd_homeCornerHotImageView.hidden = YES;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 126;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [self beginLoadCell:(DD_HomeTableViewCell *)cell index:indexPath.row];
}

-(void)beginLoadCell:(DD_HomeTableViewCell *)cell index:(NSInteger)index {
    if(index < self.dataArray.count) {
        Models *model = self.dataArray[index];
        [cell initHomeTableCellWithModel:model];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.prepareExpandView = [tableView cellForRowAtIndexPath:indexPath];
    Models *model = self.dataArray[indexPath.row];
    if(model && model.Pid.length > 0) {
        [self requestDetailDataWithPid:model.Pid type:@"0"];
    }
}

-(void)requestDetailDataWithPid:(NSString *)pid type:(NSString *)type {
    if(!isEnableClick) {
        return;
    }
    isEnableClick = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        isEnableClick = YES;
    });
    WEAKSELF
//    [SVProgressHUD show];
    [[RequestMannager sharedInstance] requestProductDetailWithPid:pid
                                                             type:type
                                                  completionBlock:^(id responseObject) {
                                                      [SVProgressHUD dismiss];
                                                      ProductDetailModel *model = [[ProductDetailModel alloc] initWithDictionary:responseObject error:nil];
                                                      [weakSelf directToDetailViewControllerWithUrl:model.InnerData.Extend1 name:model.InnerData.Name pid:model.InnerData.Pid];
                                                  } failBlock:^(NSError *error) {
                                                      [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                                                  }];
}

// 跳转到产品详情页
-(void)directToDetailViewControllerWithUrl:(NSString *)url name:(NSString *)name pid:(NSString *)pid {
    if(pid.length > 0 && url.length > 0 && [url rangeOfString:@"http"].length > 0) {
//        QH_loanWebDetailViewController *detail = [[QH_loanWebDetailViewController alloc] initWithUrlStr:url titleStr:name pid:pid];
//        [[ExpandView shareInstance] pushToController:detail navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:self.prepareExpandView type:ExpandViewAnimationTypeDefault showEnd:^{
//
//        }];
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:^(BOOL success) {
                if(pid.length > 0) {
                    [[RecordTool sharedRecordTool] clickProductRecordWithPid:pid];
                }
            }];
        }
    }
}

-(NSString *)getListTypeWithIndex:(NSInteger)index {
    NSString *type = @"0";
    switch (index) {
        case 0: { type = @"3"; } break;
        case 1: { type = @"1"; } break;
        case 2: { type = @"2"; } break;
        default: break;
    }
    return type;
}


- (NSMutableArray *)dataArray {
    if(!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

-(void)setRecordType:(RecordType)recordType {
    if(!_recordType) {
        _recordType = recordType;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (recordType) {
            case RecordTypeClick:
                self.title = @"浏览记录";
                break;
            case RecordTypeApply:
                self.title = @"申请记录";
                break;
            default:
                self.title = @"记录";
                break;
        }
    });
    [self refreshProducts];
}

@end
