//
//  CompleteListViewController.m
//  XZSQ
//
//  Created by Paul on 2019/6/30.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "CompleteListViewController.h"
#import "CompleteMaterialViewController.h"
#import "ExpandView.h"
#import <Contacts/Contacts.h>
#import "UserInfoModel.h"
#import "ContactsViewController.h"

@interface CompleteListViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *listTableView;
@property (nonatomic, strong) NSArray *array;

@end

@implementation CompleteListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"认证";
    [self addBackItem];
    self.listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.array = @[@{@"title":@"实名认证"}, @{@"title":@"通讯录授权"}];
    [self.listTableView reloadData];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"list"];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = kSysFontSize(15);
        cell.textLabel.textColor = ItemColorFromRGB(0x333333);
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = self.array[indexPath.row][@"title"];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0: {
            // 实名认证
            CompleteMaterialViewController *completeMaterialViewController = NibController(CompleteMaterialViewController);
            [[ExpandView shareInstance] pushToController:completeMaterialViewController navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:[tableView cellForRowAtIndexPath:indexPath] type:(ExpandViewAnimationTypeDefault) showEnd:^{
                
            }];
        } break;
        case 1: {
            // 获取通讯录
            [self requestContactAuthorAfterSystemVersion9];
        } break;
            
        default:
            break;
    }
}

//请求通讯录权限
#pragma mark 请求通讯录权限
- (void)requestContactAuthorAfterSystemVersion9 {
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if (status == CNAuthorizationStatusNotDetermined) {
        CNContactStore *store = [[CNContactStore alloc] init];
        [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError*  _Nullable error) {
            if (error) {
                NSLog(@"授权失败");
            }else {
                NSLog(@"成功授权");
            }
        }];
    } else if(status == CNAuthorizationStatusRestricted) {
        NSLog(@"用户拒绝");
        [self showAlertViewAboutNotAuthorAccessContact];
    } else if (status == CNAuthorizationStatusDenied) {
        NSLog(@"用户拒绝");
        [self showAlertViewAboutNotAuthorAccessContact];
    } else if (status == CNAuthorizationStatusAuthorized) {
        //已经授权
        //有通讯录权限-- 进行下一步操作
        [self openContact];
    }
}

//有通讯录权限-- 进行下一步操作
- (void)openContact {
    // 获取指定的字段,并不是要获取所有字段，需要指定具体的字段
    NSArray *keysToFetch = @[CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey];
    CNContactFetchRequest *fetchRequest = [[CNContactFetchRequest alloc] initWithKeysToFetch:keysToFetch];
    CNContactStore *contactStore = [[CNContactStore alloc] init];
    NSMutableArray *contacts = [[NSMutableArray alloc] init];
    BOOL isSuc = [contactStore enumerateContactsWithFetchRequest:fetchRequest error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
        //拼接姓名
        NSString *nameStr = [NSString stringWithFormat:@"%@%@",contact.familyName,contact.givenName];
        NSArray *phoneNumbers = contact.phoneNumbers;
        
        for (CNLabeledValue *labelValue in phoneNumbers) {
            CNPhoneNumber *phoneNumber = labelValue.value;
            NSString * string = phoneNumber.stringValue ;
            //去掉电话中的特殊字符
            string = [string stringByReplacingOccurrencesOfString:@"+86" withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@"-" withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@"(" withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@")" withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
            string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
            [contacts addObject:@{@"Name":nameStr, @"Phone":string}];
        }
    }];
    if([[NSString stringWithFormat:@"%@", [UserInfoModel getUserInfo].InnerData.IFAddressBookStatus] isEqual:@"0"]) {
        if(isSuc) {
            if(contacts.count > 0) {
                [SVProgressHUD showWithStatus:@"通讯录获取中..."];
                [[RequestMannager sharedInstance] submitAddressBooksWithContacts:[contacts copy] completionBlock:^(id responseObject) {
                    if(responseObject) {
                        if([[NSString stringWithFormat:@"%@", responseObject[@"Status"]] isEqual:@"1"]) {
                            [SVProgressHUD showSuccessWithStatus:@"获取完成"];
                            ContactsViewController *contactViewController = NibController(ContactsViewController);
                            contactViewController.contacts = contacts;
                            [[ExpandView shareInstance] pushToController:contactViewController navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:self.view type:(ExpandViewAnimationTypeDefault) showEnd:^{
                                
                            }];
                            
                            [[RequestMannager sharedInstance] requestAccountInfoWithCompletionBlock:^(id responseObject) {
                                UserInfoModel *model = [[UserInfoModel alloc] initWithDictionary:responseObject error:nil];
                                [UserInfoModel setUserInfo:model];
                            } failBlock:^(NSError *error) {
                                
                            }];
                        } else {
                            [SVProgressHUD showErrorWithStatus:responseObject[@"Message"]];
                        }
                    } else {
                        [SVProgressHUD showErrorWithStatus:@"提交失败！请稍后重试"];
                    }
                } failBlock:^(NSError *error) {
                    [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                }];
            } else {
                NSLog(@"通讯录列表为空");
            }
        }
    } else {
        ContactsViewController *contactViewController = NibController(ContactsViewController);
        contactViewController.contacts = contacts;
        [[ExpandView shareInstance] pushToController:contactViewController navController:self.navigationController hidesBottomBarWhenPushed:YES expandView:self.view type:(ExpandViewAnimationTypeDefault) showEnd:^{
            
        }];
    }
}

//提示没有通讯录权限
- (void)showAlertViewAboutNotAuthorAccessContact{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"请授权通讯录权限"
                                          message:[NSString stringWithFormat:@"请在iPhone的\"设置-隐私-通讯录\"选项中,允许%@访问你的通讯录", AppName]
                                          preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:OKAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
