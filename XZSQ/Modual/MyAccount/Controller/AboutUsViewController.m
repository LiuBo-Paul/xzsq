//
//  AboutUsViewController.m
//  XZSQ
//
//  Created by Paul on 2019/6/26.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "AboutUsViewController.h"

@interface AboutUsViewController ()

@property (strong, nonatomic) IBOutlet UILabel *msgLabel;
@property (strong, nonatomic) IBOutlet UILabel *contactLabel;
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *backImageView;

@end

@implementation AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"关于我们";
    [self addBackItem];
    
    [self.backImageView setImage:[[General_Tool GT_sharedInstance] GT_getLaunchImage]];
    self.msgLabel.text = [NSString stringWithFormat:@"想你所想\n\"%@\"最贴心", AppName];
    self.contactLabel.text = AppName;
    self.versionLabel.text = [NSString stringWithFormat:@"V %@", AppVersion];
}

@end
