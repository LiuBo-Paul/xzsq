//
//  EditMeViewController.m
//  DaddyLoan
//
//  Created by Sara on 22/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import "EditMeViewController.h"
#import "EditNickNameViewController.h"
#import "ExpandView.h"
#import "FSCacheManager.h"

@interface EditMeViewController ()
<
    UITableViewDelegate,
    UITableViewDataSource,
    UIGestureRecognizerDelegate,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate
>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) UIImageView *avatarImageView;

@end

@implementation EditMeViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    WEAKSELF
    dispatch_async(dispatch_get_main_queue(), ^{
        STRONGSELF
        [[FSCacheManager sharedManager] searchDataWithName:@"AvatarImage" cacheType:(FSCacheTypeImage) successBlock:^(NSString * _Nullable message, NSData * _Nullable data, NSString * _Nullable path) {
            UIImage *image = [UIImage imageWithData:data];
            if(image) {
                [strongSelf.avatarImageView setImage:image];
            } else {
                [strongSelf.avatarImageView setImage:[UIImage imageNamed:@"ic_head"]];
            }
        } failBlock:^(NSString * _Nullable message) {
            
        }];
        [strongSelf.tableView reloadData];
    });
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    [self addBackItem];
    [self initData];
    [self initView];
    [self addLogoutBtn];
}

-(void)addLogoutBtn {
    UIButton *logout = [[UIButton alloc] initWithFrame:CGRectMake(40, ScreenHeight/3.0, ScreenWidth-80, 40)];
    logout.backgroundColor = MainSelectedColor;
    logout.clipsToBounds = YES;
    logout.layer.cornerRadius = logout.height/2.0;
    [logout setTitle:@"退出登录" forState:(UIControlStateNormal)];
    [logout setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [logout.titleLabel setFont:kSysFontSize(15)];
    [logout addTarget:self action:@selector(logoutAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:logout];
}

-(void)logoutAction:(UIButton *)sender {
    // 退出登录
    WEAKSELF
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *nickName = [[NSUserDefaults standardUserDefaults] objectForKey:@"NickName"];
        NSString *mes = [NSString stringWithFormat:@"是否确定退出当前账户?"];
        if(nickName && nickName.length > 0) {
            mes = [NSString stringWithFormat:@"是否确定退出当前账户(%@)?", nickName];
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"退出登录" message:mes preferredStyle:(UIAlertControllerStyleActionSheet)];
        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            [SVProgressHUD show];
            [[RequestMannager sharedInstance] logOutCompletionBlock:^(id responseObject) {
                [[General_Tool GT_sharedInstance] GT_clearGlobalConfigInfoModel];
                [APPDELEGATE resetRoot_ForceLoginViewController];
            } failBlock:^(NSError *error) {
                
            }];
        }]];
        [weakSelf presentViewController:alert animated:YES completion:nil];
    });
}

-(void)initData {
    self.dataArray = [[NSMutableArray alloc] initWithArray:@[@{@"title":@"修改昵称", @"image":@"feedback"}]];
}

-(void)back:(UIBarButtonItem *)item {
    [[ExpandView shareInstance] popWithNavController:self.navigationController type:(ExpandViewAnimationTypeDefault) showEnd:^{
        
    }];
}

-(void)initView {
    self.view.backgroundColor = ItemColorFromRGB(0xf2f2f2);
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 200) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.scrollEnabled = NO;
    [self.view addSubview:self.tableView];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 80)];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.userInteractionEnabled = YES;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 220, 80)];
    titleLabel.text = @"头像(点击右侧修改)";
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = [UIFont systemFontOfSize:14];
    [headerView addSubview:titleLabel];
    
    NSInteger imageHeight = 60;
    NSString *avatarUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"Avatar"];
    self.avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.width - imageHeight - 30, (headerView.height - imageHeight)/2.0, imageHeight, imageHeight)];
    self.avatarImageView.userInteractionEnabled = YES;
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:avatarUrl?avatarUrl:@""] placeholderImage:[UIImage imageNamed:@"ic_portrait"]];
    self.avatarImageView.clipsToBounds = YES;
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.height/2.0;
    self.avatarImageView.backgroundColor = ItemColorFromRGB(0xf2f2f2);
    [headerView addSubview:self.avatarImageView];
    
    UITapGestureRecognizer *tapHeader = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHeader:)];
    tapHeader.delegate = self;
    [self.avatarImageView addGestureRecognizer:tapHeader];
    
    self.tableView.tableHeaderView = headerView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *myCellIdentifier = @"GeneralTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myCellIdentifier];
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:myCellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(self.dataArray.count > 0)
    {
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        if(indexPath.section == 0)
        {
            cell.textLabel.text = self.dataArray[indexPath.section][@"title"];
            NSString *nickName = [[NSUserDefaults standardUserDefaults] objectForKey:@"NickName"];
            cell.detailTextLabel.text = MakeSureNotNil(nickName);
            cell.detailTextLabel.font = [UIFont systemFontOfSize:13];
        }
        else if(indexPath.section == 1)
        {
            cell.textLabel.text = self.dataArray[indexPath.section][@"title"];
            cell.detailTextLabel.text = @"";
        }
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if((section == 0) || (section == 1))
    {
        return 10;
    }
    else
    {
        return 0.000001;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if(section == 0)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 10)];
        [view setBackgroundColor:ItemColorFromRGB(0xf2f2f2)];
        return view;
    }
    else
    {
        return [UIView new];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.000001;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0)
    {
        //修改昵称
        EditNickNameViewController *editNickNameViewController = [[EditNickNameViewController alloc] init];
        editNickNameViewController.title = @"修改昵称";
        NSString *nickName = [[NSUserDefaults standardUserDefaults] objectForKey:@"NickName"];
        editNickNameViewController.nickName = MakeSureNotNil(nickName);
        [self.navigationController pushViewController:editNickNameViewController animated:YES];
    }
}

-(void)tapHeader:(UITapGestureRecognizer *)tap {
    //拍照
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"上传头像" message:@"" preferredStyle:(UIAlertControllerStyleActionSheet)];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"相册" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        // 跳转到相册页面
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePickerController animated:YES completion:^{
            
        }];
    }]];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [alertController addAction:[UIAlertAction actionWithTitle:@"相机" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            // 跳转到相机页面
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.delegate = self;
            imagePickerController.allowsEditing = YES;
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imagePickerController animated:YES completion:^{
                
            }];
        }]];
    }
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    NSMutableData *imageBase64Data = [[self imageWithImage:image scaledToSize:CGSizeMake(300, (image.size.height*300)/image.size.width)] mutableCopy];
    NSString *imageBase64Str = [imageBase64Data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    NSString *dataStr = [NSString stringWithFormat:@"base64,%@",imageBase64Str];//data:image/png;base64,
    
    [SVProgressHUD show];
    WEAKSELF
    [[RequestMannager sharedInstance] uploadAvatarWithImageBase64Str:dataStr
                                                     completionBlock:^(id responseObject) {
                                                         [SVProgressHUD showSuccessWithStatus:responseObject[@"Message"]];
                                                         NSString *url = responseObject[@"InnerData"][@"Url"];
                                                         if(url) {
                                                             [[NSUserDefaults standardUserDefaults] setObject:[url stringByReplacingOccurrencesOfString:@"\\" withString:@"/"] forKey:@"Avatar"];
                                                             STRONGSELF
                                                             [[FSCacheManager sharedManager] saveData:UIImagePNGRepresentation(image) name:@"AvatarImage" cacheType:(FSCacheTypeImage) successBlock:^(NSString * _Nullable message, NSData * _Nullable data, NSString * _Nullable path) {
                                                                 UIImage *image = [UIImage imageWithData:data];
                                                                 if(image) {
                                                                     [strongSelf.avatarImageView setImage:image];
                                                                 } else {
                                                                     [strongSelf.avatarImageView setImage:[UIImage imageNamed:@"ic_head"]];
                                                                 }
                                                             } failBlock:^(NSString * _Nullable message) {
                                                                 
                                                             }];

                                                         }
                                                     } failBlock:^(NSError *error) {
                                                         [SVProgressHUD dismiss];
                                                     }];
}

- (NSData *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize; {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return UIImageJPEGRepresentation(newImage, 0.8);
}

@end

