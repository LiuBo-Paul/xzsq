//
//  EditNickNameViewController.m
//  DaddyLoan
//
//  Created by Sara on 23/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import "EditNickNameViewController.h"

@interface EditNickNameViewController ()

@end

@implementation EditNickNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addBackItem];
    self.view.backgroundColor = ItemColorFromRGB(0xf2f2f2);
    
    NSString *nickName = [[NSUserDefaults standardUserDefaults] objectForKey:@"NickName"];
    self.nickName = MakeSureNotNil(nickName);
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, self.view.width, 40)];
    backView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backView];
    
    UITextField *nickNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(16, 20, self.view.width - 32, 40)];
    nickNameTextField.backgroundColor = [UIColor whiteColor];
    nickNameTextField.text = self.nickName;
    [nickNameTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:nickNameTextField];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:@selector(finishEditNickName:)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} forState:UIControlStateNormal];
}

- (void)textFieldDidChange:(UITextField *)textField {
    if(textField.text.length > 12)
    {
        [SVProgressHUD showErrorWithStatus:@"昵称长度需要为2-12"];
        textField.text = self.nickName;
        return;
    }
    else
    {
        self.nickName = textField.text;
    }
}

-(void)finishEditNickName:(UIBarButtonItem *)item {
    if((self.nickName.length > 12) || (self.nickName.length < 2))
    {
        [SVProgressHUD showErrorWithStatus:@"昵称长度需要为2-12"];
    }
    else
    {
        [SVProgressHUD show];
        [[RequestMannager sharedInstance] modifyAccountWithNickName:self.nickName
                                                    completionBlock:^(id responseObject) {
                                                        [SVProgressHUD dismiss];
                                                        [[NSUserDefaults standardUserDefaults] setObject:self.nickName forKey:@"NickName"];
                                                        [self.navigationController popViewControllerAnimated:YES];
                                                    } failBlock:^(NSError *error) {
                                                        [SVProgressHUD dismiss];
                                                    }];
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder)to:nil from:nil forEvent:nil];
}

@end

