//
//  EditNickNameViewController.h
//  DaddyLoan
//
//  Created by Sara on 23/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import "BaseViewController.h"

@interface EditNickNameViewController : BaseViewController

@property (nonatomic, copy) NSString *nickName;

@end
/**
 
 */
