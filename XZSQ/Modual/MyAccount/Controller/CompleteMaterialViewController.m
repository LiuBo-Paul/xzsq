//
//  CompleteMaterialViewController.m
//  XZSQ
//
//  Created by Paul on 2019/6/27.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "CompleteMaterialViewController.h"
#import "UserInfoModel.h"

@interface CompleteMaterialViewController ()

@property (strong, nonatomic) IBOutlet UIView *topBackView;
@property (strong, nonatomic) IBOutlet UIButton *closeBtn;
@property (strong, nonatomic) IBOutlet UIView *mainBackView;
@property (strong, nonatomic) IBOutlet UITextField *nameText;
@property (strong, nonatomic) IBOutlet UITextField *idNumberText;
@property (strong, nonatomic) IBOutlet UIButton *sureBtn;

@end

@implementation CompleteMaterialViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self checkSate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"完善资料";
    [self addBackItem];
}

-(void)checkSate {
    WEAKSELF
    dispatch_async(dispatch_get_main_queue(), ^{
        UserInfoModel *model = [UserInfoModel getUserInfo];
        if([[NSString stringWithFormat:@"%@", model.InnerData.IFRealNameStatus] isEqual:@"0"]) {
            // 待实名
        } else {
            // 已实名
            weakSelf.mainBackView.alpha = 0.5;
            weakSelf.nameText.userInteractionEnabled = NO;
            weakSelf.idNumberText.userInteractionEnabled = NO;
            weakSelf.nameText.text = [NSString stringWithFormat:@"%@", model.InnerData.MTRealName];
            weakSelf.idNumberText.text = [NSString stringWithFormat:@"%@", model.InnerData.MTIdNo];
            weakSelf.sureBtn.hidden = YES;
        }
    });
}


- (IBAction)closeBtnAction:(id)sender {
    WEAKSELF
    [UIView animateWithDuration:0.5 animations:^{
        weakSelf.topBackView.frame = ({
            CGRect frame = weakSelf.topBackView.frame;
            frame.size.height = 0;
            frame;
        });
        weakSelf.mainBackView.frame = ({
            CGRect frame = weakSelf.mainBackView.frame;
            frame.origin.y = 0;
            frame;
        });
    } completion:^(BOOL finished) {
        [weakSelf.topBackView removeAllSubviews];
        [weakSelf.topBackView removeFromSuperview];
    }];
}

- (IBAction)sureBtnAction:(id)sender {
    if(self.nameText.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入姓名"];
        [self.nameText becomeFirstResponder];
        return;
    }
    if(self.idNumberText.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入身份证号"];
        [self.idNumberText becomeFirstResponder];
        return;
    }
    if(self.idNumberText.text.length < 15) {
        [SVProgressHUD showErrorWithStatus:@"身份证号最少15位，请检查后重新输入"];
        [self.idNumberText becomeFirstResponder];
        return;
    }
    WEAKSELF
    [[RequestMannager sharedInstance] submitIFRealNameWithRealName:self.nameText.text IdNum:self.idNumberText.text completionBlock:^(id responseObject) {
        STRONGSELF
        if(responseObject) {
            BaseJSONModel *model = [[BaseJSONModel alloc] initWithDictionary:responseObject error:nil];
            [SVProgressHUD showSuccessWithStatus:model.Message];
            [[RequestMannager sharedInstance] requestAccountInfoWithCompletionBlock:^(id responseObject) {
                UserInfoModel *model = [[UserInfoModel alloc] initWithDictionary:responseObject error:nil];
                [UserInfoModel setUserInfo:model];
                [strongSelf checkSate];
            } failBlock:^(NSError *error) {
                
            }];
        }
    } failBlock:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

@end
