//
//  ContactsViewController.m
//  XZSQ
//
//  Created by Paul on 2019/6/30.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "ContactsViewController.h"

@interface ContactsViewController ()

@property (strong, nonatomic) IBOutlet UITableView *listTableView;

@end

@implementation ContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"通讯录";
    [self addBackItem];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contacts.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:@"list"];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = kSysFontSize(15);
        cell.textLabel.textColor = ItemColorFromRGB(0x333333);
        cell.detailTextLabel.font = kSysFontSize(13);
        cell.detailTextLabel.textColor = ItemColorFromRGB(0x666666);
    }
    cell.textLabel.text = self.contacts[indexPath.row][@"Name"];
    cell.detailTextLabel.text = self.contacts[indexPath.row][@"Phone"];
    
    return cell;
}

-(void)setContacts:(NSArray *)contacts {
    if(!_contacts) {
        _contacts = [NSArray new];
    }
    _contacts = [NSArray arrayWithArray:contacts];
    [self.listTableView reloadData];
}

@end
