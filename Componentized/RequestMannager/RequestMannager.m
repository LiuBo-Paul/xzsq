//
//  RequestMannager.m
//  ComponentizedFramework
//
//  Created by Paul on 2019/8/24.
//  Copyright © 2018年 Paul. All rights reserved.
//

#import "RequestMannager.h"
#import "RequestHelper.h"

@interface RequestMannager()

@end

@implementation RequestMannager

DEF_SINGLETON(RequestMannager);

-(AFHTTPSessionManager *)requestWithRequestKey:(NSString *)requestKey param:(NSDictionary *)param CompletionBlock:(RMQueryCompletionBlock)completionBlock failBlock:(RMQueryFailBlock)failBlock
{
    RequestHelper *requestHelper = [[RequestHelper alloc] init];
    AFHTTPSessionManager *manager = [[General_Tool GT_sharedInstance] GT_sharedAFManager];
    NSLog(@"__requestHeader:\n%@", manager.requestSerializer.HTTPRequestHeaders);
    [requestHelper requestWithManager:manager key:requestKey parameter:param completion:^(id responseObject) {
        if(responseObject) {
            if([[NSString stringWithFormat:@"%@", [responseObject objectForKey:@"Status"]] rangeOfString:@"99"].length > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APPDELEGATE resetRoot_ForceLoginViewController];
                });
            } else {
                if(completionBlock) {
                    completionBlock(responseObject);
                }
            }
        } else {
            NSError *error = [[NSError alloc] init];
            [error.userInfo setValue:[NSString stringWithFormat:@"get请求失败(key:%@,param:%@)，返回数据：%@", requestKey, param, responseObject] forKey:NSLocalizedDescriptionKey];
            if(failBlock) {
                failBlock(error);
            }
        }
    } failure:^(NSError *error) {
        if(error.code == 401) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [APPDELEGATE resetRoot_ForceLoginViewController];
            });
        } else {
            if(failBlock) {
                failBlock(error);
            }
        }
    }];
    return manager;
}

-(AFHTTPSessionManager *)postWithRequestKey:(NSString *)requestKey param:(NSDictionary *)param CompletionBlock:(RMQueryCompletionBlock)completionBlock failBlock:(RMQueryFailBlock)failBlock
{
    RequestHelper *requestHelper = [[RequestHelper alloc] init];
    AFHTTPSessionManager *manager = [[General_Tool GT_sharedInstance] GT_sharedAFManager];
    [requestHelper postWithManager:manager key:requestKey parameter:param completion:^(id responseObject) {
        if(responseObject) {
            if([[responseObject objectForKey:@"ResultCode"] rangeOfString:@"99"].length > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [APPDELEGATE resetRoot_ForceLoginViewController];
                });
            } else {
                if(completionBlock) {
                    completionBlock(responseObject);
                }
            }
        } else {
            NSError *error = [[NSError alloc] init];
            [error.userInfo setValue:[NSString stringWithFormat:@"post请求失败(key:%@,param:%@)，返回数据：%@", requestKey, param, responseObject] forKey:NSLocalizedDescriptionKey];
            if(failBlock) {
                failBlock(error);
            }
        }
    } failure:^(NSError *error) {
        if(failBlock) {
            failBlock(error);
        }
    }];
    return manager;
}

//检版本查更新
-(AFHTTPSessionManager *)checkVersionUpdateCompletionBlock:(RMQueryCompletionBlock)completionBlock
                                                 failBlock:(RMQueryFailBlock)failBlock
{
    return [self requestWithRequestKey:RequestKey_checkUpdate
                                 param:@{@"T":@"iOS", @"P":AppBundleIdentifier, @"V":AppVersion}
                       CompletionBlock:^(id responseObject) {
                           if(completionBlock) {
                               completionBlock(responseObject);
                           }
                       } failBlock:^(NSError *error) {
                           if(failBlock) {
                               failBlock(error);
                           }
                       }];
}

// 请求产品
-(AFHTTPSessionManager *)requestProductsWithParam:(NSDictionary *)param
                                  completionBlock:(RMQueryCompletionBlock)completionBlock
                                        failBlock:(RMQueryFailBlock)failBlock {
    return [self requestWithRequestKey:RequestKey_Products
                                 param:param
                       CompletionBlock:^(id responseObject) {
                           if(completionBlock) {
                               completionBlock(responseObject);
                           }
                       } failBlock:^(NSError *error) {
                           if(failBlock) {
                               failBlock(error);
                           }
                       }];
}

// 获取首页主要信息
-(AFHTTPSessionManager *)requestHomePageInfoParam:(NSDictionary *)param
                                  completionBlock:(RMQueryCompletionBlock)completionBlock
                                        failBlock:(RMQueryFailBlock)failBlock {
    return [self requestWithRequestKey:RequestKey_HomePageInfo
                                 param:param
                       CompletionBlock:^(id responseObject) {
                           if(completionBlock) {
                               completionBlock(responseObject);
                           }
                       } failBlock:^(NSError *error) {
                           if(failBlock) {
                               failBlock(error);
                           }
                       }];
}

// 获取全局配置信息
-(AFHTTPSessionManager *)requestGlobalInfoCompletionBlock:(RMQueryCompletionBlock)completionBlock
                                                failBlock:(RMQueryFailBlock)failBlock {
    return [self requestWithRequestKey:RequestKey_GlobalConfigInfo
                                 param:@{NativeKey:DefaultNativeId, @"AppTerminalType":@"ios", @"AppVersion":[NSString stringWithFormat:@"%@", AppVersion], @"AppChannel":AppStoreKey, @"AppPackageName":AppName}
                       CompletionBlock:^(id responseObject) {
                           if(completionBlock) {
                               completionBlock(responseObject);
                           }
                       } failBlock:^(NSError *error) {
                           if(failBlock) {
                               failBlock(error);
                           }
                       }];
}

// 获取产品详情
-(AFHTTPSessionManager *)requestProductDetailWithPid:(NSString *)pid
                                                type:(NSString *)type
                                     completionBlock:(RMQueryCompletionBlock)completionBlock
                                           failBlock:(RMQueryFailBlock)failBlock {
    return [self requestWithRequestKey:RequestKey_ProductDetail
                                 param:@{@"Pid":MakeSureNotNil(pid), @"step":MakeSureNotNil(type)}
                       CompletionBlock:^(id responseObject) {
                           if(completionBlock) {
                               completionBlock(responseObject);
                           }
                       } failBlock:^(NSError *error) {
                           if(failBlock) {
                               failBlock(error);
                           }
                       }];
}

// 获取联合登录数据
-(AFHTTPSessionManager *)requestCheckUnionLoginWithPid:(NSString *)pid
                                       completionBlock:(RMQueryCompletionBlock)completionBlock
                                             failBlock:(RMQueryFailBlock)failBlock {
    return [self requestWithRequestKey:RequestKey_CheckUnionLogin
                                 param:@{@"pid":MakeSureNotNil(pid)}
                       CompletionBlock:^(id responseObject) {
                           if(completionBlock) {
                               completionBlock(responseObject);
                           }
                       } failBlock:^(NSError *error) {
                           if(failBlock) {
                               failBlock(error);
                           }
                       }];
}

//使用短信验证码登录
-(AFHTTPSessionManager *)postLoginByVCodeWithMobile:(NSString *)mobile
                                              vcode:(NSString *)vcode
                                    completionBlock:(RMQueryCompletionBlock)completionBlock
                                          failBlock:(RMQueryFailBlock)failBlock {
    return [self postWithRequestKey:RequestKey_LoginByVCode
                              param:@{@"mobile":MakeSureNotNil(mobile), @"vcode":vcode, @"App":AppName}
                    CompletionBlock:^(id responseObject) {
                        if(completionBlock) {
                            completionBlock(responseObject);
                        }
                    } failBlock:^(NSError *error) {
                        if(failBlock) {
                            failBlock(error);
                        }
                    }];
}

// 获取账户信息
-(AFHTTPSessionManager *)requestAccountInfoWithCompletionBlock:(RMQueryCompletionBlock)completionBlock
                                                     failBlock:(RMQueryFailBlock)failBlock {
    return [self requestWithRequestKey:RequestKey_AccountInfo
                                 param:@{}
                       CompletionBlock:^(id responseObject) {
                           if(completionBlock) {
                               completionBlock(responseObject);
                           }
                       } failBlock:^(NSError *error) {
                           if(failBlock) {
                               failBlock(error);
                           }
                       }];
}

//退出登录
-(AFHTTPSessionManager *)logOutCompletionBlock:(RMQueryCompletionBlock)completionBlock
                                     failBlock:(RMQueryFailBlock)failBlock {
    return [self postWithRequestKey:RequestKey_LoginOut
                              param:@{NativeKey:DefaultNativeId}
                    CompletionBlock:^(id responseObject) {
                        if(completionBlock) {
                            completionBlock(responseObject);
                        }
                    } failBlock:^(NSError *error) {
                        if(failBlock) {
                            failBlock(error);
                        }
                    }];
}

//反馈
-(AFHTTPSessionManager *)feedBackWithContent:(NSString *)content
                             completionBlock:(RMQueryCompletionBlock)completionBlock
                                   failBlock:(RMQueryFailBlock)failBlock {
    return [self postWithRequestKey:RequestKey_FeedBack
                              param:@{NativeKey:DefaultNativeId, @"Content":MakeSureNotNil(content)}
                    CompletionBlock:^(id responseObject) {
                        if(completionBlock) {
                            completionBlock(responseObject);
                        }
                    } failBlock:^(NSError *error) {
                        if(failBlock) {
                            failBlock(error);
                        }
                    }];
}

//修改昵称
-(AFHTTPSessionManager *)modifyAccountWithNickName:(NSString *)nickName
                                   completionBlock:(RMQueryCompletionBlock)completionBlock
                                         failBlock:(RMQueryFailBlock)failBlock {
    return [self postWithRequestKey:RequestKey_ModifyAccount
                              param:@{NativeKey:DefaultNativeId, @"NickName":MakeSureNotNil(nickName)}
                    CompletionBlock:^(id responseObject) {
                        if(completionBlock) {
                            completionBlock(responseObject);
                        }
                    } failBlock:^(NSError *error) {
                        if(failBlock) {
                            failBlock(error);
                        }
                    }];
}

//找回密码
-(AFHTTPSessionManager *)findPwdWithMobile:(NSString *)mobile
                                       pwd:(NSString *)pwd
                                confirmPwd:(NSString *)confirmPwd
                                     VCode:(NSString *)VCode
                           completionBlock:(RMQueryCompletionBlock)completionBlock
                                 failBlock:(RMQueryFailBlock)failBlock {
    return [self postWithRequestKey:RequestKey_FindPwd
                              param:@{@"Mobile":mobile, @"Pwd":MakeSureNotNil(pwd), @"ConfirmPwd":MakeSureNotNil(confirmPwd), @"VCode":MakeSureNotNil(VCode)}
                    CompletionBlock:^(id responseObject) {
                        if(completionBlock) {
                            completionBlock(responseObject);
                        }
                    } failBlock:^(NSError *error) {
                        if(failBlock) {
                            failBlock(error);
                        }
                    }];
}

//获取找回密码验证码
-(AFHTTPSessionManager *)sendFindVCodeWithMobile:(NSString *)mobile
                                 completionBlock:(RMQueryCompletionBlock)completionBlock
                                       failBlock:(RMQueryFailBlock)failBlock {
    return [self postWithRequestKey:RequestKey_SendFindVCode
                              param:@{@"Mobile":MakeSureNotNil(mobile)}
                    CompletionBlock:^(id responseObject) {
                        if(completionBlock) {
                            completionBlock(responseObject);
                        }
                    } failBlock:^(NSError *error) {
                        if(failBlock) {
                            failBlock(error);
                        }
                    }];
}

//上传头像
-(AFHTTPSessionManager *)uploadAvatarWithImageBase64Str:(NSString *)imageBase64Str
                                        completionBlock:(RMQueryCompletionBlock)completionBlock
                                              failBlock:(RMQueryFailBlock)failBlock {
    return [self postWithRequestKey:RequestKey_UploadAvatar
                              param:@{NativeKey:DefaultNativeId, @"Img":MakeSureNotNil(imageBase64Str)}
                    CompletionBlock:^(id responseObject) {
                        if(completionBlock) {
                            completionBlock(responseObject);
                        }
                    } failBlock:^(NSError *error) {
                        if(failBlock) {
                            failBlock(error);
                        }
                    }];
}

//获取访问记录
-(AFHTTPSessionManager *)requestRecordBrowsesWithPageIndex:(NSInteger)pageIndex
                                                    pageNo:(NSInteger)pageNo
                                           completionBlock:(RMQueryCompletionBlock)completionBlock
                                                 failBlock:(RMQueryFailBlock)failBlock {
    return [self requestWithRequestKey:RequestKey_RecordBrowses
                                 param:@{NativeKey:DefaultNativeId, @"PageIndex":[NSString stringWithFormat:@"%ld", pageIndex], @"PageNo":[NSString stringWithFormat:@"%ld", pageNo]}
                       CompletionBlock:^(id responseObject) {
                           if(completionBlock) {
                               completionBlock(responseObject);
                           }
                       } failBlock:^(NSError *error) {
                           if(failBlock) {
                               failBlock(error);
                           }
                       }];
}

-(AFHTTPSessionManager *)postSendLoginByVCodeWithMobile:(NSString *)mobile
                                        completionBlock:(RMQueryCompletionBlock)completionBlock
                                              failBlock:(RMQueryFailBlock)failBlock {
    return [self postWithRequestKey:RequestKey_SendLoginByVCode
                              param:@{@"Mobile":mobile}
                    CompletionBlock:^(id responseObject) {
                        if(completionBlock) {
                            completionBlock(responseObject);
                        }
                    } failBlock:^(NSError *error) {
                        if(failBlock) {
                            failBlock(error);
                        }
                    }];
}

//实名
-(AFHTTPSessionManager *)submitIFRealNameWithRealName:(NSString *)realName
                                                IdNum:(NSString *)idNum
                                      completionBlock:(RMQueryCompletionBlock)completionBlock
                                            failBlock:(RMQueryFailBlock)failBlock {
    return [self postWithRequestKey:RequestKey_SubmitIFRealName
                              param:@{@"RealName":realName, @"IdNo":idNum}
                    CompletionBlock:^(id responseObject) {
                        if(completionBlock) {
                            completionBlock(responseObject);
                        }
                    } failBlock:^(NSError *error) {
                        if(failBlock) {
                            failBlock(error);
                        }
                    }];
}

// 提交通讯录
-(AFHTTPSessionManager *)submitAddressBooksWithContacts:(NSArray *)contacts
                                      completionBlock:(RMQueryCompletionBlock)completionBlock
                                            failBlock:(RMQueryFailBlock)failBlock {
    
    return [self postWithRequestKey:RequestKey_SubmitAddressBooks
                              param:@{@"Models":[[NSString alloc]initWithData:[NSJSONSerialization dataWithJSONObject:contacts options:0 error:NULL] encoding:NSUTF8StringEncoding]}
                    CompletionBlock:^(id responseObject) {
                        if(completionBlock) {
                            completionBlock(responseObject);
                        }
                    } failBlock:^(NSError *error) {
                        if(failBlock) {
                            failBlock(error);
                        }
                    }];
}

@end
