//
//  GeneralFooter.m
//  IntegralWall
//
//  Created by Paul on 2019/7/3.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import "GeneralFooter.h"

@implementation GeneralFooter

+(MJRefreshBackNormalFooter *)generalFooterTarget:(id)target selector:(SEL)selector
{
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:target refreshingAction:selector];
    footer.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    footer.stateLabel.textColor = MainSelectedColor;
    footer.stateLabel.hidden = YES;
    return footer;
}

+(MJRefreshBackNormalFooter *)generalFooterWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)footerWithRefreshingBlock
{
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:footerWithRefreshingBlock];
    footer.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    footer.stateLabel.textColor = MainSelectedColor;
    [footer setTitle:@"获取更多最新产品" forState:(MJRefreshStateIdle)];
    [footer setTitle:@"最新产品正在派送..." forState:(MJRefreshStateRefreshing)];
    [footer setTitle:@"最新产品已备好" forState:(MJRefreshStateWillRefresh)];
    [footer setTitle:@"轻轻松开  获取更多最新产品" forState:(MJRefreshStatePulling)];
    [footer setTitle:@"只有这些了" forState:(MJRefreshStateNoMoreData)];
    return footer;
}
@end
