//
//  GeneralHeader.m
//  IntegralWall
//
//  Created by Paul on 2019/7/3.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import "GeneralHeader.h"

@implementation GeneralHeader

+(MJRefreshNormalHeader *)generalHeaderTarget:(id)target selector:(SEL)selector
{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:target refreshingAction:selector];
    header.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    header.lastUpdatedTimeLabel.hidden = YES;
    header.stateLabel.textColor = MainSelectedColor;
    header.stateLabel.hidden = YES;
    return header;
}

+(MJRefreshNormalHeader *)generalHeaderTarget:(id)target selector:(SEL)selector title:(NSString *)title
{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:target refreshingAction:selector];
    header.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    header.lastUpdatedTimeLabel.hidden = YES;
    header.stateLabel.textColor = MainSelectedColor;
    [header setTitle:title forState:(MJRefreshStateIdle)];
    [header setTitle:title forState:(MJRefreshStateRefreshing)];
    [header setTitle:title forState:(MJRefreshStateWillRefresh)];
    [header setTitle:title forState:(MJRefreshStatePulling)];
    return header;
}

+(MJRefreshNormalHeader *)generalHeaderRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock
{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:refreshingBlock];
    header.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    header.lastUpdatedTimeLabel.hidden = YES;
    header.stateLabel.textColor = MainSelectedColor;
    [header setTitle:@"获取最新产品" forState:(MJRefreshStateIdle)];
    [header setTitle:@"最新产品正在派送..." forState:(MJRefreshStateRefreshing)];
    [header setTitle:@"最新产品已备好" forState:(MJRefreshStateWillRefresh)];
    [header setTitle:@"轻轻松开  获取最新产品" forState:(MJRefreshStatePulling)];
    return header;
}
@end
