//
//  GeneralCollectionViewCell.h
//  IntegralWall
//
//  Created by QingHu on 2018/6/25.
//  Copyright © 2019 ShengHe. All rights reserved.
//

/**
 * 通用CollectionViewCell样式
 **/

#import <UIKit/UIKit.h>

@interface GeneralCollectionViewCell : UICollectionViewCell

@end
