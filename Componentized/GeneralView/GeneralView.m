//
//  GeneralView.m
//  IntegralWall
//
//  Created by Paul on 2019/8/2.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import "GeneralView.h"

@implementation GeneralView

- (instancetype)initWithSMFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self = [[NSBundle mainBundle]loadNibNamed:@"GeneralView" owner:self options:nil][0];
        self.frame = frame;
    }
    return self;
}

@end
