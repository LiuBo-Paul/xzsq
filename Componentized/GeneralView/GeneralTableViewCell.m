//
//  GeneralTableViewCell.m
//  IntegralWall
//
//  Created by QingHu on 2018/6/25.
//  Copyright © 2019 ShengHe. All rights reserved.
//

#import "GeneralTableViewCell.h"

@implementation GeneralTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
