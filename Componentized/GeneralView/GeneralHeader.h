//
//  GeneralHeader.h
//  IntegralWall
//
//  Created by Paul on 2019/7/3.
//  Copyright © 2019 ShengHe. All rights reserved.
//

/**
 * 通用列表头视图刷新样式
 **/

#import <MJRefresh/MJRefresh.h>

@interface GeneralHeader : MJRefreshNormalHeader

+(MJRefreshNormalHeader *)generalHeaderTarget:(id)target selector:(SEL)selector;
+(MJRefreshNormalHeader *)generalHeaderTarget:(id)target selector:(SEL)selector title:(NSString *)title;
+(MJRefreshNormalHeader *)generalHeaderRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlocks;

@end
