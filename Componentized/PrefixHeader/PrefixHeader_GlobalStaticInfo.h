//
//  PrefixHeader_GlobalStaticInfo.h
//  ComponentizedFramework
//
//  Created by Paul on 2019/8/23.
//  Copyright © 2018年 Paul. All rights reserved.
//

/**
 * 全局常量定义
 **/

#ifndef PrefixHeader_GlobalStaticInfo_h
#define PrefixHeader_GlobalStaticInfo_h

#define SIDCNAUIFLUIEBFO                                  @"Ki92"
#define IDSFHBUISDHFIUT                                   @"8xIs"
#define WNFEIUNWIEUFTR                                    @"o01Z"

//#define SIDCNAUIFLUIEBFO                                  @"Mjd2"
//#define IDSFHBUISDHFIUT                                   @"81f"
//#define WNFEIUNWIEUFTR                                    @"KwWrJ"

#define AppStoreKey                                       @"appstore"

#define UMengKey                                          @"5d1af9fc0cafb2d9a7000b39"                                       //友盟key
#define NativeKey                                         @"NativeId"

#define BaseUrl                                           @"https://lm-api-xzsq.she-tech.cn" //雪中送钱

#endif /* PrefixHeader_GlobalStaticInfo_h */
