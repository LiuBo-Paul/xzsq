//
//  PrefixHeader_ConfigurationInfo.h
//  ComponentizedFramework
//
//  Created by Paul on 2019/8/23.
//  Copyright © 2018年 Paul. All rights reserved.
//

/**
 * 全局配置信息宏定义
 **/

#ifndef PrefixHeader_ConfigurationInfo_h
#define PrefixHeader_ConfigurationInfo_h

#define DefaultNativeId        ([[NSUserDefaults standardUserDefaults] objectForKey:NativeKey]?[[NSUserDefaults standardUserDefaults] objectForKey:NativeKey]:@"")
#define MainColor               ItemColorFromRGB(0x666666)
#define MainSelectedColor       ItemColorFromRGB(0x1c88ee)
#define NavTitleFont            17

#endif /* PrefixHeader_ConfigurationInfo_h */
