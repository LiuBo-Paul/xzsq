//
//  FSCacheManager.h
//  ComponentizedFramework
//
//  Created by Paul on 2019/11/30.
//  Copyright © 2018 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FSCachePath.h"

typedef void(^FSCacheManagerDidSuccessSaveCache)(NSString * _Nullable message, NSData * _Nullable data, NSString * _Nullable path);
typedef void(^FSCacheManagerDidFailSaveCache)(NSString * _Nullable message);

NS_ASSUME_NONNULL_BEGIN

@interface FSCacheManager : NSObject

/**
 缓存单例

 @return 单例
 */
+(FSCacheManager *)sharedManager;

/**
 将data缓存

 @param data data
 @param name 文件名
 @param cacheType 缓存类型
 @param successBlock 成功回调
 @param failBlock 失败回调
 */
-(void)saveData:(NSData *)data name:(NSString *)name cacheType:(FSCacheType)cacheType successBlock:(FSCacheManagerDidSuccessSaveCache)successBlock failBlock:(FSCacheManagerDidFailSaveCache)failBlock;

/**
 查询缓存数据

 @param name 名字
 @param cacheType 缓存类型
 @param successBlock 成功回调
 @param failBlock 失败回调
 */
-(void)searchDataWithName:(NSString *)name cacheType:(FSCacheType)cacheType successBlock:(FSCacheManagerDidSuccessSaveCache)successBlock failBlock:(FSCacheManagerDidFailSaveCache)failBlock;

/**
 清空缓存
 */
-(void)clearAllCache;

/**
 清空某类型缓存

 @param cactheType 缓存类型
 */
-(void)clearCacheWithCacheType:(FSCacheType)cactheType;

/**
 清空指定缓存

 @param name 名字
 @param cactheType 缓存类型
 */
-(void)clearCacheWithName:(NSString *)name cacheType:(FSCacheType)cactheType;

@end

NS_ASSUME_NONNULL_END
