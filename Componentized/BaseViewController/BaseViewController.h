//
//  BaseViewController.h
//  DaddyLoan
//
//  Created by Sara on 16/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

//添加返回按钮
-(void)addBackItem;

// 添加侧滑手势返回
-(void)openPanGestureToBack;

// 移除侧滑手势返回
-(void)closePanGestureToBack;

@end
