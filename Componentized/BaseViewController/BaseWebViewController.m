//
//  BaseWebViewController.m
//  CreditCardHousekeeper
//
//  Created by Paul on 2017/11/20.
//  Copyright © 2017年 QingHu. All rights reserved.
//

#import "BaseWebViewController.h"

@interface BaseWebViewController ()
<
    WKNavigationDelegate,
    WKUIDelegate,
    UIGestureRecognizerDelegate,
    WKScriptMessageHandler
>

@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *interactiveTransition;
@property (nonatomic, strong) UIPanGestureRecognizer *panGesture;
@property (nonatomic, strong) WKWebView *wkWebView;
@property (nonatomic, copy) NSString *loadurl;

@end

@implementation BaseWebViewController

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.wkWebView = nil;
}

- (instancetype)initWithUrl:(NSString *)url {
    self = [super init];
    if (self) {
        _loadurl = url;
    }
    return self;
}

-(id)initWithUrl:(NSString *)url title:(NSString *)title {
    self = [super init];
    if (self) {
        _loadurl = url;
        self.title = title;
    }
    return self;
}

-(id)initWithUrl:(NSString *)url title:(NSString *)title customNavView:(UIView *)customNavView {
    self = [super init];
    if (self) {
        _loadurl = url;
        self.title = title;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [SVProgressHUD show];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setLeftBarButtonBack];
    self.wkWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, self.view.height - 64)];
    self.wkWebView.navigationDelegate = self;
    self.wkWebView.scrollView.bounces = NO;
    self.wkWebView.scrollView.showsVerticalScrollIndicator = NO;
    [self.wkWebView sizeToFit];
    self.wkWebView.scrollView.showsHorizontalScrollIndicator = NO;
    [[self.wkWebView configuration].userContentController addScriptMessageHandler:self name:@"NOWTaskSub"];
    [self.view addSubview:self.wkWebView];
    
    NSURL *url = [NSURL URLWithString:self.loadurl];
    [self.wkWebView loadRequest:[NSURLRequest requestWithURL:url]];
    [self openPanGestureToBack];
}

-(void)openPanGestureToBack {
    self.panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(backHandle:)];
    self.panGesture.delegate = self;
    [self.view addGestureRecognizer:self.panGesture];
}

-(void)closePanGestureToBack {
    [self.view removeGestureRecognizer:self.panGesture];
}

- (void)backHandle:(UIPanGestureRecognizer *)recognizer {
    if(self.navigationController.childViewControllers.count == 1) {
        return;
    }
    // _interactiveTransition就是代理方法2返回的交互对象，我们需要更新它的进度来控制POP动画的流程。（以手指在视图中的位置与屏幕宽度的比例作为进度）
    CGFloat process = [recognizer translationInView:self.view].x/self.view.bounds.size.width;
    process = MIN(1.0, MAX(0.0, process));
    
    if(recognizer.state == UIGestureRecognizerStateBegan) {
        // 此时，创建一个UIPercentDrivenInteractiveTransition交互对象，来控制整个过程中动画的状态
        _interactiveTransition = [[UIPercentDrivenInteractiveTransition alloc] init];
    } else if(recognizer.state == UIGestureRecognizerStateChanged) {
        [_interactiveTransition updateInteractiveTransition:process]; // 更新手势完成度
    } else if(recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        // 手势结束时，若进度大于0.2就完成pop动画，否则取消
        if(process > 0.2) {
            [_interactiveTransition finishInteractiveTransition];
            if(self.navigationController && self.navigationController.viewControllers.count >= 1) {
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        } else {
            [_interactiveTransition cancelInteractiveTransition];
        }
        _interactiveTransition = nil;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        if (self.childViewControllers.count == 1 ) {
            return NO;
        }
        if (self.panGesture && [[self.panGesture.view gestureRecognizers] containsObject:gestureRecognizer]) {
            CGPoint tPoint = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:gestureRecognizer.view];
            if (tPoint.x >= 0) {
                CGFloat y = fabs(tPoint.y);
                CGFloat x = fabs(tPoint.x);
                CGFloat af = 30.0f/180.0f * M_PI;
                CGFloat tf = tanf(af);
                if ((y/x) <= tf) {
                    return YES;
                }
                return NO;
                
            } else {
                return NO;
            }
        }
    }
    return YES;
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    //code
    NSLog(@"name = %@, body = %@", message.name, message.body);
    NSURL *url = [NSURL URLWithString:message.body];
    [self.wkWebView loadRequest:[NSURLRequest requestWithURL:url]];
}

//设置返回按钮
- (void)setLeftBarButtonBack {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"hhqb_ic_left"] style:UIBarButtonItemStyleDone target:self action:@selector(didBackButtonClicked)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
}

//返回按钮事件
- (void)didBackButtonClicked {
    if(self.wkWebView.canGoBack) {
        [self.wkWebView goBack];
        [self.wkWebView reload];
    } else {
        [self presentBackAction];
    }
}

-(void)presentBackAction {
    self.wkWebView = nil;
    if(self.navigationController.viewControllers.count <= 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)pushBackAction {
    [SVProgressHUD dismiss];
    if([self.wkWebView canGoBack]) {
        [self.wkWebView goBack];
    } else {
        [self.wkWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
        if(self.navigationController.viewControllers.count == 1) {
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"loading"]) {
        
    } else if ([keyPath isEqualToString:@"title"]) {
        self.title = self.wkWebView.title;
    }  else if ([keyPath isEqualToString:@"URL"]) {
        
    }
}

//失败
- (void)webView:(WKWebView *)webView didFailNavigation: (null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    [SVProgressHUD dismiss];
    NSLog(@"%@",error);
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSLog(@"%@", navigationAction.request.URL);
    WKNavigationActionPolicy policy =WKNavigationActionPolicyAllow;
    /* 简单判断host，真实App代码中，需要更精确判断itunes链接 */
    NSString *url = [NSString stringWithFormat:@"%@", navigationAction.request.URL];
    if([url rangeOfString:@"tel"].length > 0) {
        [[UIApplication sharedApplication] openURL:navigationAction.request.URL];
    }
    if([[navigationAction.request.URL host] isEqualToString:@"itunes.apple.com"] && [[UIApplication sharedApplication] openURL:navigationAction.request.URL]){
        policy = WKNavigationActionPolicyCancel;
    }
    
    decisionHandler(policy);
}

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [SVProgressHUD dismiss];
    //禁用webView加载网页的长按出现’拷贝’事件
    [webView evaluateJavaScript:@"document.documentElement.style.webkitUserSelect='none'" completionHandler:nil];
    [webView evaluateJavaScript:@"document.documentElement.style.webkitTouchCallout='none'" completionHandler:nil];
}

//在dealloc 中取消监听
-(void)dealloc {
    self.wkWebView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    NSLog(@"Receive Memory Warning");
}

@end
