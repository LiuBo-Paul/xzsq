//
//  BaseViewController.m
//  DaddyLoan
//
//  Created by Sara on 16/10/2017.
//  Copyright © 2017 QingHu. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()<UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *interactiveTransition;
@property (nonatomic, strong) UIPanGestureRecognizer *panGesture;

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont systemFontOfSize:NavTitleFont]}];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:[[ViewTool sharedInstance] imageWithColor:MainSelectedColor size:CGSizeMake(APPDELEGATE.window.frame.size.width, NavBarHeight + StateBarHeight)] forBarMetrics:UIBarMetricsDefault];
    [self openPanGestureToBack];
}

-(void)openPanGestureToBack {
    self.panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(backHandle:)];
    self.panGesture.delegate = self;
    [self.view addGestureRecognizer:self.panGesture];
}

-(void)closePanGestureToBack {
    [self.view removeGestureRecognizer:self.panGesture];
}

- (void)backHandle:(UIPanGestureRecognizer *)recognizer {
    if(self.navigationController.childViewControllers.count == 1) {
        return;
    }
    // _interactiveTransition就是代理方法2返回的交互对象，我们需要更新它的进度来控制POP动画的流程。（以手指在视图中的位置与屏幕宽度的比例作为进度）
    CGFloat process = [recognizer translationInView:self.view].x/self.view.bounds.size.width;
    process = MIN(1.0, MAX(0.0, process));
    
    if(recognizer.state == UIGestureRecognizerStateBegan) {
        // 此时，创建一个UIPercentDrivenInteractiveTransition交互对象，来控制整个过程中动画的状态
        _interactiveTransition = [[UIPercentDrivenInteractiveTransition alloc] init];
    } else if(recognizer.state == UIGestureRecognizerStateChanged) {
        [_interactiveTransition updateInteractiveTransition:process]; // 更新手势完成度
    } else if(recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        // 手势结束时，若进度大于0.2就完成pop动画，否则取消
        if(process > 0.2) {
            [_interactiveTransition finishInteractiveTransition];
            if(self.navigationController && self.navigationController.viewControllers.count >= 1) {
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        } else {
            [_interactiveTransition cancelInteractiveTransition];
        }
        _interactiveTransition = nil;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        if (self.childViewControllers.count == 1 ) {
            return NO;
        }
        if (self.panGesture && [[self.panGesture.view gestureRecognizers] containsObject:gestureRecognizer]) {
            CGPoint tPoint = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:gestureRecognizer.view];
            if (tPoint.x >= 0) {
                CGFloat y = fabs(tPoint.y);
                CGFloat x = fabs(tPoint.x);
                CGFloat af = 30.0f/180.0f * M_PI;
                CGFloat tf = tanf(af);
                if ((y/x) <= tf) {
                    return YES;
                }
                return NO;
                
            } else {
                return NO;
            }
        }
    }
    return YES;
}

-(void)addBackItem {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"hhqb_ic_left"] style:UIBarButtonItemStyleDone target:self action:@selector(back:)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
}

-(void)back:(UIBarButtonItem *)item {
    [SVProgressHUD dismiss];
    if(self.navigationController.viewControllers.count <= 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    NSLog(@"Receive Memory Warning");
}

@end

