//
//  OpenShowMainView.h
//  QKH
//
//  Created by Paul on 2018/9/10.
//  Copyright © 2018年 QingHu. All rights reserved.
//

/**
 * 自定义的弹窗框视图样式，使用OpenShowView承载该视图
 **/

#import <UIKit/UIKit.h>

@interface OpenShowMainView : UIView

@property (strong, nonatomic) IBOutlet UIWebView *mainWebView;
@property (strong, nonatomic) IBOutlet UIButton *agreeBtn;

@property (strong, nonatomic) IBOutlet UIButton *adSureBtn;

- (instancetype)initWithOpenShowFrame:(CGRect)frame;
- (instancetype)initWithAdFrame:(CGRect)frame;

@end
