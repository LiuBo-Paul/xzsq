//
//  RecordTool.h
//  XZSQ
//
//  Created by Paul on 2019/8/29.
//  Copyright © 2018年 QingHu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecordTool : NSObject

//单例
+(RecordTool *)sharedRecordTool;

//点击列表产品
-(void)clickProductRecordWithPid:(NSString *)pid;

//第三方页面停留
-(void)accountTimeRecordWithPid:(NSString *)pid duration:(NSString *)duration startTime:(NSString *)startTime endTime:(NSString *)endTime;

@end
