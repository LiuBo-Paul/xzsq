//
//  RecordTool.m
//  XZSQ
//
//  Created by Paul on 2019/8/29.
//  Copyright © 2018年 QingHu. All rights reserved.
//

#import "RecordTool.h"

#define HttpRequestKeyRecordClickAction @"/1.0/tracker/RecordClickAction" //追踪统计接口
#define HttpRequestKeyRecordBrowse @"/1.0/contents/RecordBrowse" //记录产品浏览记录

@interface RecordTool()

@end

@implementation RecordTool

static RecordTool *recordTool = nil;

+(RecordTool *)sharedRecordTool
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        recordTool = [[RecordTool alloc] init];
    });
    return recordTool;
}

//点击列表产品
-(void)clickProductRecordWithPid:(NSString *)pid
{
    pid = pid?pid:@"";
    [self clickRecordWithParam:[self dealDicWithType:@"90" otherParam:@{@"Pid":pid, @"Extend1":pid}]];
}

//第三方页面停留
-(void)accountTimeRecordWithPid:(NSString *)pid duration:(NSString *)duration startTime:(NSString *)startTime endTime:(NSString *)endTime
{
    pid = pid?pid:@"accountTimeRecordWithPid";
    duration = duration?duration:@"";
    startTime = startTime?startTime:@"";
    endTime = endTime?endTime:@"";
    [self clickRecordWithParam:[self dealDicWithType:@"10" otherParam:@{@"Pid":pid, @"Extend1":pid, @"Extend2":duration, @"Extend3":startTime, @"Extend4":endTime}]];
}

-(NSDictionary *)dealDicWithType:(NSString *)type otherParam:(NSDictionary *)otherParam
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:@{NativeKey:DefaultNativeId, @"ActionClickType":type}];
    for (int i = 0; i < [otherParam allKeys].count; i++)
    {
        [dic setObject:[otherParam allValues][i] forKey:[otherParam allKeys][i]];
    }
    return [dic copy];
}

-(void)clickRecordWithParam:(NSDictionary *)param
{
    if(![DefaultNativeId isEqual:@""])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self postWithManager:[AFHTTPSessionManager new] url:[NSString stringWithFormat:@"%@%@", BaseUrl, HttpRequestKeyRecordClickAction] parameter:param];
        });
    }
}

- (AFHTTPSessionManager *)postWithManager:(AFHTTPSessionManager *)manager
                                      url:(NSString *)url
                                parameter:(NSDictionary *)parameter
{
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    NSLog(@"__url:%@\n__parameter:%@\n", url, parameter);
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"AppTerminalType"];
    [manager.requestSerializer setValue:AppVersion forHTTPHeaderField:@"AppVersion"];
    [manager.requestSerializer setValue:AppStoreKey forHTTPHeaderField:@"AppChannel"];
    [manager.requestSerializer setValue:AppBundleIdentifier forHTTPHeaderField:@"AppPackageName"];
    [manager.requestSerializer setValue:AppNameEncoding forHTTPHeaderField:@"AppName"];
    [manager.requestSerializer setValue:[[General_Tool GT_sharedInstance] GT_IDFA] forHTTPHeaderField:@"UniqueId"];
    [manager.requestSerializer setValue:DefaultNativeId forHTTPHeaderField:NativeKey];
    [manager.requestSerializer setValue:[USERDEFAULTS objectForKey:@"User-Agent"] forHTTPHeaderField:@"User-Agent"];
    
    [manager POST:url parameters:parameter progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"__POST进度:%.2f%@", ((uploadProgress.completedUnitCount*1.0)/(uploadProgress.totalUnitCount*1.0))*100, @"%");
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [AFNetworkActivityIndicatorManager sharedManager].enabled = NO;
        NSLog(@"__responseObject:%@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [AFNetworkActivityIndicatorManager sharedManager].enabled = NO;
        NSLog(@"__error:%@", error);
    }];
    return manager;
}

@end
