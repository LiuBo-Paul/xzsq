//
//  ViewTool.h
//  IntegralWall
//
//  Created by QingHu on 2018/6/25.
//  Copyright © 2019 ShengHe. All rights reserved.
//

/**
 * 视图控制器工具
 **/

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

typedef void(^ViewToolBlankViewRefreshBlock)(void);

@interface ViewTool : NSObject

@property (nonatomic, strong) NSMutableArray *needsToRefreshSubLayers;

GD_SINGLETON(ViewTool)

/**
 画扇形图
 
 @param datas 数据源
 @param colors 对应颜色
 @param view 目标视图
 @param sectorLineWidth 分割线宽度
 */
- (void)drawSectorWithDatas:(NSArray <NSNumber *>*)datas
                     colors:(NSArray <UIColor *>*)colors
                     inView:(UIView *)view
            sectorLineWidth:(NSUInteger)sectorLineWidth;

/**
 页面数据为空时，加载空界面
 
 @param view 目标视图
 @param title 标题
 @param subTitle 子标题
 @param image 图片
 */
-(void)addBlankViewToView:(UIView *)view
                    title:(NSString *)title
                 subTitle:(NSString *)subTitle
                    image:(UIImage *)image
             refreshBlock:(ViewToolBlankViewRefreshBlock)refreshBlock;

/**
 移除空界面
 
 @param view 目标视图
 */
-(void)removeBlankViewFromView:(UIView *)view;

/**
 给一组视图添加过渡动画
 
 @param viewsArray 视图组
 */
-(void)addTransitionAnimationToViews:(NSArray *)viewsArray;
//animationTime 动画时长
-(void)addTransitionAnimationToViews:(NSArray *)viewsArray animationTime:(CGFloat)animationTime;

/**
 截取视图中的某一部分，生成图片

 @param view 要截取的视图
 @return 截取图片
 */
- (UIImage *)snapshotImageWithView:(UIView *)view;

/**
 截取网页长图

 @param webView 网页
 @return 截取长图
 */
- (UIImage *)snapLongImageWithWebView:(UIWebView *)webView;

/**
 给视图添加旋转动画

 @param view 要旋转的视图
 @param isNorSir 默认是顺时针效果，若将isNorSir == NO，则为逆时针效果
 @param time 动画时长
 @param repeatCount 重复次数
 */
-(void)addRotationToView:(UIView *)view isNorDir:(BOOL)isNorSir time:(CGFloat)time repeatCount:(NSInteger)repeatCount;

/**
 根据给定颜色和尺寸生成对应的纯色图片
 
 @param aColor 颜色
 @param size 尺寸
 @return 纯色图片
 */
-(UIImage *)imageWithColor:(UIColor *)aColor size:(CGSize)size;

/**
 生成渐变色图片
 
 @param colors 给定多个颜色值（至少要有2种颜色）
 @param locations 颜色分割线,颜色之间的界限 范围在0到1之间
 @param startPoint 起始点
 @param endPoint 结束点 说明：（0，0）到（1，0）和（0，1）到（1，1）都是水平从左向右渐变；（0，0）到（1，1）是从左上角向右下角渐变；（0，1）到（1，0）是从左下角向右上角渐变。
 @param type `axial' (默认值), `radial', and `conic'
 @param size 图片大小
 @return 渐变色图片
 调用例子
 [self.navigationController.navigationBar setBackgroundImage:[self colorsImageWithColors:@[(__bridge id)MainSelectedColor.CGColor, (__bridge id)[UIColor whiteColor].CGColor]
 locations:@[@(0.0), @(1.0)]
 strartPoint:CGPointMake(0, 0)
 endPoint:CGPointMake(0, 1)
 type:@"axial"
 imageSize:CGSizeMake(ScreenWidth, NavBarHeight + StateBarHeight)] forBarMetrics:UIBarMetricsDefault];
 
 */
-(UIImage *)colorsImageWithColors:(NSArray *)colors
                        locations:(NSArray<NSNumber *> *)locations
                      strartPoint:(CGPoint)startPoint
                         endPoint:(CGPoint)endPoint
                             type:(CAGradientLayerType)type
                        imageSize:(CGSize)size;

@end
