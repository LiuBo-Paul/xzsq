//
//  General_Tool.h
//  IntegralWall
//
//  Created by QingHu on 2018/6/25.
//  Copyright © 2019 ShengHe. All rights reserved.
//

/**
 * 通用工具类，提供常用工具方法
 * 使用单例模式创建实例，全局可访问
 **/

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "AFHTTPSessionManager.h"
#import "GlobalConfigInfoModel.h"

typedef void(^NetworkReachabilityNoNetworkBlock)(void);
typedef void(^NetworkReachabilityNetworkBlock)(AFNetworkReachabilityStatus status);

@interface General_Tool : NSObject

@property (nonatomic, copy) NSString *updateVersionUrl;

+ (General_Tool *)GT_sharedInstance;

/**
 快速创建UINavigationController

 @param viewController 跟视图控制器
 @param title 标题
 @param imageName 未选中图片名字
 @param selectedImageName 选中图片名字
 @param normalTitlColor 未选中标题颜色
 @param selectedTitleColor 选中标题颜色
 @param fontSize 字体大小
 @return UINavigationController
 */
-(UINavigationController *)GT_createRootTabBarItemWithController:(UIViewController *)viewController
                                                        title:(NSString *)title
                                                    imageName:(NSString *)imageName
                                            selectedImageName:(NSString *)selectedImageName
                                              normalTitlColor:(UIColor *)normalTitlColor
                                           selectedTitleColor:(UIColor *)selectedTitleColor
                                                    titleFont:(NSUInteger)fontSize;

/**
 判断是否是合法的手机号

 @param phoneNum 手机号
 @return 是否合法，YES-合法，NO-不合法
 */
- (BOOL)GT_isPhoneNum:(NSString *)phoneNum;

/**
 读取本地json文件

 @param name 文件名
 @return 文件内容
 */
- (NSDictionary *)GT_readLocalFileWithName:(NSString *)name;

/**
 取消第一响应者，隐藏键盘
 */
-(void)GT_resignFirstResponder;

/**
 SessionManager单例，防止多次创建占用CPU

 @return AFHTTPSessionManager
 */
-(AFHTTPSessionManager *)GT_sharedAFManager;

/**
 把数字转换成带百千万汉字的,例如500->5百,200000->20万

 @param num 数字
 @return 汉字数字
 */
-(NSString *)GT_transportNumberToUnit:(NSInteger)num;

/**
 对字典(Key-Value)排序 区分大小写
 
 @param dict 要排序的字典
 */
- (NSArray *)GT_sortedDictionary:(NSDictionary *)dict;

/**
 获取广告标识符

 @return 广告标识符
 */
-(NSString *)GT_IDFA;

/**
 获取启屏页图片
 
 @return 启屏页图片
 */
-(UIImage *)GT_getLaunchImage;

/**
 判断当前网络是否添加了代理

 @return YES-添加了代理， NO-没有代理
 */
- (BOOL)GT_getProxyStatus;

/**
 获取字符串高度

 @param string 字符串
 @param fontSize 字体大小
 @param width 固定宽度
 @return 所需高度
 */
- (CGFloat)GT_getHeightWithString:(NSString *)string fontSize:(CGFloat)fontSize width:(CGFloat)width;

/**
 获取字符串所需宽度

 @param string 字符串
 @param fontSize 字体大小
 @return 所需宽度
 */
- (CGFloat)GT_getWidthWithString:(NSString *)string fontSize:(CGFloat)fontSize;

/**
 获取设备具体型号
 
 @return 设备具体型号
 */
- (NSString*)GT_deviceModelName;

/**
 获取当前网络状态
 
 @return 当前网络状态
 */
- (NSString *)GT_getNetconnType;


/**
 监听网络状态

 @param networkBlock 有网回调
 @param noNetworkBlock 没有网络回调
 */
-(void)GT_AFNReachabilitynetworkBlock:(NetworkReachabilityNetworkBlock)networkBlock noNetworkBlock:(NetworkReachabilityNoNetworkBlock)noNetworkBlock;

/**
 根据base64字符串获取图片

 @param base64String base64字符串
 @return 图片
 */
-(UIImage *)GT_getImageByBase64String:(NSString *)base64String;

/**
 获取当前时间

 @param formatStr 时间格式
 @return 给定格式的时间
 */
-(NSString *)GT_getCurrentTimeWithFormatStr:(NSString *)formatStr;

/**
 获取date对应的周几

 @param date date
 @return 周几
 */
- (NSString *)GT_getweekDayStringWithDate:(NSDate *)date;

/**
 根据date获取周几
 
 @param date date
 @return 周几，周日->1,周一->2,...周六->7
 */
- (NSInteger)GT_getweekDayIntegerWithDate:(NSDate *)date;

/**
 将字符串粘贴到剪切板

 @param copyStr 要粘贴的字符串
 */
-(void)GT_copyStringToPast:(NSString *)copyStr;

/**
 拨打电话

 @param phoneNum 电话号码
 */
-(void)GT_callTelPhoneNumber:(NSString *)phoneNum;

/**
 字典转字符串

 @param dict 字典
 @return 字符串
 */
-(NSString *)GT_dicToStringWithDict:(id)dict;

/**
 判断是否是第一次登录

 @return 是否是第一次登录
 */
- (BOOL)GT_isFirstLoad;

/**
 保存”User-Agent“
 */
-(void)GT_saveUserAgent;

/**
 获取不到数据时，加载空页面
 
 @param view 承载视图
 @param title 标题
 @param subTitle 子标题
 @param image 图标
 */
-(void)GT_addBlankViewToView:(UIView *)view title:(NSString *)title subTitle:(NSString *)subTitle image:(UIImage *)image;

/**
 移除空白页面
 
 @param view 承载视图
 */
-(void)GT_removeBlankViewFromView:(UIView *)view;

/**
 存储全局配置信息
 
 @param globalConfigInfoModel 数据模型
 */
-(void)GT_setGlobalConfigInfoModel:(GlobalConfigInfoModel *)globalConfigInfoModel;

/**
 获取全局配置数据模型
 
 @return 全局配置数据模型
 */
-(GlobalConfigInfoModel *)GT_getGlobalConfigInfoModel;

/**
 清空全局配置数据
 */
-(void)GT_clearGlobalConfigInfoModel;

/**
 data转字典

 @param data data
 @return 字典
 */
-(NSDictionary*)GT_returnDictionaryWithData:(NSData*)data;

/**
 data转字符串

 @param data data
 @return 字符串
 */
-(NSString *)GT_returnStringWithData:(NSData*)data;

/**
 字典转data

 @param dict 字典
 @return data
 */
-(NSData *)GT_returnDataWithDictionary:(NSDictionary*)dict;

/**
 获取全局信息
 */
-(void)GT_requestGlobalInfo;

/**
 是否已经登录

 @return 状态
 */
-(BOOL)GT_isLogined;

@end
